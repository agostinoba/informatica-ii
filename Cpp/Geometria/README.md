# Ejercicio 5 - Clases relacionadas con geometría

Se propone implementar diversas clases relacionadas con la manipulación de
figuras geométricas. Para dicho fin, se harán significativas simplificaciones
a problemas las cuáles estarán aclaradas en los hitos correspondientes. Las
figuras geométricas que trabajaremos, serán bi-dimensionales. Para este
ejercicio, utilice la biblioteca <math.h>

## 5.1 - Definición de un punto en el plano

Definir e implementar una clase **Punto** la cuál tendrá las siguientes
características:

- Miembros privados:

```
        double x;
        double y;
```

- Constructores:

```
        Punto(double xx = 0, double yy = 0);
        Punto(const Punto &p);
        ~Punto();
```

- Operadores:

```
        const Punto & operator=(const Punto &p);

        void operator+=(const Punto &p);
        void operator-=(const Punto &p);

        Punto operator+(const Punto &p);
        Punto operator-(const Punto &p);

        bool operator==(const Punto &p);
        bool operator!=(const Punto &p);
```

- Métodos de acceso a miembros:

```
        void set_x(double xx) {x = xx;}
        void set_y(double yy) {y = yy;}

        double get_x() const {return x;}
        double get_y() const {return y;}
```

- Cálculo de distancia entre dos puntos:

```
        double distancia(const Punto &p) const;
```

Recuerde la fórmula de distancia entre dos puntos:

  <span>&#8730;((x<sub>a</sub> - x<sub>b</sub>)^2 + (y<sub>a</sub> - y<sub>b</sub>)^2)</span>  

Para probar la implementación de la clase realizada, utilice el siguiente
*main*:

```
#include <iostream>
#include <punto.h>

using namespace std;

int main(void) {
    Punto a{};
    Punto b{1, 1};
    cout << (a + b).get_y() << endl;
    if(a == b)
        cout << "a y b son iguales" << endl;
    else
        cout << "a y b son distintos" << endl;
    Punto c = a - b;
    c += Punto{1, 1};
    if(a == c)
        cout << "a y c son iguales" << endl;
    else
        cout << "a y c son distintos" << endl;
    cout << "Distancia del punto a al punto b: " << a.distancia(b) << endl;
    return 0;
}
```

Del cuál debería obtener la siguiente salida:

        1
        a y b son distintos
        a y c son iguales
        Distancia del punto a al punto b: 1.41421
    
## 5.2 - Definiendo polígonos

Definir e implementar una clase **Polígono** la cuál tendrá las siguientes
características:

- Miembros protegídos:

```
        unsigned int cantidad_vertices;
        Punto *vertices;
```

- Errores públicos:

```
        class ErrorNoDefinePoligono{};
        class ErrorRango{};
```

- Constructores/Destructores:

```
        Poligono(unsigned int n_vertices);
        Poligono(const Poligono &p);
        ~Poligono();
```
En caso de que intente instanciar un objeto **Polígono** con menos de tres
vértices, se deberá generar un error de tipo **ErrorNoDefinePoligono**.

- Operadores:

```
        bool operator==(const Poligono &p) const;
        bool operator!=(const Poligono &p) const;
```

- Métodos de acceso:

```
        void set_vertice(unsigned int n_vertice, const Punto &p);
        const Punto & get_vertice(unsigned int n_vertice) const;
        unsigned int get_cantidad_vertices() const;
```
En caso de que se intente acceder/fijar un vértice fuera de rango, se deberá
generar un error de tipo **ErrorRango**.

- Cálculo de perímetro:

```
        double perimetro() const;
```
Para calcular el perímetro de cualquier polígono, simplemente basta con sumar la
distancia entre todos los vértices que conforman el polígono.  
Este método **asume** que el olígono está definido de forma tal que el mismo no
tiene intersecciones en los segmentos que lo conforman. Asumiremos que siempre
se generan polígonos con esta característica.

- Cálculo de área:

```
        double area() const;
```

Para clacular el área de cualquier polígono, utilice la siguiente fórmula:
    
A = 1/2 E(sumatoria desde v hasta n=1) |*x<sub>n</sub>y<sub>n+1</sub> - x<sub>n+1</sub>y<sub>n</sub>*|.  

Donde:

- *v* --> Cantidad de vértices en el polígono
- *x* --> Coordenada *x*
- *y* --> Coordenada *y*
    
Esta ecuación **asume** que el polígono está definido de forma tal que el mismo
no tiene interesecciones en los segmentos que lo conforman. Asumiremos que
siempre se generan polígonos con esta característica.  
Nótese también que para el cálculo en el último vértice, el índice *n* + 1
estaría fuera de rango, por lo que en dicho caso el término correcto de la
sumatoria sería |x<sub>v</sub>y<sub>1</sub> - x<sub>1</sub>y<sub>v</sub>|.  
**NOTA: Los índices en la ecuación están escritos en notación matemática, por lo
que el índice 1 equivaldría a**:

        vertices[0]
    
Para probar que la implementación de la clase es correcta, utilizar el siguiente
*main* (corroborar que no hayan *memory leaks*):

```
#include <iostream>
#include <poligono.h>

using namespace std;

int main(void) {
    Poligono triang{3};
    traing.set_vertice(0, Punto{1});
    traing.set_vertice(1, Punto{0, 1});
    traing.set_vertice(2, Punto{-1});
    Poligono triang_2{triang};
    cout << "Perímetro del triángulo: " << triang.perimetro() << endl;
    cout << "Área del triángulo: " << triang.area() << endl;
    if(traing == triang2)
        cout << "Ambos triángulos son iguales." << endl;
    else
        cout << "Los triángulos son distintos." << endl;
    retunr 0;
}
```

De lo cuál debería obtener el siguiente resultado:

        Perímetro del triángulo: 4.82843
        Área del triángulo: 1
        Ambos triágulos son iguales.
        
## 5.3 - Implementación de una clase rectángulo

Es usual que las librerías que implementan figuras geométricas, implementen
clases como **Rectángulo** o **Triángulo** dado que son figuras usualmente
utilizadas que se pueden describir fácilmente a través de distintas formas.  
Una forma intuitiva de definir un **Rectángulo** es mediante un **Punto** que
indique su vértice superior izquierdo y asignando un *ancho* y *alto*, dado que
los tres vértices restantes pueden ser calculados fácilmente a partir de dichos
parámetros. Teniendo dicha implementación en cuenta, definir una clase
**Rectángulo** la cuál utilice a la clase **Polígono** como clase base,
utilizando herencia *pública*.

La clase **Rectángulo** tendrá las siguientes características:

- Miembros privados

```
        Punto vertice_inicial;
        unsigned int ancho;
        unsigned int alto;
```

- Métodos privados:

```
        void calc_vertices();
```

El método debe calcular y asignar los vértices correspondientes a partir del
*vértice inicial*, el *ancho* y el *alto*.

- Constructores/Destructores:

```
        Rectangulo(const Punto &p, unsigned int w, unsigned int h);
        Rectangulo(const Rectangulo &r);
        ~Rectangulo();
```

- Operadores:

```
        bool operator==(const Rectangulo &r) const;
        bool operator!=(const Rectangulo &r) const;
```

No utilice la misma estrategia  que con la clase **Polígono**, piense qué cosas
deben ser iguales o distintas para considerar que dos **Rectángulos** son
iguales.

- Métodos de acceso:

```
        void set_vertice_inicial(const Punto &p);
        void set_ancho(unsigned int w);
        void set_alto(unsigned int h);

        const Punto & get_vertice_inicial() const;
        unsigned int get_ancho() const;
        unsigned int get_alto() const;

        void set_vertice(const Punto &p) = delete;
```

El último método no debe ser implementado. El objetivo de esta línea es prevenir
que se pueda utilizar el método *set_vertice* desde un objeto de tipo
**Rectángulo**.

- Cálculo de perímetro:

```
        double perimetro() const;
```

Utilizar la fórmula de cálculo de perímetro típica de un rectángulo:

        P = 2 x ancho + 2 x alto
        
- Cálculo de área:

```
        double area() const;
```

Utilizar la fórmula de cálculo de área típica de un rectángulo:

        A = ancho x alto

A fin de probar la implementación de la clase, utilice el siguiente *main*:

```
#include <iostream>
#include <rectangulo.h>

using namespace std;

ostream & operator<<(ostream &o, const Poligono &p) {
    for(unsigned int i = 0; i < p.get_cantidad_vertices(); i++)
        cout << "Vertice " << i << " : (" << p.get_vertice(i).get_x() << ", "
             << p.get_vertice(i).get_y() << ")" << endl;
    return 0;
}

ostream & operator<<(ostream &o, const Rectangulo  &r) {
    cout << Poligono{r} << endl;
    cout << "Perimetro: " << r.perimetro() << endl;
    cout << "Area: " << r.area() << endl;
    return 0;
}

int main(void) {
    Rectangulo r{Punto{1, 1}, 1, 2};
    Rectangulo aux{r};
    cout << "Rectangulo:" << endl << r;
    r.set_vertice_inicial(Punto{10,10});
    cout << "Rectangulo:" << endl << r;
    r.set_alto(10);
    cout << "Rectangulo:" << endl << r;
    if(r == aux)
        cout << "Los rectangulos son iguales." << endl;
    else
        cout << "Los rectangulos son distintos." << endl;
    return 0;
}
```

Del cuál debería obtener la siguiente salida:

    Rectangulo:
    Vertice 0 : (1, 1)
    Vertice 1 : (2, 1)
    Vertice 2 : (2, -1)
    Vertice 3 : (1, -1)
    
    Perimetro: 6
    Area: 2
    Rectangulo:
    Vertice 0 : (10, 10)
    Vertice 1 : (11, 10)
    Vertice 2 : (11, 8)
    Vertice 3 : (10, 8)
    
    Perimetro: 6
    Area: 2
    Rectangulo:
    Vertice 0 : (10, 10)
    Vertice 1 : (11, 10)
    Vertice 2 : (11, 0)
    Vertice 3 : (10, 0)

    Perimetro: 22
    Area: 10
    Los rectangulos son distintos.
    
## 5.4 - Preguntas teóricas

- ¿Qué ventaja se obtiene al reimplementar los métodos *perimetro(), area()* y
los operadores de comparación?  
- ¿Por qué prevenimos la utilización del método *set_vertice* desde la clase
**Rectangulo**? ¿Qué peligro hubiesemos corrido en caso de no haber eliminado el
método y permitir su utilización?  
- En la implementación propuesta, estamos utilizando un puntero de objetos clase
**Punto** para guardar los distintos vértices de un **Poligono** como así
también los de un **Rectangulo**. Esto impide la posibilidad de expandir un
**Poligono** en tiempo de ejecución. ¿Qué estrategia utilizaría para sobrellevar
este problema en caso de poder cambiar algo en la implementación?
