/*******************************************************************************
 *
 * @file                 poligono.h
 * @brief                Ejercicio Geometria
 * @version              1.00
 * @date                 02/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef POLIGONO_H
#define POLIGONO_H

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "punto.h"
#include <new>

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
class Poligono {
    public:
        Poligono(void);
        Poligono(unsigned int);
        Poligono(const Poligono &);
        ~Poligono(void);

        void set_vertice(unsigned int, const Punto &);
        const Punto & get_vertice(unsigned int) const;
        unsigned int get_cantidad_vertices(void) const;
        double perimetro(void) const;
        double area(void) const;

        bool operator==(const Poligono &) const;
        bool operator!=(const Poligono &) const;

        class ErrorNoDefinePoligono {};
        class ErrorRango {};

    protected:
        unsigned int cantidad_vertices;
        Punto *vertices;
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // POLIGONO_H
