/*******************************************************************************
 *
 * @file                 punto.cpp
 * @brief                Ejercicio Geometria
 * @version              1.00
 * @date                 02/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <math.h>
#include "punto.h"

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
Punto::Punto(double xx, double yy) {
    set_x(xx);
    set_y(yy);
}

Punto::Punto(const Punto &p) {
    set_x(p.get_x());
    set_y(p.get_y());
}

const Punto & Punto::operator=(const Punto &p) {
    if(*this != p) {
        x = p.get_x();
        y = p.get_y();
    }
    return *this;
}

void Punto::operator+=(const Punto &p) {
    x = x + p.get_x();
    y = y + p.get_y();
}

void Punto::operator-=(const Punto &p) {
    x = x - p.get_x();
    y = y - p.get_y();
}

Punto Punto::operator+(const Punto &p) const {
    Punto aux;
    aux.set_x(x + p.get_x());
    aux.set_y(y + p.get_y());
    return aux;
}

Punto Punto::operator-(const Punto &p) const {
    Punto aux;
    aux.set_x(x - p.get_x());
    aux.set_y(y - p.get_y());
    return aux;
}

bool Punto::operator==(const Punto &p) const {
    if(this->x == p.get_x() && this->y == p.get_y())
        return 1;
    return 0;
}

bool Punto::operator!=(const Punto &p) const {
    if(this->x != p.x || this->y != p.y)
        return 1;
    return 0;
}

double Punto::get_x(void) const {
    return x;
}

double Punto::get_y(void) const {
    return y;
}

void Punto::set_x(double xx) {
    x = xx;
}

void Punto::set_y(double yy) {
    y = yy;
}

double Punto::distancia(const Punto &p) const {
    double Xaux, Yaux;
    Xaux = this->x - p.get_x();
    Yaux = this->y - p.get_y();
    return sqrt((Xaux * Xaux) + (Yaux * Yaux));
}
