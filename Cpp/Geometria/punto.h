/*******************************************************************************
 *
 * @file                 punto.h
 * @brief                Ejercicio Geometria
 * @version              1.00
 * @date                 02/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef PUNTO_H
#define PUNTO_H

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <iostream>

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
class Punto {
    public:
        /**
         * @brief Constructor con argumentos
         * @param xx Valor al cual fijar x
         * @param yy Valor al cual fijar y
         */
        Punto(double xx = 0, double yy = 0);

        /**
         * @brief Constructor de copia
         * @param p
         */
        Punto(const Punto &p);

        /**
         * @brief Operador de asignación
         * @param p Punto a asignar
         * @return Referencia al mismo Punto
         */
        const Punto & operator=(const Punto &p);

        /**
         * @brief Operador suma y asignación
         * @param p Punto a sumar al propio Punto
         */
        void operator+=(const Punto &p);

        /**
         * @brief Operador resta y asignación
         * @param p Punto a restar al propio Punto
         */
        void operator-=(const Punto &p);

        /**
         * @brief Operador suma
         * @param p Punto a sumar con el propio
         * @return Nuevo Punto resultante de la suma
         */
        Punto operator+(const Punto &p) const;

        /**
         * @brief Operador resta
         * @param p Punto a restar con el propio
         * @return Nuevo Punto resultante de la resta
         */
        Punto operator-(const Punto &p) const;

        /**
         * @brief Operador comparación por igual
         * @param p Punto con el cual comparar el propio
         * @return Resultado de la comparación
         */
        bool operator==(const Punto &p) const;

        /**
         * @brief Operador comparación por distinto
         * @param p Punto con el cual comparar el propio
         * @return Resultado de la comparación
         */
        bool operator!=(const Punto &p) const;

        /**
         * @brief Obtener componente x
         * @return Componente x del Punto
         */
        double get_x() const;

        /**
         * @brief Obtener componente y
         * @return Componente y del Punto
         */
        double get_y() const;

        /**
         * @brief Fijar componente x
         * @param xx Componente x a fijar del Punto
         */
        void set_x(double xx);

        /**
         * @brief Fijar componente y
         * @param yy Componente y a fijar del Punto
         */
        void set_y(double yy);

        /**
         * @brief Calcula la distancia entre el Punto p y el propio
         * @param p Punto al cual calcular la distancia
         * @return Distancia calculada
         */
        double distancia(const Punto &p) const;

    private:
        /** Componente x del Punto */
        double x;

        /** Componente y del Punto */
        double y;
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // PUNTO_H
