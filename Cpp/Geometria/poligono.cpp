/*******************************************************************************
 *
 * @file                 poligono.cpp
 * @brief                Implementación del Módulo para el seteo de los GPIOs.
 * @version              1.00
 * @date                 02/07/2020
 * @author               Agostino Barbetti
 *
 *******************************************************************************

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "poligono.h"

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
Poligono::Poligono(void) {

}

Poligono::Poligono(unsigned int n_vertices) {
    if(2 < n_vertices) {
        vertices = new Punto[n_vertices];
        if(vertices)
            cantidad_vertices = n_vertices;
    }
    else
        throw ErrorNoDefinePoligono();
}

Poligono::Poligono(const Poligono &p) {
    if(2 < p.cantidad_vertices) {
        vertices = new Punto[p.cantidad_vertices];
        if(vertices) {
            for(unsigned int i = 0; i < p.cantidad_vertices; i++) {
                vertices[i].set_x((p.vertices[i]).get_x());
                vertices[i].set_y((p.vertices[i]).get_y());
            }
            cantidad_vertices = p.cantidad_vertices;
        }
    }
    else
        throw ErrorNoDefinePoligono();
}

Poligono::~Poligono(void) {
    delete vertices;
}

void Poligono::set_vertice(unsigned int n_vertice, const Punto &p) {
    vertices[n_vertice].set_x(p.get_x());
    vertices[n_vertice].set_y(p.get_y());
}

const Punto & Poligono::get_vertice(unsigned int n_vertice) const {
    return vertices[n_vertice];
}

unsigned int Poligono::get_cantidad_vertices(void) const {
    return cantidad_vertices;
}

double Poligono::perimetro(void) const {
    unsigned int i;
    double perimetro = 0;
    for(i = 0; i < cantidad_vertices - 1; i++) {
        perimetro += vertices[i].distancia(vertices[i + 1]);
    }
    perimetro += vertices[i].distancia(vertices[0]);
    return perimetro;
}

double Poligono::area(void) const {
    unsigned int i;
    double area = 0;
    for(i = 0; i < cantidad_vertices - 1; i++) {
        area += (vertices[i].get_x() * vertices[i+1].get_y()
                 - vertices[i+1].get_x() * vertices[i].get_y());
    }
    area += (vertices[i].get_x() * vertices[0].get_y()
             - vertices[0].get_x() * vertices[i].get_y());
    area *= 0.5;
    return area;
}

bool Poligono::operator==(const Poligono &p) const {
    bool igualdad = 0;
    if(p.get_cantidad_vertices() == cantidad_vertices) {
        igualdad = 1;
        for(unsigned int i = 0; i < cantidad_vertices; i++) {
            if(vertices[i].get_x() != p.vertices[i].get_x())
                igualdad = 0;
            if(vertices[i].get_y() != p.vertices[i].get_y())
                igualdad = 0;
        }
    }
    return igualdad;
}

bool Poligono::operator!=(const Poligono &p) const {
    bool igualdad = 0;
    if(p.get_cantidad_vertices() == cantidad_vertices) {
        igualdad = 1;
        for(unsigned int i = 0; i < cantidad_vertices; i++) {
            if(vertices[i].get_x() != p.vertices[i].get_x())
                igualdad = 0;
            if(vertices[i].get_y() != p.vertices[i].get_y())
                igualdad = 0;
        }
    }
    return !igualdad;
}
