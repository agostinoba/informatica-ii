/*******************************************************************************
 *
 * @file                 rectangulo.h
 * @brief                Ejercicio Geometria
 * @version              1.00
 * @date                 02/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef RECTANGULO_H
#define RECTANGULO_H

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "punto.h"
#include "poligono.h"

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
class Rectangulo: public Poligono {
    public:
        Rectangulo(const Punto &, unsigned int, unsigned int);
        Rectangulo(const Rectangulo &);
        ~Rectangulo(void);

        void set_vertice_inicial(const Punto &);
        void set_ancho(unsigned int);
        void set_alto(unsigned int);

        const Punto & get_vertice_inicial() const;
        unsigned int get_ancho() const;
        unsigned int get_alto() const;

        double perimetro() const;
        double area() const;

        void set_vertice(const Punto &) = delete;

        bool operator==(const Rectangulo &) const;
        bool operator!=(const Rectangulo &) const;

    private:
        void calc_vertices(void);

        Punto vertice_inicial;
        unsigned int ancho;
        unsigned int alto;
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // RECTANGULO_H
