/*******************************************************************************
 *
 * @file                 main.cpp
 * @brief                Ejercicio Geometria
 * @version              1.00
 * @date                 02/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <iostream>
#include "stdio.h"
#include "punto.h"
#include "poligono.h"
#include "rectangulo.h"

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
ostream & operator<<(ostream &o, const Punto &p) {
    o << '(' << p.get_x()<< ", " << p.get_y() << ')';
    return o;
}

ostream & operator<<(ostream &o, const Poligono &p) {
    for(unsigned int i = 0; i < p.get_cantidad_vertices(); i++)
        cout << "Vertice " << i << " : (" << p.get_vertice(i).get_x()
             << ", " << p.get_vertice(i).get_y() << ")" << endl;
    return o;
}

ostream & operator<<(ostream &o, const Rectangulo &r) {
    cout << Poligono{r} << endl;
    cout << "Perimentro: " << r.perimetro() << endl;
    cout << "Area: " << r.area() << endl;
    return o;
}

int main(void) {
    // Consigna 1
//    Punto a{1,1}, b, c, d, e;
//
//    d = c = b = a;
//
//    b += a;
//    c -= a;
//
//    d = a + b - c;
//    e = d - d;
//
//    cout << "Punto a: " << a << endl;
//    cout << "Punto b: " << b << endl;
//    cout << "Punto c: " << c << endl;
//    cout << "Punto d: " << d << endl;
//    cout << "Punto e: " << e << endl << endl;
//
//    cout << "Distancia del punto d al origen: " << Punto{}.distancia(d)
//         << endl << endl;
//
//    if(a == b)
//        cout << "El punto a es igual al punto b" << endl;
//    else
//        cout << "El punto a es distinto al punto b" << endl;
//
//    if(c == e)
//        cout << "El punto c es igual al punto e" << endl;
//    else
//        cout << "El punto c es distinto al punto e" << endl;

    // Consigna 2
//    Poligono triang{3};
//
//    triang.set_vertice(0, Punto{1});
//    triang.set_vertice(1, Punto{0, 1});
//    triang.set_vertice(2, Punto{-1});
//
//    Poligono triang_2{triang};
//
//    cout << "Perimentro del triangulo: " << triang.perimetro() << endl;
//    cout << "Area del triangulo: " << triang.area() << endl;
//
//    if(triang == triang_2)
//        cout << "Ambos triangulos son iguales." << endl;
//    else
//        cout << "Ambos triangulos son distintos." << endl;

    // Consigna 3
    Rectangulo r{Punto{1, 1}, 1, 2};
    Rectangulo aux{r};

    cout << "Rectangulo: " << endl << r;

    r.set_vertice_inicial(Punto{10, 10});

    cout << "Rectangulo: " << endl << r;

    r.set_alto(10);

    cout << "Rectangulo: " << endl << r;

    if(r == aux)
        cout << "Los rectangulos son iguales." << endl;
    else
        cout << "Los rectangulos son distintos." << endl;

    return 0;
}
