/*******************************************************************************
 *
 * @file                 rectangulo.cpp
 * @brief                Ejercicio Geometria
 * @version              1.00
 * @date                 02/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "rectangulo.h"

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
Rectangulo::Rectangulo(const Punto &p, unsigned int w, unsigned int h) {
    vertices = new Punto[4];
    if(vertices) {
        cantidad_vertices = 4;
        vertices[0] = p;
        vertices[1].set_x(p.get_x() + w);
        vertices[1].set_y(p.get_y());
        vertices[2].set_x(vertices[1].get_x());
        vertices[2].set_y(vertices[1].get_y() - h);
        vertices[3].set_x(p.get_x());
        vertices[3].set_y(p.get_y() - h);
        vertice_inicial = p;
        ancho = w;
        alto = h;
    }
}

Rectangulo::Rectangulo(const Rectangulo &r) {
    vertices = new Punto[4];
    if(vertices) {
        for(unsigned int i = 0; i < 4; i++) {
            vertices[i].set_x((r.vertices[i]).get_x());
            vertices[i].set_y((r.vertices[i]).get_y());
        }
        set_vertice_inicial(r.get_vertice_inicial());
        set_alto(r.get_alto());
        set_ancho(r.get_alto());
    }
}

Rectangulo::~Rectangulo(void) {

}

void Rectangulo::set_vertice_inicial(const Punto &p) {
    cout << endl;
    cout << "Set_vertice_inicial" << endl;
    cout << "X: " << p.get_x() << endl;
    cout << "Y: " << p.get_y() << endl;
    cout << endl;
    vertice_inicial = p;
}

void Rectangulo::set_ancho(unsigned int w) {
    ancho = w;
}

void Rectangulo::set_alto(unsigned int h) {
    alto = h;
}

const Punto & Rectangulo::get_vertice_inicial() const {
    return vertice_inicial;
}

unsigned int Rectangulo::get_ancho() const {
    return ancho;
}

unsigned int Rectangulo::get_alto() const {
    return alto;
}

double Rectangulo::perimetro() const {
    return (2*(ancho + alto));
}

double Rectangulo::area() const {
    return (ancho * alto);
}

bool Rectangulo::operator==(const Rectangulo &r) const {
    bool igualdad = 1;
    if(alto != r.alto)
        igualdad = 0;
    if(ancho != r.ancho)
        igualdad = 0;
    return igualdad;
}

bool Rectangulo::operator!=(const Rectangulo &r) const {
    bool igualdad = 1;
    if(alto != r.alto)
        igualdad = 0;
    if(ancho != r.ancho)
        igualdad = 0;
    return !igualdad;
}
