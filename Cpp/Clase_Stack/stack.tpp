/*******************************************************************************
 *
 * @file                 stack.tpp
 * @brief                Implementación del Módulo para el seteo de los GPIOs.
 * @version              1.00
 * @date                 26/05/2020
 * @author               Agostino Barbetti
 *
 *******************************************************************************

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef STACK_TPP
#define STACK_TPP

// El tipo de archivo .tpp se usar para los templates.
// Despues lo agrego al final del stack.h

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "stack.h"

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
template<class T>
Stack<T>::Stack(void) {
    inicio = nullptr;
}

template<class T>
void Stack<T>::push(const T &d) {
    Nodo *p = new Nodo(d);

    if(vacio()) {
        inicio = p;
    }
    else {
        Nodo *a = ultimo();
        a->next = p;
    }
}

template<class T>
T Stack<T>::pop() {
    if(vacio())
        throw ErrorStackVacio();
    if(inicio->next == nullptr) {
        T ret = inicio->data;
        delete inicio;
        inicio = nullptr;
        return ret;
    }
    else {
        Nodo *a = ultimo();
        Nodo *p = inicio;
        while(p->next != a)
            p = p->next;
        T ret = a->data;
        delete a;
        p->next = nullptr;
        return ret;
    }
}

template<class T>
typename Stack<T>::Nodo * Stack<T>::ultimo() const {
    Nodo *a = inicio;
    while(a->next != nullptr)
        a = a->next;
    return a;
}

template<class T>
Stack<T>::~Stack(void) {
    if(!vacio()) {
        Nodo *p = inicio->next;
        while(p != nullptr) {
            delete inicio;
            inicio = p;
            p = p->next;
        }
        delete inicio;
    }
}

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // STACK_TPP
