/*******************************************************************************
 *
 * @file                 stack.h
 * @brief                Módulo para el seteo de puertos y pines.
 * @version              1.00
 * @date                 26/05/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef STACK_H
#define STACK_H

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
// template puede recibir más de un tipo. Supongo que se ultiliza para definir
// varios tipos dentro de la clase - template<class T, class Z, int>
template<class T>
class Stack {
    public:
    Stack(void);
    ~Stack(void);

    void push(const T &d);
    T pop(void);

    class ErrorStackVacio {

    };

    private:
        class Nodo {
        public:
            Nodo(const T &d) : data{d}, next{nullptr} {}
            T data;
            Nodo *next;
        };
        bool vacio(void) {
            return (inicio == nullptr);
        }
        Nodo *ultimo() const;
        Nodo *inicio;
};

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <stack.tpp>

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // STACK_H
