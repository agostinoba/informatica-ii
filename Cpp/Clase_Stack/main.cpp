/*******************************************************************************
 *
 * @file                 main.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 26/05/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/
#include <iostream>
#include <stack.h>

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
int main(void) {
    Stack<int> s;

    try {
        s.push(10);
    }
    catch (Stack<int>::ErrorStackVacio & e) {
        cout << "Se de Stack vacio" << endl;
    }
    catch(...) {
        cout << "Se detectó otro tipo de error desconocido" << endl;
    }

    cout << s.pop() << endl;
}
