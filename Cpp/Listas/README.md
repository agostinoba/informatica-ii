# Ejercicio 4 - Array dinámico inteligente (Lista)

Se propone desarrollar una clase que se ocupe de manejar una memoria que pueda
cambiar su tamaño dinámicamente, capacidad que suele necesitarse em pácticamente
cualquier software de complejidad baja/media. La estrategia que utilizaremos
para el desarrollo, será la de una *lista doblemente enlazada*.

## 4.1 - Definición de la clase e implementación del constructor básico

Definir la clase con el nombre **Lista** la cuál será una *clase template*. La
misma deberá implementar en primer lugar un único constructor sin parámetros.

Se deberá definir también en la clase **Lista**, una *clase privada* llamada
**Nodo** la cuál constará de tres miembros *públicos*:

- data -> Tipo de dato del *template*
- next -> Puntero a otro objeto **Nodo**
- prev -> Puntero a otro objeto **Nodo**

Y tendrá un único constructor, al cuál se le pasará como único argumento una
*referencia constante al tipo de dato del template* el cuál tendrá que asignar
al campo *data*.

La clase **Lista** tendrá como miembros, dos punteros a la clase **Nodo** uno
llamado *inicio* y otro *fin*, y un entero sin signo llamado
*cantidad_elementos* y en su constructor (el cuál no recibirá ningún parámetro)
deberá inicializar dichos punteros a el puntero nulo *nullptr* y la variable
contadora de elementos a cero.

Para corroborar que la definición de la clase es correcta, compilar el siguiente
*main*:

```
#include <lista.h>

int main(void) {
    Lista<int> l;
    return 0;
}
```

El mismo debería compilar sin errores, y si se ejecuta no debería hacer nada.

## 4.2 - Destrucción de la clase

Implementar el destructor de la clase **Lista**. Tener en cuenta que a medida
que se vayan insertando *Nodos* en la misma, se tendrán todos los nodos
vinculados dinámicamente, por lo que se deben liberar sucesivamente cada uno de
ellos.

## 4.3 - Métodos de asignación

Se implementarán los métodos *prepend y append* de la forma que se explica a
continuación:

- *append*: Se pasará como argumento una *referencia constante al tipo de dato
del template*. Se creará un **Nodo**, el cuál se colocará al final de la lista.
- *prepend*: Se pasará como argumento una *referencia constate al tipo de dato
del template*. Se creará un **Nodo**, el cuál se colocará al inicio de la lista.

Ambos métodos deberán incrementar el contador de cantidad de elementos del
objeto. Se debe tener especial cuidado a la hora de agregar un **Nodo** a la
lista en cualquiera de los dos métodos, ya que los miembros *inicio* y *fin* de
la **Lista** deben quedar apuntando a los **Nodos** correctos, como así también
los miembros *next* y *prev* del nuevo **Nodo** creado.

## 4.4 - Obtención y modificación de elementos

Implemente el método *size* el cuá devuelva la cantidad actual de elementos en
la **Lista**. Luego, implemente el *operador indexación* que admita como único
argumento un *entero con signo*. La indexación con un índice positivo deberá
funcionar como es usual, devolviendo una *referencia no constante a un objeto
del tipo del template*. En caso de que la indexación se realice con un índice
negativo, se deberá devolver una *referencia no constante a un objeto del tipo
del template* ubicado a |*índice* + 1| lugares **desde el final de la Lista**.
Al devolver una *referencia no constante a un objeto del tipo template*, se
puede utilizar el mismo métodopara modificar elementos de la **Lista**.

En caso de que el elemento que se intenta indexar (sea con un índice positivo
o negativo) excedaal tamaño de la lista, se deberá arrojar un *error*, para lo
cuál se deberá definir una clase pública **FueraDeRango** en la clase **Lista**.
La clase **FueraDeRango** no debe tener ningún miembro ni método. Para arrojar
un *error* con la clase **FueraDeRango**, utilizar la siguiente línea de código:

```
throw FueraDeRango();
```

Para corroborar el correcto funcionamiento de la clase **Lista** hasta este
punto, utilizar el siguiente *main*:

```
#include <lista.h>
#include <iostream>

using namespace std;

int main(void) {
    Lista<int> l;
    
    l.append(10);
    l.append(100);
    cout << l[0] << endl;
    l.prepend(5);
    cout << l[0] << endl;
    cout << l[-1] << endl;
    l[-1] = 4;
    cout << l[-1] << endl;
    cout << l[10] << endl;
    return 0;
}
```

Del cuál debería obtenerse la siguiente salida:

        10
        5
        100
        4
        terminate called after throwing an instance of 'Lista<int>::FueraDeRango'
    
## 4.5 - Remoción de elementos

Para manipular la lista de forma dinámica completamente, hace falta alguna forma
de eliminar elementos en la lista con cierto control. Para cumplir con dicho
cometido, se propone implementar el método *remove*, el cuál recibirá como
argumento un *entero con signo* y deberá eliminar (liberando la memoria
correspondiente para prevenir *memory leaks*) el **Nodo** en el índice pasado
como argumento, siguiendo la misma política que con el *operador indexación*
para el manejo del signo y rango del índice. Tener especial cuidado con los
miembros *inicio* y *fin* de la clase **Lista**. A su vez también implementar el
método *clear* el cuál no recibe parámetro alguno y se encarga de eliminar todos
los nodos de la lista.

A fines de probar el correcto funcionamiento de la clase, utilizar el siguiente
*main*:

```
#include <lista.h>
#include <iostream>

using namespace std;

int main(void) {
    Lista<int> l;
    
    l.append(10);
    l.append(100);
    cout << l[0] << endl;
    l.prepend(5);
    cout << l[0] << endl;
    cout << l[-1] << endl;
    l[-1] = 4;
    cout << l[-1] << endl;
    l.remove(-1);
    cout << l[-1] << endl;
    l.clear();
    cout << l[0] << endl;
    return 0;
}
```

Del cuál debería obtenerse la siguiente salida:

        10
        5
        100
        4
        10
        terminate called after throwing an instance of 'Lista<int>::FueraDeRango'
    
## 4.6 - Preguntas teóricas

- Se propone implementar un método *search* para la clase **Lista** con el
siguiente prototipo sugerido:

```
bool search(const T &item, int &idx, bool from_start = true);
```

En donde se retorna *true* en caso de existir un índice en el cuál se haya
encontrado un elemento igual al parámetro *ítem* en la **Lista**, guardando en
el parámetro *idx* el índice donde se encuentra el mismo. El parámetro
*from_start* es únicamente para indicar si se desea hacer la búsqueda desde el
principio o desde el final. En caso de no existir un elemento en la **Lista**
igual al elemento *ítem*, se retorna *false*. ¿Qué cuidados se deben tomar?
¿Qué errores se podrían arrojar en este método?

- Se propone implementar el método *operador desplazamiento a izquierda* a la
clase **Lista**. ¿Qué requerimientos debería cumplir el *tipo de dato de
template* de la misma para que sea viable la implementación?  
- ¿Qué operador/es implementaría para la clase **Lista**? ¿Por qué o qué
  utilidad le/s encuentra?
