/*******************************************************************************
 *
 * @file                 lista.tpp
 * @brief                Implementación del Módulo para el seteo de los GPIOs.
 * @version              1.00
 * @date                 30/06/2020
 * @author               Agostino Barbetti
 *
 *******************************************************************************

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "lista.h"

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
template<class T>
Lista<T>::Lista(void) {
    cantidad_elementos = 0;
    inicio = nullptr;
    fin = nullptr;
}

template<class T>
Lista<T>::~Lista(void) {
    if(!(nullptr == inicio)) {
        Nodo *p = inicio->next;
        while(p != nullptr) {
            delete inicio;
            inicio = p;
            p = p->next;
        }
        delete inicio;
    }
}

template<class T>
int Lista<T>::append(const T &d) {
    Nodo *p = new Nodo(d);
    if(nullptr != p) {
        if(nullptr != fin) {
            fin->next = p;
            p->prev = fin;
        }
        else {
            p->prev = nullptr;
            inicio = p;
        }
        p->next = nullptr;
        fin = p;
        cantidad_elementos++;
        return 1;
    }
    else
        return -1;
}

template<class T>
int Lista<T>::prepend(const T &d) {
    Nodo *p = new Nodo(d);
    if(nullptr != p) {
        if(nullptr != inicio) {
            p->next = inicio;
            inicio->prev = p;
        }
        else {
            p->next = nullptr;
            fin = p;
        }
        p->prev = nullptr;
        inicio = p;
        cantidad_elementos++;
        return 1;
    }
    else
        return -1;
}

template<class T>
unsigned int Lista<T>::size(void) {
    return cantidad_elementos;
}

template<class T>
int Lista<T>::remove(int indice) {
    int aux = 0;
    Nodo *p = inicio;
    if((indice <= cantidad_elementos) || (-indice <= cantidad_elementos)) {
        if(nullptr != inicio) {
            if(0 <= indice){
                for(int i = indice; 0 < i; i--){
                    p = p->next;
                }
            }
            else {
                p = fin;
                for(int i = indice; i < -1; i++){
                    p = p->prev;
                }
            }

            if(p == inicio) {
                inicio = p->next;
                (p->next)->prev = nullptr;
            }
            if(p == fin) {
                fin = p->prev;
                (p->prev)->next = nullptr;
            }
            else {
                (p->prev)->next = p->next;
                (p->next)->prev = p->prev;
            }
            delete p;
            cantidad_elementos--;
            aux = 1;
        }
        else
            aux = -1;
    }
    else
        throw FueraDeRango();
    return aux;
}

template<class T>
int Lista<T>::clear(void) {
    int aux = 0;
    if(!(nullptr == inicio)) {
        Nodo *p = inicio->next;
        while(p != nullptr) {
            delete inicio;
            inicio = p;
            p = p->next;
        }
        delete inicio;
        aux = 1;
        cantidad_elementos = 0;
    }
    else
        aux = -1;
    return aux;
}

template<class T>
T & Lista<T>::operator[](int indice) {
    Nodo *p = inicio;
    if((indice < cantidad_elementos) || (-indice < cantidad_elementos)) {
        if(nullptr != inicio) {
            if(0 <= indice){
                for(int i = indice; 0 < i; i--){
                    p = p->next;
                }
            }
            else {
                p = fin;
                for(int i = indice; i < -1; i++){
                    p = p->prev;
                }
            }
        }
    }
    else
        throw FueraDeRango();
    return p->data;
}
