/*******************************************************************************
 *
 * @file                 main.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 30/06/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <iostream>
#include <lista.h>

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
int main(void) {
    Lista<int> l;

    l.append(10);
    l.append(100);

    cout << l[0] << endl;

    l.prepend(5);

    cout << l[0] << endl;
    cout << l[-1] << endl;

    l[-1] = 4;

    cout << l[-1] << endl;

    l.remove(-1);

    cout << l[-1] << endl;

    l.clear();

    cout << l[0] << endl;

    return 0;
}
