/*******************************************************************************
 *
 * @file                 persona.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 04/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "persona.h"

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
Persona::Persona() {

}

Persona::Persona(char* nombre, char* apellido, int dni) {
    SetNombre(nombre);
    SetApellido(apellido);
    SetDNI(dni);
}

Persona::~Persona(void) {

}

void Persona::SetNombre(char* nombre) {
    int i;
    for(i = 0; i < 19 && nombre[i] != '\0'; i++) {
        Nombre[i] = nombre[i];
    }
    Nombre[i] = '\0';
}

void Persona::SetApellido(char* apellido) {
    int i;
    for(i = 0; i < 19 && apellido[i] != '\0'; i++) {
        Apellido[i] = apellido[i];
    }
    Apellido[i] = '\0';
}

void Persona::SetDNI(int dni) {
    DNI = dni;
}

const char* Persona::GetNombre(void) const {
    return Nombre;
}

const char* Persona::GetApellido(void) const {
    return Apellido;
}

int Persona::GetDNI(void) const {
    return DNI;
}

std::ostream& operator<<(std::ostream& out, Persona& z) {
    out << "Nombre: "   << z.GetNombre()    << endl;
    out << "Apellido: " << z.GetApellido()  << endl;
    out << "DNI: "      << z.GetDNI();
    return out;
}
