/*******************************************************************************
 *
 * @file                 persona.h
 * @brief                Módulo para el seteo de puertos y pines.
 * @version              1.00
 * @date                 04/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef PERSONA_H
#define PERSONA_H

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <iostream>
#include <stdio.h>

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
class Persona {
    public:
        Persona();
        Persona(char* nombre, char* apellido, int dni);
        ~Persona(void);

        void SetNombre(char*);
        void SetApellido(char*);
        void SetDNI(int);

        const char* GetNombre(void) const;
        const char* GetApellido(void) const;
        int GetDNI(void) const;

        friend std::ostream& operator<<(std::ostream&, Persona&);

    private:
        char Nombre[20];
        char Apellido[20];
        int DNI;
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // PERSONA_H
