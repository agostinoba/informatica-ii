# Ejercicio 2 - Sistema de notas de alumnos

## 2.1 - Definición básica de una persona

Para comenzar con la aplicación propuesta, primero definir una clase
**Persona**. Asumiremos que los campos que son de nuestro interés para definir
tal clase son:

- Nombre
- Apellido
- DNI

A fines de simplificar el problema, considerar que tanto los campos *nombre* y
*apellido* no pueden superar los 19 caracteres. Implementar un único constructor
para la clase **Persona** al cuál se pasen como argumento los tres parámetros
necesarios. En caso de que el nombre o el apellido pasados como argumentos
excedan los 19 caracteres, truncar el mismo.  
**NOTA: No utilizar la biblioteca <string.h>**.

## 2.2 - Extensión de una persona

Para presentar mayor funcionalidad, extender la clase **Persona** realizada en
el hito anterior, de forma tal de agregar métodos para obtener los miembros
*nombre, apellido, dni* y los necesarios para escribir los miembros *nombre y
apellido*.  
**NOTA: No tendría sentido modificar el DNI de una Persona una vez se le asignó
uno.**

A su vez, implementar el *operador desplazamiento a izquierda* para que al
utilizarlo de la siguiente forma:

```
Persona p{"Augusto", "Santini", 37993040};
cout << p << endl;
```

En la cola aparezca el siguiente resultado:

       Nombre: Augusto
       Apellido: Santini
       DNI: 37993040

## 2.3 - Implementación de un alumno a partir de una persona

Utilizando la clase **Persona** como *clase base*, implementar la clase
**Alumno** el cuál herede los miembros *nombre y apellido*, y agregue como
miembro propio el miembro *legajo*. Considerar el *legajo* como un entero sin
signo. Al igual que con la clase **Persona**, la clase **Alumno** deberá tener
un único constructor, al cuál se le deba pasar como parámetros un *nombre,
apellido, dni y legajo*.

Agregar un nuevo miembro a la clase **Alumno** que sea un array de *10 enteros
con signo* que represente las *notas* del **Alumno**. Al momento de instanciar
un objeto de la clase *Alumno*, inicializar todas las *notas* del mismo con el
valor *-1*. Implementar el método *asignar_nota* que reciba como parámetro dos
*enteros sin signo*, uno indicando qué nota se desea modificar, y el otro con
el contenido de la nota. En caso de que el índice de la nota a modificar sea
mayor al límite, truncar el mismo modificando la última nota disponible.
También implementar un método *invalidar_nota* que reciba como argumento un
único *entero sin signo* que utilice para seleccionar qué nota se desar
invalidar.

Reimplementar el operador *desplazamiento a izquierda* de forma tal de que al
utilizarlo de la siguiente forma:

```
Alumno a{"Augusto", "Santini", 37993040, 1491970};
cout << a << endl;
a.asignar_nota(0, 5);
a.asignar_nota(5, 7);
cout << a << endl;

```

Obtenga la siguiente salida:

    Nombre y apellido: Augusto Santini
    Legajo: 1491970
    Notas: El alumno no contiene notas válidas.

    Nombre y apellido: Augusto Santini
    Legajo: 1491970
    Notas: [5] [7]

Nótese que las notas inválidas no son mostradas aunque las notas válidas no se
encuentren en índices consecutivos, y que en caso de que no exista ninguna nota
válida, se le informa al usuario de dicho evento.

## 2.4 - Obtención de particularidades acerca de las notas del alumno

Implementar los siguientes métodos para la clase **Alumno**:

```
double promedio(bool *ok = nullptr);
unsigned int nota_minima(bool *ok = nullptr);
unsigned int nota_maxima(bool *ok = nullptr);
```

El parámetro *ok* en cada uno de estos métodos sirve para informar al usuario en
caso de que no se contenga ninguna nota válida. En caso de que el objeto
**Alumno** tenga notas válidas, se deberá asignar al contenido del puntero *ok*
el valor *true* y en caso de que no tenga ninguna nota válida, se deberá asignar
el valor *false*. Se le provee al usuario del método la posibilidad de no pasar
ningún parámetro, caso en el cuál asume que el resultado obtenido es correcto.
Extender también el *operador desplazamiento izquierda* para que en caso de
tener notas válidas, informe también el promedio.

Para corroborar que las implementaciones son correctas, utilizar el mismo main
del hito anterior, del cuál se debería obtener la siguiente salida:

        Nombre y apellido: Augusto Santini
        Legajo: 1491970
        Notas: El alumno no contiene notas válidas.

        Nombre y apellido: Augusto Santini
        Legajo: 1491970
        Notas: [5] [7]
        Promedio: 6

## 2.5 - Preguntas teóricas

- ¿Tendría sentido implementar el *operador comparación* para la clase
**Persona** o **Alumno**? ¿Por qué?
- La clase **Alumno** serviría para implementar una clase **Curso**. ¿Cómo
implementaría dicha clase? ¿Qué métodos mínimos implementaría?
