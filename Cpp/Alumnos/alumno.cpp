/*******************************************************************************
 *
 * @file                 alumno.cpp
 * @brief                Implementación del Módulo para el seteo de los GPIOs.
 * @version              1.00
 * @date                 04/07/2020
 * @author               Agostino Barbetti
 *
 *******************************************************************************

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "alumno.h"

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
Alumno::Alumno(char *nombre, char *apellido, int dni, unsigned int legajo) {
    SetNombre(nombre);
    SetApellido(apellido);
    SetDNI(dni);
    SetLegajo(legajo);
    for(int i = 0; i < 10; i++)
        Nota[i] = -1;
}

Alumno::~Alumno() {

}

void Alumno::SetLegajo(unsigned int legajo) {
    Legajo = legajo;
}

void Alumno::Asignar_Nota(unsigned int indice, unsigned int nota) {
    if(11 > indice)
        Nota[indice] = nota;
    else
        Nota[9] = nota;
}

void Alumno::Invalidar_Nota(unsigned int indice) {
    if(11 > indice)
        Nota[indice - 1] = -1;
    else
        cout << "El indice de la nota" << endl;
}

unsigned int Alumno::GetLegajo() const {
    return Legajo;
}

double Alumno::Promedio(bool *ok) {
    int invalidas = 0;
    double promedio = 0;
    for(int i = 0; i < 10; i++) {
        if(0 > Nota[i])
            invalidas++;
        else
            promedio += Nota[i];
    }
    if(10 == invalidas)
        *ok = false;
    else {
        *ok = true;
        promedio = promedio / (10 - invalidas);
    }
    return promedio;
}

unsigned int Alumno::Nota_Minima(bool *ok) {
    int invalidas = 0;
    int minima = 11;
    for(int i = 0; i < 10; i++) {
        if(0 > Nota[i])
            invalidas++;
        else {
            if(Nota[i] < minima)
                minima = Nota[i];
        }
    }
    if(invalidas == 10)
        *ok = false;
    else
        *ok = true;
    return minima;
}

unsigned int Alumno::Nota_Maxima(bool *ok) {
    int invalidas = 0;
    int maxima = 0;
    for(int i = 0; i < 10; i++) {
        if(0 > Nota[i])
            invalidas++;
        else {
            if(Nota[i] > maxima)
                maxima = Nota[i];
        }
    }
    if(invalidas == 10)
        *ok = false;
    else
        *ok = true;
    return maxima;
}

std::ostream& operator<<(std::ostream& out, Alumno &z) {
    int invalidas = 0;
    bool check;
    double promedio;
    int minima = 0;
    int maxima = 0;
    out << "Nombre y Apellido: " << z.GetNombre() << " " << z.GetApellido()
        << endl;
    out << "Legajo: " << z.GetLegajo() << endl;
    out << "Notas: ";
    for(int i = 0; i < 10; i++) {
        if(0 > z.Nota[i])
            invalidas++;
        else
            out << "[" << z.Nota[i] << "]";
    }
    out << endl;
    if(invalidas == 10)
        out << "El alumno no contiene notas validas.";
    promedio = z.Promedio(&check);
    if(check)
        out << "Promedio: " << promedio;
    out << endl;
    minima = z.Nota_Minima(&check);
    if(check)
        out << "Minima: " << minima;
    out << endl;
    maxima = z.Nota_Maxima(&check);
    if(check)
        out << "Maxima: " << maxima;
    out << endl;
    return out;
}
