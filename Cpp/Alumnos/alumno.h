/*******************************************************************************
 *
 * @file                 alumno.h
 * @brief                Módulo para el seteo de puertos y pines.
 * @version              1.00
 * @date                 04/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef ALUMNO_H
#define ALUMNO_H

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "persona.h"

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
class Alumno : public Persona {
    public:
        Alumno(char *, char *, int, unsigned int);
        ~Alumno();

        void SetLegajo(unsigned int);
        void Asignar_Nota(unsigned int, unsigned int);
        void Invalidar_Nota(unsigned int);

        unsigned int GetLegajo() const;

        double Promedio(bool *ok = nullptr);
        unsigned int Nota_Minima(bool *ok = nullptr);
        unsigned int Nota_Maxima(bool *ok = nullptr);

        friend std::ostream& operator<<(std::ostream&, Alumno &);

    private:
        unsigned int Legajo;
        int Nota[10];
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // ALUMNO_H
