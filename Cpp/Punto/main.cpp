/*******************************************************************************
 *
 * @file                 main.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 14/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "punto.h"
#include "intarr.h"

/*******************************************************************************
 *** DEFINES PRIVADAS AL MODULO
 ******************************************************************************/
#define PRESS_KEY cout << "\nPresione Enter para continuar...\n"; cin.get();

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
void ff(void) {
    Punto p, q, w;
    Punto h(34);
    Punto r = h;

    cout << "ff. Puntos creados: "  << Punto::getCantCreada()       <<
            " - Existentes: "       << Punto::getCantExistente()    << endl;
}

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
int main(void) {
/*
    // --------------------------    Consigna 1    -------------------------
    cout << "------------------    Consigna 1    -----------------" << endl;
    Punto p(3000.12, 4.45);
    Punto r;

    cout << "1. Punto p: (" << p.getX() << "; " << p.getY() << ")" << endl;
    cout << "2. Punto r: (" << r.getX() << "; " << r.getY() << ")" << endl;

    r.setX(-2000.22);
    r.setY(3.33);

    cout << "3. Punto r: (" << r.getX() << "; " << r.getY() << ")" << endl;

    p.setPunto(9900.0, 8800.8);

    cout << "4. Punto p: (" << p.getX() << "; " << p.getY() << ")" << endl;

    r = p.getPunto();

    cout << "5. Punto r: (" << r.getX() << "; " << r.getY() << ")" << endl;

    PRESS_KEY;

    // --------------------------    Consigna 3    -------------------------
    cout << endl;
    cout << "------------------    Consigna 3    -----------------" << endl;

    Punto a(1234.56);
    Punto b(12, 34);

    cout << "6. Punto a: (" << a.getX() << "; " << a.getY() << ")" << endl;
    cout << "7. Punto b: (" << b.getX() << "; " << b.getY() << ")" << endl;

    a.setY(3.33);
    b.setPunto(a);

    cout << "8. Punto b: (" << b.getX() << "; " << b.getY() << ")" << endl;

    PRESS_KEY;

    // --------------------------    Consigna 4    -------------------------
    cout << endl;
    cout << "------------------    Consigna 4    -----------------" << endl;

    Punto c(12.34, -56.78);
    Punto d(c);
    Punto e = c;

    cout << "9. Punto c: (" << c.getX() << "; " << c.getY() << ")" << endl;
    cout << "10. Punto d: (" << d.getX() << "; " << d.getY() << ")" << endl;
    cout << "11. Punto e: (" << e.getX() << "; " << e.getY() << ")" << endl;

    PRESS_KEY;

    // --------------------------    Consigna 5    -------------------------
    cout << endl;
    cout << "------------------    Consigna 5    -----------------" << endl;

    Punto g(1, 4);
    Punto h;

    cout << "12. Punto c: (" << c.getX() << "; " << c.getY() << ")" << endl;
    cout << "13. Punto g: (" << g.getX() << "; " << g.getY() << ")" << endl;

    h = c + g;

    cout << "14. Punto h = c + g: (" << h.getX() << "; " << h.getY() << ")"
    << endl;

    h = c - g;

    cout << "15. Punto h = c - g: (" << h.getX() << "; " << h.getY() << ")"
    << endl;

    Punto i(990, -990);

    cout << "16. Punto i: (" << i.getX() << "; " << i.getY() << ")" << endl;

    h = i + c;

    cout << "17. Punto h = i + c: (" << h.getX() << "; " << h.getY() << ")"
    << endl;

    h = g + 47;

    cout << "18. Punto h = g + 47: (" << h.getX() << "; " << h.getY() << ")"
    << endl;

    PRESS_KEY;

    // --------------------------    Consigna 6    -------------------------
    cout << endl;
    cout << "------------------    Consigna 6    -----------------" << endl;

    Punto c(12.34, -56.78);
    Punto j, k;

    j = 78 + c;
    k = 78 - c;

    cout << "19. Punto c: (" << c.getX() << "; " << c.getY() << ")" << endl;
    cout << "20. Punto j = 78 + c: (" << j.getX() << "; " << j.getY() << ")"
    << endl;
    cout << "21. Punto k = 78 - c: (" << k.getX() << "; " << k.getY() << ")"
    << endl;

    k = c + j - 45;

    cout << "22. Punto k = c + j - 45: (" << k.getX() << "; " << k.getY() << ")"
    << endl;

    PRESS_KEY;

    // --------------------------    Consigna 7    -------------------------
    cout << endl;
    cout << "------------------    Consigna 7    -----------------" << endl;

    Punto c(12.34, -56.78);
    double l = 45;

    cout << "23. Punto c: " << c << endl;
    cout << "24. Punto l: " << l << endl;

    cout << "Ingrese valor del punto" << endl;
    Punto m;
    cin >> m;

    cout << "25. Punto m: " << m << "(*)" << endl;
    cout << endl;

    cout << "26. Es m igual a c     ? : " << ((m == c) ? "si" : "no") << endl;
    cout << "27. Es m distinto a c  ? : " << ((m != c) ? "si" : "no") << endl;
    cout << "28. Es m mayor a c     ? : " << ((m > c)  ? "si" : "no") << endl;
    cout << "29. Es m menor a c     ? : " << ((m < c)  ? "si" : "no") << endl;

    cout << "30. Es m igual a l     ? : " << ((m == l) ? "si" : "no") << endl;
    cout << "31. Es m distinto a l  ? : " << ((m != l) ? "si" : "no") << endl;
    cout << "32. Es m mayor a l     ? : " << ((m > l)  ? "si" : "no") << endl;
    cout << "33. Es m menor a l     ? : " << ((m < l)  ? "si" : "no") << endl;

    cout << endl;

    PRESS_KEY;

    // --------------------------    Consigna 8    -------------------------
    cout << endl;
    cout << "------------------    Consigna 8    -----------------" << endl;

    cout << "34. Puntos creados: "  << Punto::getCantCreada()       <<
            " - Existentes: "       << Punto::getCantExistente()    << endl;

    Punto n(12.34, -56.78);

    cout << "35. Puntos creados: "  << n.getCantCreada()            <<
            " - Existentes: "       << n.getCantExistente()         << endl;

    Punto o(n);

    cout << "36. Puntos creados: "  << Punto::getCantCreada()       <<
            " - Existentes: "       << Punto::getCantExistente()    << endl;

    ff();

    cout << "37. Puntos creados: "  << Punto::getCantCreada()       <<
            " - Existentes: "       << Punto::getCantExistente()    << endl;

    PRESS_KEY;

    // --------------------------    Consigna 9    -------------------------
    cout << endl;
    cout << "------------------    Consigna 9    -----------------" << endl;

    cout << "38. Rango de punto: "  << Punto::getLimiteInf()        <<
            ":"                     << Punto::getLimiteSup()        << endl;

    Punto q(3000.12, 5000);
    Punto s(12.34, 34.56);

    cout << "39. Punto q: " << q << endl;
    cout << "40. Punto s: " << s << endl;

    Punto::setLimites(50, 85);

    cout << "41. Rango de punto: "  << q.getLimiteInf()             <<
            ":"                     << q.getLimiteSup()             << endl;

    cout << "42. Punto q: " << q << endl;
    cout << "43. Punto s: " << s << endl;

    Punto t;

    cout << "44. Nuevo punto t: " << t << endl;

//  Como la igualdad no esta redefinida, no se ve afeectada por el nuevo limite
    s = q;
    cout << "45. s = q: " << s << endl;

    s.setPunto(q);
    cout << "46. setPunto s: " << s << endl;

    s.setLimites(500, -85);
    cout << "47. Rango de punto: "  << Punto::getLimiteInf()        <<
            ":"                     << Punto::getLimiteSup()        << endl;

    PRESS_KEY;

    // --------------------------    Consigna 10   -------------------------
    cout << endl;
    cout << "------------------    Consigna 10   -----------------" << endl;

    Punto u(12.34, 34.56);
    cout << "48. Punto u: "     << u    << endl;
    cout << "49. Punto u++: "   << u++  << endl;
    cout << "50. Punto u: "     << u    << endl;
    cout << "51. Punto ++u: "   << ++u  << endl;

    PRESS_KEY;

    // --------------------------    Consigna 11   -------------------------
    cout << endl;
    cout << "------------------    Consigna 11   -----------------" << endl;

    Punto *v = new Punto(12.34, 34.56);

    cout << "52. Punto v: " << *v << endl;
    cout << "53. Puntos creados: "  << v->getCantCreada()       <<
            " - Existentes: "       << Punto::getCantExistente()    << endl;

    delete(v);

    cout << "54. Puntos creados: "  << Punto::getCantCreada()       <<
            " - Existentes: "       << Punto::getCantExistente()    << endl;

    PRESS_KEY;

    // --------------------------    Consigna 12   -------------------------
    cout << endl;
    cout << "------------------    Consigna 12   -----------------" << endl;

    IntArr A(30);
    int v_aux[] = {23, 4, 54, 634, 6677, 32, 56};
    IntArr B(40, sizeof(v_aux) / sizeof(int), v_aux);
    A.prtArr();
    B.prtArr();

    PRESS_KEY;

    // --------------------------    Consigna 13   -------------------------
    cout << endl;
    cout << "------------------    Consigna 13   -----------------" << endl;

    B.prtArr(3);

    cout << "\n\nObbjeto B -"   << endl;
    cout << " size: "           << B.getSize()  << endl;
    cout << " used: "           << B.getUsed()  << endl;
    cout << " promedio: "       << B.getAvg()   << endl;

    PRESS_KEY;

    // --------------------------    Consigna 14   -------------------------
    cout << endl;
    cout << "------------------    Consigna 14   -----------------" << endl;

    int v_aux2[] = {0, 5, 10, 15, 20, 25, 30, 35, 40};
    IntArr C(10, sizeof(v_aux2) / sizeof(int), v_aux2);
    cout << " size: "           << C.getSize()  << endl;
    cout << " used: "           << C.getUsed()  << endl;
    C.prtArr();
    C.addElement(77);
    cout << " size: "           << C.getSize()  << endl;
    cout << " used: "           << C.getUsed()  << endl;
    C.prtArr();
    C.addElement(11);
    cout << " size: "           << C.getSize()  << endl;
    cout << " used: "           << C.getUsed()  << endl;
    C.prtArr();
    C.addElement(8, v_aux2);
    cout << " size: "           << C.getSize()  << endl;
    cout << " used: "           << C.getUsed()  << endl;
    C.prtArr();

    PRESS_KEY;

    // --------------------------    Consigna 15   -------------------------
    cout << endl;
    cout << "------------------    Consigna 15   -----------------" << endl;

    int v1[] = {0, 5, 10, 15, 20, 25, 30, 35, 40};
    int v2[] = {1, 2, 3, 4, 5, 6};

    IntArr D(10, sizeof(v1) / sizeof(int), v1);

    cout << " size: "           << D.getSize()  << endl;
    cout << " used: "           << D.getUsed()  << endl;

    D.prtArr();
    D.addElement(0, 77);
    D.addElement(56, 11);
    D.addElement(4, sizeof(v2) / sizeof(int), v2);

    cout << " size: "           << D.getSize()  << endl;
    cout << " used: "           << D.getUsed()  << endl;

    D.prtArr();
    D.addElement(4, 99);

    cout << " size: "           << D.getSize()  << endl;
    cout << " used: "           << D.getUsed()  << endl;

    D.prtArr();

    PRESS_KEY;
*/
    // --------------------------    Consigna 16   -------------------------
    cout << endl;
    cout << "------------------    Consigna 16   -----------------" << endl;

    cout << endl;
    cout << "------------------      Inicio      -----------------" << endl;

    int v3[] = {0, 5, 10, 15, 20, 25, 30, 35, 40};
    int v4[] = {1, 2, 3, 4, 5, 6};

    IntArr E(10, sizeof(v3) / sizeof(int), v3);
    IntArr F(10, sizeof(v4) / sizeof(int), v4);
    IntArr G = F;

    F.addElement(0, 99);

    cout << " size: "           << E.getSize()  << endl;
    cout << " used: "           << E.getUsed()  << endl;

    E.prtArr();

    cout << endl;
    cout << "Array F: " << F << endl;
    cout << "Array G: " << G << endl;

//    cout << "------------------      Medio       -----------------" << endl;
//    cout << endl;
//
//    E = F + G;
//    cout << "Array E = F + G: " << E << endl;
//
//    IntArr H(10, sizeof(v3) / sizeof(int), v3);
//
//    H = H;
//
//    cout << "Array E: " << E << endl;
//
//    cout << "------------------      Medio       -----------------" << endl;
//    cout << endl;
//
//    H += F;
//    cout << "Array H += F: " << H << endl;

    PRESS_KEY;

    return 0;
}
