/*******************************************************************************
 *
 * @file                 punto.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 14/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <math.h>
#include "punto.h"

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
int Punto::cantcreada = 0;
int Punto::cantexistente = 0;
double Punto::limitesuperior = 1000;
double Punto::limiteinferior = -1000;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
Punto::Punto(void) {
    cantcreada++;
    cantexistente++;
    if(0 < getLimiteInf()) {
        setX(getLimiteInf());
        setY(getLimiteInf());
    }
    else {
        setX(0);
        setY(0);
    }
}

Punto::Punto(double x, double y) {
    if(x < getLimiteInf())
        setX(getLimiteInf());
    else if(x > getLimiteSup())
        setX(getLimiteSup());
    else
        setX(x);
    if(y < getLimiteInf())
        setY(getLimiteInf());
    else if(y > getLimiteSup())
        setY(getLimiteSup());
    else
        setY(y);
    cantcreada++;
    cantexistente++;
}

Punto::Punto(const Punto &a) {
    if(a.getX() < getLimiteInf())
        setX(getLimiteInf());
    else if(a.getX() > getLimiteSup())
        setX(getLimiteSup());
    else
        setX(a.getX());
    if(a.getY() < getLimiteInf())
        setY(getLimiteInf());
    else if(a.getY() > getLimiteSup())
        setY(getLimiteSup());
    else
        setY(a.getY());
    cantcreada++;
    cantexistente++;
}

void Punto::setPunto(const Punto &a) {
    if(a.getX() < getLimiteInf())
        setX(getLimiteInf());
    else if(a.getX() > getLimiteSup())
        setX(getLimiteSup());
    else
        setX(a.getX());
    if(a.getY() < getLimiteInf())
        setY(getLimiteInf());
    else if(a.getY() > getLimiteSup())
        setY(getLimiteSup());
    else
        setY(a.getY());
}

Punto::~Punto(void) {
    cantexistente--;
}

void Punto::setPunto(double x, double y) {
    if(x < getLimiteInf())
        setX(getLimiteInf());
    else if(x > getLimiteSup())
        setX(getLimiteSup());
    else
        setX(x);
    if(y < getLimiteInf())
        setY(getLimiteInf());
    else if(y > getLimiteSup())
        setY(getLimiteSup());
    else
        setY(y);
}

Punto & Punto::operator=(const Punto &z) {
    setPunto(z);
    return *this;
}

Punto Punto::operator+(const Punto &z) {
    Punto aux;
    aux.setX(getX() + z.getX());
    aux.setY(getY() + z.getY());
    return aux;
}

Punto Punto::operator-(const Punto &z) {
    Punto aux;
    aux.setX(getX() - z.getX());
    aux.setY(getY() - z.getY());
    return aux;
}

bool Punto::operator==(const Punto &a) const {
    bool igualdad = 0;
    if(this->getX() == a.getX()) {
        if(this->getY() == a.getY())
            igualdad = 1;
    }
    return igualdad;
}

bool Punto::operator!=(const Punto& a) const {
    bool igualdad = 0;
    if(this->getX() != a.getX())
        igualdad = 1;
    if(this->getY() != a.getY())
        igualdad = 1;
    return igualdad;

}

bool Punto::operator>(const Punto& a) const {
    bool val = 0;
    double distancia1, distancia2;
    distancia1 = sqrt((getX() * getX()) + (getY() * getY()));
    distancia2 = sqrt((a.getX() * a.getX()) + (a.getY() * a.getY()));
    if(distancia1 > distancia2)
        val = 1;
    return val;
}

bool Punto::operator<(const Punto& a) const {
    bool val = 0;
    double distancia1, distancia2;
    distancia1 = sqrt((getX() * getX()) + (getY() * getY()));
    distancia2 = sqrt((a.getX() * a.getX()) + (a.getY() * a.getY()));
    if(distancia1 < distancia2)
        val = 1;
    return val;
}

ostream& operator<<(ostream& out, const Punto& a) {
    out << "(" << a.getX() << ", " << a.getY() << ")";
    return out;
}

istream& operator>>(istream& in, Punto& a) {
    double x, y;
    cout << "x: ";
    in >> x;
    a.setX(x);
    cout << "y: ";
    in >> y;
    a.setY(y);
    return in;
}

Punto Punto::operator++() {
    setX(getX() + 1);
    setY(getY() + 1);
    return *this;
}

Punto Punto::operator++(int) {
    Punto suma(*this);
    setX(getX() + 1);
    setY(getY() + 1);
    return suma;
}

void* Punto::operator new(size_t size) {
    void *p = ::new char[size];
    return p;
}

void Punto::operator delete(void *p) {
    free(p);
}
