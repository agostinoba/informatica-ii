/*******************************************************************************
 *
 * @file                 punto.h
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 14/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef PUNTO_H
#define PUNTO_H

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <iostream>
#include <string.h>
#include "globales.h"

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
class Punto {
    public:
        Punto(void);
        Punto(double x, double y = 0);
        Punto(const Punto &);
        ~Punto(void);

        void setPunto(double, double);
        void setPunto(const Punto &);
        inline void setX(double x) { mx = x; }
        inline void setY(double y) { my = y; }
        inline double getX() const { return mx; }
        inline double getY() const { return my; }
        inline Punto getPunto(void) const {
                                            Punto a;
                                            a.setX(getX());
                                            a.setY(getY());
                                            return a; }

        Punto & operator=(const Punto &);
        Punto operator+(const Punto &);
        Punto operator-(const Punto &);
        friend Punto operator+(const int, const Punto &);
        friend Punto operator-(const int, const Punto &);

        bool operator==(const Punto&) const;
        bool operator!=(const Punto&) const;
        bool operator>(const Punto&) const;
        bool operator<(const Punto&) const;
        friend ostream& operator<<(ostream&, const Punto&);
        friend istream& operator>>(istream&, Punto&);

        static inline int getCantCreada() { return cantcreada; }
        static inline int getCantExistente() { return cantexistente; }

        static inline double getLimiteSup() { return limitesuperior; }
        static inline double getLimiteInf() { return limiteinferior; }
        static void setLimites(float inferior, float superior) {
            if(inferior < superior) {
                limiteinferior = inferior;
                limitesuperior = superior;
            }
        };

        Punto operator++();
        Punto operator++(int);

        void* operator new(size_t size);
        void operator delete(void *p);

    private:
        double mx;
        double my;
        static int cantcreada;
        static int cantexistente;
        static double limitesuperior;
        static double limiteinferior;
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // PUNTO_H
