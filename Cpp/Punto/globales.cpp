/*******************************************************************************
 *
 * @file                 globales.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 14/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "globales.h"

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
Punto operator+(const int v, const Punto &z) {
    Punto aux;
    aux.setX(v + z.getX());
    aux.setY(0 + z.getY());
    return aux;
}

Punto operator-(const int v, const Punto &z) {
    Punto aux;
    aux.setX(v - z.getX());
    aux.setY(0 - z.getY());
    return aux;
}
