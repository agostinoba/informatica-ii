/*******************************************************************************
 *
 * @file                 intarr.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 14/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <iostream>
#include "intarr.h"

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
IntArr::IntArr() {

}

IntArr::IntArr(int sz) {
    p = new int[sz];
    size = sz;
    used = 0;
}

IntArr::~IntArr() {
    delete[] p;
}

IntArr::IntArr(int sz, int qtty, int *vec) {
    used = 0;
    if(qtty > sz)
        sz = qtty;
    p = new int[sz];
    size = sz;
    for(int i = 0; i < qtty; i++) {
        p[i] = vec[i];
        used++;
    }
}

void IntArr::prtArr(int indice) const {
    if(0 < indice && indice <= used) {
        cout << "Array: ";
        if(0 == used)
            cout << "Vacío !!!" << endl;
        else {
            for(int i = 0; i < indice - 1; i++)
                cout << p[i] << "; ";
            cout << p[indice - 1] << endl;
        }
    }
}

void IntArr::prtArr(void) const {
    cout << "Array: ";
    if(0 == used)
        cout << "Vacío !!!" << endl;
    else {
        for(int i = 0; i < used - 1; i++)
            cout << p[i] << "; ";
        cout << p[used - 1] << endl;
    }
}

double IntArr::getAvg(void) const {
    double promedio = 0;
    for(int i = 0; i < used; i++)
        promedio += p[i];
    promedio /= used;
    return promedio;
}

void IntArr::addElement(int elemento) {
    if(size == used) {
        int *paux = new int[size + 6];
        for(int i = 0; i < size; i++)
            paux[i] = p[i];
        paux[size] = elemento;
        delete[] p;
        p = paux;
        size += 6;
    }
    else
        p[used] = elemento;
    used++;
}

void IntArr::addElement(int cantidad, int *vec) {
    if(size < used + cantidad) {
        int *paux = new int[used + cantidad + 5];
        size = used + cantidad + 5;
        for(int i = 0; i < used; i++)
            paux[i] = p[i];
        for(int i = 0; i < cantidad; i++)
            paux[used + i] = vec[i];
        delete[] p;
        p = paux;
    }
    else {
        for(int i = 0; i < cantidad; i++)
            p[used + i] = vec[i];
        size += cantidad;
    }
    used += cantidad;
}

void IntArr::addElement(int posicion, int elemento) {
    if(posicion < 0)
        posicion = 0;
    if(size < posicion)
        posicion = size;
    if(size == used) {
        int *paux = new int[size + 6];
        for(int i = 0; i < posicion; i++)
            paux[i] = p[i];
        paux[posicion] = elemento;
        for(int i = posicion + 1; i < size + 1; i++)
            paux[i] = p[i];
        paux[size] = elemento;
        delete[] p;
        p = paux;
        size += 6;
    }
    else {
        int paux[used - posicion];
        for(int i = posicion; i < used; i++)
            paux[i - posicion] = p[i];
        p[posicion] = elemento;
        for(int i = 0; i < used; i++)
            p[posicion + 1 + i] = paux[i];
    }
    used++;
}

void IntArr::addElement(int posicion, int cantidad, int *vec) {
    if(posicion < 0)
        posicion = 0;
    if(size < posicion)
        posicion = size;
    if(size < used + cantidad) {
        int *paux = new int[used + cantidad + 5];
        size = used + cantidad + 5;
        for(int i = 0; i < posicion; i++)
            paux[i] = p[i];
        for(int i = 0; i < cantidad; i++)
            paux[posicion + i] = vec[i];
        for(int i = 0; i < used; i++)
            paux[posicion + cantidad + i] = p[posicion + i];
        delete[] p;
        p = paux;
    }
    else {
        int paux[used - posicion];
        for(int i = posicion; i < used; i++)
            paux[i - posicion] = p[i];
        for(int i = 0; i < cantidad; i++)
            p[posicion + i] = vec[i];
        for(int i = 0; i < used; i++)
            p[posicion + 1 + i] = paux[i];
        size += cantidad;
    }
    used += cantidad;
}

ostream& operator<<(ostream& out, const IntArr& a) {
    out << endl;
    if(0 == a.used)
        out << "Vacío !!!";
    else {
        out << "Array (size: " << a.getSize() << ") (used:" << a.getUsed()
            << ")" << endl;
        out << "Array: ";
        for(int i = 0; i < a.used - 1; i++)
            out << a.p[i] << "; ";
        out << a.p[a.used - 1] << endl;
    }
    return out;
}

IntArr & IntArr::operator=(const IntArr &a) {
    cout << "Dato" << endl;
    if(p != a.p) {
        if(size < a.used) {
            int *paux = new int[a.used];
            for(int i = 0; i < a.used; i++) {
                paux[i] = a.p[i];
            }
            delete[] p;
            p = paux;
            size = a.getSize();
            used = a.getUsed();
        }
        else {
            for(int i = 0; i < a.used; i++) {
                p[i] = a.p[i];
            }
            size = a.getSize();
            used = a.getUsed();
        }
    }
    return *this;
}

IntArr IntArr::operator+(const IntArr &b) {
    int size_a = getSize();
    int used_a = getUsed();
    int size_b = b.getSize();
    int used_b = b.getUsed();
    int *paux = new int[size_a + size_b];
    for(int i = 0; i < used_a; i++)
        paux[i] = p[i];
    for(int i = 0; i < used_b; i++)
        paux[used_a + i] += b.p[i];
    IntArr aux(size_a + size_b, used_a + used_b, paux);
    delete[] paux;
    return aux;
}

void IntArr::operator+=(const IntArr &a) {
    for(int i = 0; i < getUsed(); i++)
        p[i] += a.p[i];
}
