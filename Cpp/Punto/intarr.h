/*******************************************************************************
 *
 * @file                 intarr.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 14/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef INTARR_H
#define INTARR_H

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <iostream>
#include <string.h>

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
class IntArr {
    public:
        IntArr();
        IntArr(int);
        IntArr(int, int, int *);
        ~IntArr();

        void prtArr(void) const;
        void prtArr(int) const;

        inline int getSize(void) const { return size; };
        inline int getUsed(void) const { return used; };
        double getAvg(void) const;

        void addElement(int);
        void addElement(int, int *);

        void addElement(int, int);
        void addElement(int, int, int *);

        friend ostream& operator<<(ostream&, const IntArr&);
        IntArr & operator=(const IntArr &);
        IntArr operator+(const IntArr &);
        void operator+=(const IntArr &);

    private:
        int *p;
        int size;
        int used;
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // INTARR_H
