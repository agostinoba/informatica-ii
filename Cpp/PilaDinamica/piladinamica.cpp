/*******************************************************************************
 *
 * @file                 piladinamica.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 07/06/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "piladinamica.h"
#include <stdio.h>
#include <iostream>

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
PilaDinamica::PilaDinamica(void) {
    H = nullptr;
}

PilaDinamica::~PilaDinamica(void) {
    struct nodo *aux = H;
    while(H) {
        H = H->N;
        delete aux;
        aux = H;
    }
    return;
}

bool PilaDinamica::push(struct dato &e) {
    bool salida = false;
    struct nodo *q;

    q = new struct nodo;

    if(q) { // Checkea errores al crear la estuctura
        q->d = e; // Cargo los datos
        //enlazar
        q->N = H;
        H = q;
        salida = true;
    }
    return salida;
}

bool PilaDinamica::pop(struct dato &e) {
    bool salida = false;
    struct nodo *q;

    if(!Vacia()) {
        e = H->d ; // tomo los datos
        //elimino nodo
        q = H;
        H = H->N ;
        delete q;
        salida = true;
    }
    return salida;
}

bool PilaDinamica::Vacia(void) {
    return H == nullptr ;
}
