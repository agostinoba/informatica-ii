/*******************************************************************************
 *
 * @file                 piladinamica.h
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 07/06/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef PILADINAMICA_H
#define PILADINAMICA_H

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/
struct dato {
    int a;
    int b;
};

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
class PilaDinamica {
    protected: // Protected es privado, pero se puede heredar
        struct nodo
        {
            struct dato d;
            struct nodo *N;
        };

        struct nodo *H;
        bool Vacia(void);

    public:
        PilaDinamica(void);
        ~PilaDinamica(void);
        bool push(struct dato &e);
        bool pop(struct dato &e);
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // PILADINAMICA_H
