/*******************************************************************************
 *
 * @file                 main.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 07/06/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <iostream>
#include "piladinamica.h"
#include "coladinamica.h"
#include "lse.h"

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
int main(void) {
    PilaDinamica A; // pone solo al ppio
    ColaDinamica B; // pone solo al final
    LSE C;          // pone solo con un criterio

    struct dato DATO;

    for (int i = 0; i < 5; i ++) {
        DATO.a = i;
        DATO.b = i*5;

//        cout << "a = " << DATO.a << " | b = " << DATO.b << endl;

        A.push(DATO);
        B.push(DATO);
    }

    cout << "PILA-----------------------" << endl;

    while (A.pop(DATO)) {
        cout << "a = " << DATO.a << " | b = " << DATO.b << endl;
        C.push(DATO);
    }

//    cout << "COLA------------------------" << endl;
//    while (B.pop(DATO))
//        cout << "a = " << DATO.a << " | b = " << DATO.b << endl;


    cout << "LSE-------------------------" << endl;
    while (C.pop(DATO))
        cout << "a = " << DATO.a << " | b = " << DATO.b << endl;

    DATO.a = 5;
    DATO.b = 7;
    C.push(DATO);

    DATO.a = 1;
    DATO.b = 5;
    C.push(DATO);

    DATO.a = 3;
    DATO.b = 5;
    C.push(DATO);

    DATO.a = 3;
    DATO.b = 6;
    C.push(DATO);

//    DATO.a = 9;
//    DATO.b = 1;
//    C.push(DATO);

    while (C.pop(DATO))
        cout << "a = " << DATO.a << " | b = " << DATO.b << endl;

    return 0;
}
