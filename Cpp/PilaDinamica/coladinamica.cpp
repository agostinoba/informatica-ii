/*******************************************************************************
 *
 * @file                 coladinamica.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 07/06/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "coladinamica.h"
#include <stdio.h>

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
ColaDinamica::ColaDinamica(void) {

}

bool ColaDinamica::push(struct dato &e) {
    bool salida = false;
    struct nodo *q;

    q = new struct nodo;

    if(q) {
        q->d = e; // Cargo los datos

        if(!Vacia()) {
            struct nodo *i = H ;

            while(i->N)
               i = i->N;

            //enlazar
            q->N = nullptr;
            i->N = q;
        }
        else
            PilaDinamica::push(e);
        salida = true;
    }
    return salida;
}
