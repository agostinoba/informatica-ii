/*******************************************************************************
 *
 * @file                 lse.cpp
 * @brief                Módulo para el seteo de puertos y pines.
 * @version              1.00
 * @date                 07/06/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <iostream>
#include "lse.h"

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
LSE::LSE(void) {

}

bool LSE::insertar(struct nodo *p, struct dato &e) {
    bool salida = false;
    struct nodo *q;

    q = new struct nodo;

    if(q) { // Checkeo que se haya creado el espacio en memoria
        q->d = e;
        if (H == nullptr) {
            // Para el caso que la lista esté vacia
            H = q;
            q->N = nullptr;
        }
        else {
            if(H == p && ((q->d.a < p->d.a) ||
                          (q->d.a == p->d.a &&
                           q->d.b < p->d.b))) {
                // Para el caso de que haya que ingresar un dato al inicio
                // de la lista
                H = q;
                q->N = p;
            }
            else {
                // Para cualquier otro caso
                q->N = p->N;
                p->N = q;
            }
        }

        salida = true;
        }

    return salida;
}

bool LSE::push(struct dato &e) {
    bool salida = false;
    struct nodo *q;

    q = new struct nodo;

    if(q) {
        q->d = e; // Cargo los datos

        if(!Vacia()) {
            struct nodo *i = H;
            struct nodo *anterior = H;

            // Aca tengo que buscar el lugar donde insertar el objeto
            while(q->d.a > i->d.a) {
                anterior = i;
                i = i->N;
            }

            // Hago la comparacción respecto de 'b' debido a que los 'a'
            // son iguales
            if ((q->d.a == i->d.a) && (q->d.b > i->d.b))
                anterior = i;

             salida = insertar(anterior, e);
        }
        else
             salida = insertar(H, e); // Si la lista está vacía
    }
    return salida; // retorno 0 en caso de error
}
