/*******************************************************************************
 *
 * @file                 balde.h
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 03/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef BALDE_H
#define BALDE_H

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <iostream>
#include "stdio.h"

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
class Balde {
    public:
        Balde();
        Balde(unsigned int, float);
        Balde(const Balde &);  // Constructor de copia
        ~Balde(void);

        void set_litros(unsigned int);
        void set_precio(float);

        unsigned int get_litros(void) const;
        float get_precio(void) const;
        int get_id(void) const;

        int id;
        static int nextid;

        friend std::ostream& operator<<(std::ostream&, const Balde&);

    private:
        unsigned int litros;
        float precio;
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // BALDE_H
