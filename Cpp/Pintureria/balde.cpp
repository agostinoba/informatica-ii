/*******************************************************************************
 *
 * @file                 balde.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 03/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "balde.h"

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
int Balde::nextid = 0;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
Balde::Balde() : id(nextid++) {
    set_precio(0);
    set_litros(0);
}

// Lista de inicialización
Balde::Balde(unsigned int l, float p) : id(nextid++) {
    set_litros(l);
    set_precio(p);
}

Balde::Balde(const Balde &objeto) : id(nextid++) { // Constructor de copia
    set_litros(objeto.get_litros());
    set_precio(objeto.get_precio());
}

Balde::~Balde(void) {

}

void Balde::set_litros(unsigned int l) {
    litros = l;
}

void Balde::set_precio(float p) {
    precio = p;
}

unsigned int Balde::get_litros(void) const {
    return litros;
}

float Balde::get_precio(void) const {
    return precio;
}

int Balde::get_id(void) const {
    return id;
}

std::ostream& operator<<(std::ostream& out, const Balde& z) {
    out << "Balde con (ID "        << z.get_id()       << ")"  << endl;
    out << "Contenido por balde: " << z.get_litros()   << " L" << endl;
    out << "Precio por litro: "    << z.get_precio()   << "$"  << endl;
    return out;
}
