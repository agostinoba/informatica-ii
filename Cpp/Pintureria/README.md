# Ejercicio 1 - Sistema de compras de una pinturería

Se propone implementar un sistema básico de compras para un pinturería. Recordar
que una implementación completa de dicho sistema, comprendería una cantidad de
aspectos demasiado grande para abordar en esta guía, por lo que se realizan
ciertas simplificaciones para mantener el ejercicio en un nivel realizable.

## 1.1 - Definición de un balde de pintura

Para comenzar con la implementación propuesta, definir una *clase* con el nombre
**Balde** descripta por:

```
class Balde {
      public:
            Balde(unsigned int l, float p);
            ~Balde();

            void set_litros(unsigned int l);
            void set_precio(float p);


      private:
            unsigned int litros;
            float precio;
};
```

Completar todas las implementaciones descriptas en la clase. Nótese que no se
permite instanciar un objeto **Balde** sin pasar argumentos a su constructor.
Con el objeto de evaluar la correcta definición de la clase, ejecutar el
siguiente *main*:

```
#include <iostream>
#include <balde.h>

using namespace std;

inf main(void) {
    Balde a{4, 730.0/4};
    Balde b{20, 3200.0/20};

    cout << "Balde a:" << endl;
    cout << "Contenido por balde: " << a.get_litros() << "L" << endl;
    cout << "Precio por litro: " << a.get_precio() << "$" << endl;

    cout << endl;

    cout << "Balde b:" << endl;
    cout << "Contenido por balde: " << b.get_litros() << "L" << endl;
    cout << "Precio por litro: " << b.get_precio() << "$" << endl;

    return 0;
}
```

Del cuál debería obtener la salida:

       Balde a:
       Contenido por balde: 4 L
       Precio por litro: 182.5$

       Balde b:
       Contenido por balde: 20 L
       Precio por litro: 160$

## 1.2 - Extensión de la clase Balde

Para poder implementar una búsqueda de producto, agregue un miembro *id* a la
clase **Balde** de forma tal que a cada objeto instanciado se le asigne
automáticamente un *id* en forma sucesiva (0, 1, ...) sin modificar el
constructor propuesto en el hito anterior, ni agregar ningún constructor nuevo.
A su vez, implemente el método *get_id()*. **No implemente** el método
*set_id()* dado que la idea es dejar que la clase automatice la asignación del
mismo.  
**NOTA: Para implementar este hito, declare una variable *estática* a la
clase que lleve dicha cuenta de identificador único.**

A fines de probar la implementación, utilice el siguiente *main*:

```
#include <iostream>
#include <balde.h>

using namespace std;

int main(void) {
    Balde a{4, 730.0/4};
    Balde b{20, 3200.0/20};

    cout << "Balde a (ID ):" << a.get_id() << ")" << endl;
    cout << "Contenido por balde: " << a.get_litros() << "L" << endl;
    cout << "Precio por litro: " << a.get_precio() << "$" << endl;

    cout << endl;

    cout << "Balde b (ID ):" << b.get_id() << ")" << endl;
    cout << "Contenido por balde: " << b.get_litros() << "L" << endl;
    cout << "Precio por litro: " << b.get_precio() << "$" << endl;

    return 0;
}
```

Del cuál debería obtener la salida:

       Balde a (ID 0)
       Contenido por balde: 4 L
       Precio por litro: 182.5$

       Balde b (ID 1)
       Contenido por balde: 20 L
       Precio por litro: 160$

## 1.3 - Constructor de copia y asignación

Defina e implemente el constructor sin argumentos para la clase **Balde**, el
cuál asigne el *id* correspondiente al mismo, e inicialice *precio* en cero.
Defina e implemente el constructor de copia y operador de asignación para la
clase **Balde**.  
**NOTA: El *id* de cada objeto Balde debe ser único, tomar los
recaudos necesarios para que esta condición se cumpla.**

Para probar las nuevas incorporaciones, utilizar el siguiente *main*:

```
#include <iostream>
#include <balde.h>

using namespace std;

int main(void) {
    Balde a{4, 730.0/4};
    Balde b{a};

    cout << "Balde a (ID ):" << a.get_id() << ")" << endl;
    cout << "Contenido por balde: " << a.get_litros() << "L" << endl;
    cout << "Precio por litro: " << a.get_precio() << "$" << endl;

    cout << endl;

    cout << "Balde b (ID ):" << b.get_id() << ")" << endl;
    cout << "Contenido por balde: " << b.get_litros() << "L" << endl;
    cout << "Precio por litro: " << b.get_precio() << "$" << endl;

    return 0;
}
```

Del cuál debería obtener la salida:

       Balde a (ID 0)
       Contenido por balde: 4 L
       Precio por litro: 182.5$

       Balde b (ID 1)
       Contenido por balde: 20 L
       Precio por litro: 160$

Nótese que aunque el campo *litros y precio* de los baldes es igual, el id del
mismo es distinto, dado que son **distintos objetos**.

## 1.4 - Impresión de forma más automática

Sobrecargar el *operador desplazamiento a izquierda* de la clase **ostream**
para poder ejecutar una línea como la siguiente:

```
Balde a;
// Asignación de valores al Balde a
cout << a << endl;
```

De forma tal que imprima en pantalla lo siguiente:

       Balde con ID: i
       Contenido por balde: n L
       Precio por litro: x$

Donde:

- i = *id* del balde.
- n = *litros* del balde
- x = *precio* del balde

La función debe recibir un **const Balde &b** como segundo parámetro.

Para evaluar que todo haya sido desarrollado correctamente, utilice el sigueinte
*main*:

```
#include <iostream>
#include <balde.h>

using namespace std;

int main(void) {
    Balde a{4, 730.0/4};
    Balde b{a};

    cout << a;
    cout << endl;
    cout << b;

    return 0;
}
```

Del cuál debería obtener la salida:

       Balde con ID: 0
       Contenido por balde: 4 L
       Precio por litro: 182.5$

       Balde con ID: 1
       Contenido por balde: 20 L
       Precio por litro: 160$

## 1.5 - Generación de un catálogo

Implemente una clase **Catálogo** la cuál tenga la siguiente estructura:

```
#include <balde.h>

class Catalogo {
        public:
            Catalogo(unsigned int largo);
            ~Catalogo();

            Balde & operator[](unsigned int idx);
            const Balde & operator[](unsigned int idx) const;

        private:
            unsigned int cantidad_elementos;
            Balde *elementos;
};
```

Implementar la clase de forma tal que se reserve la memoria dinámicamente cuando
se instancie un objeto, y tener cuidado de no genera *memory leaks*. El único
constructor con el que cuenta la clase, deberá instanciar la cantidad pasada por
argumento de objetos de la clase **Balde** con su constructor sin argumentos. El
*operador índice* debe devolver una referencia a un objeto tipo **Balde** cuyo
índice se corresponda con el argumento pasado. Nótese que existen dos
implementaciones del *operador índice*, uno para objetos *const* y otro para los
modificables, en este caso, ambos harán lo mismo.  
**NOTA: Para evitar errores de indexación, truncar el índice a utilizar
realmente al último índice del catálogo.**

A fines de corroborar que el desarrollo fue correctamente realizado, utilizar el
siguiente *main*:

```
#include <iostream>
#include <catalogo.h>

using namespace std;

int main(void) {
    constexpr unsigned int LARGO_CATALOGO{5};
    Catalogo cat(LARGO_CATALOGO);

    for(unsigned int i = 0; i < LARGO_CATALOGO; i++) {
    cout << cat[i] << endl;
    }

    // Intento de indexación erronea
    cout << "Intento de indexación erronea..." << endl;
    cout << cat[10000] << endl;

    return 0;
}

```

Del cuál debería obtener la salida:

    Balde con ID: 0
    Contenido por balde: 0 L
    Precio por litro: 0$

    Balde con ID: 1
    Contenido por balde: 0 L
    Precio por litro: 0$

    Balde con ID: 2
    Contenido por balde: 0 L
    Precio por litro: 0$

    Balde con ID: 3
    Contenido por balde: 20 L
    Precio por litro: 160$

    Balde con ID: 4
    Contenido por balde: 20 L
    Precio por litro: 160$

    Intento de indexación erronea...
    Balde con ID: 4
    Contenido por balde: 0 L
    Precio por litro: 0$

## 1.6 - Interacción con el usuario

Implemente el *operador desplazamiento a izquierda* de la clase **Catálogo** de
forma tal de que imprima solamente los elementos *válidos* del mismo, siendo
elementos *inválidos* los que tengan como *precio* cero. En caso de que no tenga
ningún elemento *válido*, deberá informarlo en pantalla.  
No implemente ningún método más allá del mencionado en la clase **Catalogo**,
con los que se tiene hasta el momento se puede realizar lo que se pide a
continuación.  
Con las clases obtenidas hasta el momento, genere un *main* de forma tal que al
comenzar el programa, espere el ingreso del largo deseado del catálogo, y luego
ofrezca un menú al usuario con las siguiente opciones:

1. Modificar elemento del catálogo
2. Mostrar catálogo actual
3. Cualquier otro valor: Terminar programa

No contemplar casos donde el usuario ingrese opciones erroneas, asumir que
siempre se ingresan valores válidos, en este caso siempre serán números.
A continuación se adjunta una salida posible del programa (con interacciones)
del usuario incluídas):

```
Ingrese largo deseado del catálogo: 5
Elija una opción:
0: Modificar elemento del catálogo.
1: Mostrar catálogo actual.
Cualquier otro valor: Terminar programa.
Selección: 1
No hay elementos váidos!
Elija una opción:
0: Modificar elemento del catálogo.
1: Mostrar catálogo actual.
Cualquier otro valor: Terminar programa.
Selección: 0
Elija índice a modificar: 3
Ingrese litros del elemento: 5
Ingrese precio por litro del elemento: 200.3
Elija una opción:
0: Modificar elemento del catálogo.
1: Mostrar catálogo actual.
Cualquier otro valor: Terminar programa.
Selección: 1
Balde con ID: 3
Contenido por balde: 5 L
Precio por litro: 200.3$
----------

Elija una opción:
0: Modificar elemento del catálogo.
1: Mostrar catálogo actual.
Cualquier otro valor: Terminar programa.
Selección: 2
```

## 1.7 - Preguntas teóricas

- Sino se hubiese implementado explícitamente el *operador de asignación* y el
*constructor de copia* de la clase **Balde**, el lenguaje hubiese implementado
los mismos por default. ¿Qué problema hubiese existido en caso de utilizar los
mismos con los detalles planteados en la implementación propuesta de la clase?  
- Si se pretendiera implementar el *operator==* de la clase **Balde**. ¿Qué
miembro de la clase utilizaría para definir la igualdad de los dos objetos?
¿Por qué?  
- ¿Qué métodos consideraría necesarios implementar en las clases **Balde** y/o
**Catálago** para implementar el *operator==* de la clase **Catálogo**?
