/*******************************************************************************
 *
 * @file                 main.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 03/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <iostream>
#include <balde.h>
#include <catalogo.h>

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
int main(void) {
//    Balde a{4, 730.0/4};
//    Balde b{a};
//    Balde c{b};
//    Balde d{32, 100};
//    constexpr unsigned int LARGO_CATALOGO {5};
//
//    Catalogo cat(LARGO_CATALOGO);
//
//    for(unsigned int i = 0; i < LARGO_CATALOGO; i++) {
//        cout << cat[i] << endl;
//    }

    // Intento de indexación erronea
//    cout << "Intento de indexacion erronea..." << endl;
//    cout << cat[10000] << endl;
    int opcion = 1;
    int largo_catalogo = 0;
    unsigned int indice = 0;
    unsigned int litros = 0;
    float precio = 0;

    cout << "Ingrese largo deseado del catalogo." << endl;
    cin >> largo_catalogo;

    Catalogo z(largo_catalogo);

    while((0 == opcion) || (1 == opcion)) {
        cout << "Elija una opción:" << endl;
        cout << "0: Modificar elemento del catalogo." << endl;
        cout << "1: Mostrar catalogo actual." << endl;
        cout << "Cualquier otro valor: Terminar programa." << endl;
        cin >> opcion;
        cout << "Selección: " << opcion << endl;;
        switch(opcion) {
            case 0:
                cout << "Elija indice a modificar: ";
                cin >> indice;
                cout << "Ingrese litros del elemento: ";
                cin >> litros;
                z[indice].set_litros(litros);
                cout << "Ingrese precio por litro del elemento: ";
                cin >> precio;
                z[indice].set_precio(precio);
                break;
            case 1:
                cout << z << endl;
                break;
            default:
                break;

        }
    }
    return 0;
}
