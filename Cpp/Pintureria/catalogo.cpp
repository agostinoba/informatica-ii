/*******************************************************************************
 *
 * @file                 catalogo.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 03/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "catalogo.h"

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
Catalogo::Catalogo(unsigned int largo) {
    elementos = new Balde[largo];
    if(elementos) {
        for(unsigned int i = 0; i < largo; i++) {
            elementos[i].set_litros(0);
            elementos[i].set_precio(0);
        }
        cantidad_elementos = largo;
    }
}

Catalogo::~Catalogo() {

}

// Para instanciar objetos en el constructor
Balde & Catalogo::operator[](unsigned int idx) {
    if(idx >= cantidad_elementos) {
        return elementos[cantidad_elementos - 1];
    }
    else
        return elementos[idx];
}

// Para la sobrecarga de <<
const Balde & Catalogo::operator[](unsigned int idx) const {
    if(idx >= cantidad_elementos)
        return elementos[cantidad_elementos - 1];
    else
        return elementos[idx];
}

std::ostream& operator<<(std::ostream& out, const Catalogo& z) {
    unsigned int invalidos = 0;
    for(unsigned int i = 0; i < z.cantidad_elementos; i++) {
        if(0 != z[i].get_precio()) {
            out <<  endl;
            out << "Balde con ID: " << i << endl;
            out << "Contenido por balde: " << z[i].get_litros() << "L" << endl;
            out << "Precio por litro: " << z[i].get_precio() << "$" << endl;
            out <<  endl;
        }
        else
            invalidos++;
    }
    if((invalidos == z.cantidad_elementos) || (0 == invalidos))
        out << "No hay elementos validos!" << endl;
    return out;
}
