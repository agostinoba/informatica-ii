/*******************************************************************************
 *
 * @file                 main.cpp
 * @brief                Módulo con las funciones del ADC.
 * @version              1.00
 * @date                 03/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <iostream>
#include <stdio.h>
#include "matriz.h"

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
int main(void) {
    // Consigna 1
//    try {
//        Matriz m{2, 2};
//        Matriz e{0, 1};
//    }
//    catch (Matriz::ErrorDimensional err) {
//        cout << "Se detecto error de dimension!" << endl;
//    }

    // Consigna 2
//    Matriz m{2, 2};
//
//    for(unsigned int i = 0; i < 2; i++) {
//        for(unsigned int j = 0; j < 2; j++)
//            m.Set(i, j, i + 7);
//    }



    Matriz m{2, 2};
    for(unsigned int i = 0; i < 2; i++) {
        for(unsigned int j = 0; j < 2; j++)
            m.Set(i, j, 7 + i);
    }
//    Matriz e{4, 4};
//    for(unsigned int i = 0; i < 4; i++) {
//        for(unsigned int j = 0; j < 4; j++)
//            e.Set(i, j, i + 1);
//    }


    cout << "Matriz m: " << endl;
    cout << m << endl;

    Matriz e{m};

    e.Set(0, 0, 5);
    e.Set(1, 1, 10); // Consigna 2 - e.Set(0, 1, 10.3);

    cout << "Matriz e: " << endl;
    cout << e << endl;

    Matriz f = m * e;

    cout << "Matriz f = m * e: " << endl;
    cout << f << endl;

    f += f;

    cout << "f += f" << endl;
    cout << f << endl;

    return 0;
}
