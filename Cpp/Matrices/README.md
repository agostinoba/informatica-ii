# Ejercicio 3 - Implementación de matrices numéricas

Se propone implementar una clase que nos permita operar matemáticamente con
matrices. Se contemplarán las operaciones mínimas necesarias para poder operar
con matrices, teniendo en cuenta las propiedades de las mismas.

## 3.1 - Definición propuesta de la clase

A la hora de definir la clase **Matriz** debemos detenernos a pensar por un
momento qué miembros y métodos implementar como mínimo.
Para que la clase **Matriz** pueda ser viable *debe* poder tomar distintas
dimensiones. Para simplificar el problema, restringiremos la cantidad de
*dimensiones* de la **Matriz** a dos dimensiones, pudiendo así generar matrices
de dimensiones *[fila x columna]* como: *[2x2], [3x2], [1x10], etc*. Una
*Matriz* con una única fila, es el equivalente a un *Vector*, pudiendo la clase
**Matriz**cumplir las veces del mismo.
Para homogeneizar la resolución del problema, se propone entonces definir la
clase **Matriz** con tres miembro:

- filas -> Entero sin signo
- columnas -> Entero sin signo
- valores -> Puntero a *double*

Los miembros *filas y columnas* indicarán simplemente la cantidad de las mismas
para la **Matriz**, mientras que en el miembro *valores* se alojarán los
distintos elementos.
Implementar en la clase **Matriz** una *clase pública* llamada
**ErrorDimensional**, la cuál no tenga miembros ni métodos. Utilizaremos esta
clase para informar como un error si se intenta instanciar un objeto de la
clase *Matriz* con el largo de filas o columnas en cero.
Implementar un único constructor al que se le pasen como parámetro dos
*enteros sin signo* que indiquen cantidad de filas y columnas respectivamente.
Como se propuso, en caso de que se intente instanciar un objeto de la clase
**Matriz** con cantidad de *filas y columnas* igual a cero, arrojar el error
**ErrorDimensional** mediante la sentencia:

```
throw ErrorDimensional();
```

Para probar la implementación de la clase y corroborar que no se estén
generando *memory leaks*, utilizar el siguiente *main*:

```
#include <iostream>
#inlcude <matriz.h>

using namespace std;

int main(void) {
    try {
        Matriz m{2, 2};
        Matriz e{0, 1};
    }
    catch(Matriz::ErrorDimensional err) {
        cout << "Se detecto error de dimensión!" << endl;
    }
    return 0;
}
```

El cuál debería detectar el error de dimensión inválida e imprimir en consola el
texto en el bloque *catch*. Corroborar que no se genere ningún *memory leak*.

## 3.2 - Asignación/Lectra de valores

Implementar el método *at* para obtener un elemento de la **Matriz** el cuál
recibirá dos *enteros sin signo* indicando a qué *fila y columna*
respectivamente se desea acceder, y devolverá el *double* correspondiente a
dichos parámetros. En caso de que el elemento pedido se encuentre fuera del
rango de la **Matriz**, generar un error del tipo **ErrorRango**.
Implementar el método *set* para fijar el valor de un elemento de la *Matriz* el
cuál recibirá dos *enteros sin signo* indicando a qué *fila y columna*
respectivamente se desea fijar. En caso de que el elemento a fijar se encuentre
fuera del rango de la **Matriz**, generar un error del tipo **ErrorRango**.
Implementar el *constructor de copia* de la clase **Matriz**, el cuál reciba una
*referencia constante a una **Matriz***. Realizar una copia de cada uno de los
elementos de la **Matriz**, es decir, la memoria de cada objeto debe ser
independiente.
Implementar también el *operador desplazamiento a izquierda* con una
*referencia no constante* a un objeto **ostream** como primer argumento, una
*referencia constante a una **Matriz*** como segundo parámetro y que retorne una
*referencia no constante* a un objeto **ostream**, de forma tal de imprimir la
**Matriz** en la forma que se muestra más adelante.

Con el fin de probar que lo métodos hayan sido implementados correctamente,
utilizar el siguiente *main*:

```
#include <iostream>
#inlcude <matriz.h>

using namespace std;

int main(void) {
    Matriz m{2, 2};
    for(unsigned int i = 0; i < 2; i++) {
        for(unsigned int j = 0; j < 2; j++) {
            m.set(i, j, i);
        }
    }
    cout << "Matriz m: " << endl;
    cout << m << endl;
    Matriz e{m};
    e.set(0, 0, 5);
    e.set(0, 1, 10.3);
    cout << "Matriz e: " << endl;
    cout << e << endl;
    return 0;
}
```

De lo cuál debería obtener la siguiente salida:

        Matriz m:
        (0, 0)
        (1, 1)

        Matriz e:
        (5, 10.3)
        (1, 1)
    
## 3.3 - Operaciones matriciales

Implementar los siguientes operadores de la clase **Matriz**:

```
Matriz & operator=(const Matriz &m);
Matriz operator+(const Matriz &m) const;
Matriz operator+(double v) const;
void operator+=(const Matriz & m);
void operator+=(double v);
Matriz operator-(const Matriz &m) const;
Matriz operator-(double v) const;
void operator-=(const Matriz & m);
void operator-=(double v);
Matriz operator*(const Matriz &m) const;
Matriz operator*(double v) const;
void operator*=(const Matriz & m);
void operator*=(double v);
```

Los operadores suma, resta y multiplicación de matrices a implementar, deben
corroborar que las dimensiones de las matrices sean las correspondientes para
que la operación sea válida y, en caso de no serlo, deberá arrojarse el error
**ErrorDimensional**.

Con el fin de probar la implementación de los operadores, utilizar el siguiente
*main*:

```
#include <iostream>
#inlcude <matriz.h>

using namespace std;

int main(void) {
    Matriz m{2, 2};
    for(unsigned int i = 0; i < 2; i++) {
        for(unsigned int j = 0; j < 2; j++) {
            m.set(i, j, i + 7);
        }
    }
    cout << "Matriz m: " << endl;
    cout << m << endl;
    Matriz e{m};
    e.set(0, 0, 5);
    e.set(0, 1, 10);
    cout << "Matriz e: " << endl;
    cout << e << endl;
    Matriz f = m * e;
    cout << "Matriz f = m * e: " << endl;
    cout << f << endl;
    f += f;
    cout << "f += f" << endl;
    cout << f << endl;
    return 0;
}
```

De lo cuál debería obtener la siguiente salida:

        Matriz m:
        (7, 7)
        (8, 8)

        Matriz e:
        (5, 7)
        (8, 10)

        Matriz f = m * e:
        (91, 119)
        (104, 136)

        f += f:
        (182, 238)
        (208, 272)
    
## 3.4 - Preguntas teóricas

- El método trivial de comparación entre objetos de la clase **Matriz**.
¿Alcanza para nuestro caso? ¿Por qué? De no alcanzar, ¿Cómo implementaría dicho
método?  
- ¿Qué otros métodos implementaría para aportar utilidad a la clase **Matriz**?  
- Si se quisiera mejorar la clase **Matriz** para que la misma pueda contener
datos de distintos tipos (int, double, complejo, etc) ¿Qué estrategia
utilizaría?  
