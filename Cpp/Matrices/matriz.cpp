/*******************************************************************************
 *
 * @file                 mariz.cpp
 * @brief                Implementación del Módulo para el seteo de los GPIOs.
 * @version              1.00
 * @date                 03/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "matriz.h"

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
Matriz::Matriz(unsigned int f, unsigned int c) {
    SetFilas(f);
    SetColumnas(c);
    valores = new double*[f];
    for(int i = 0; (unsigned int)i < f; i++) {
        valores[i] = new double[c];
    }
}

Matriz::Matriz(const Matriz &dato) {
    SetFilas(dato.filas);
    SetColumnas(dato.columnas);
    valores = new double*[dato.filas];
    for(int i = 0; (unsigned int)i < dato.filas; i++)
        valores[i] = new double[dato.columnas];
    for(unsigned int i = 0; i < dato.filas; i++) {
        for(unsigned int j = 0; j < dato.columnas; j++)
            Set(i, j, dato.valores[i][j]);
    }
}

Matriz::~Matriz(void) {

}

void Matriz::SetFilas(unsigned int f) {
    if(0 == f)
        throw ErrorDimensional();
    filas = f;
}

void Matriz::SetColumnas(unsigned int c) {
    if(0 == c)
        throw ErrorDimensional();
    columnas = c;
}

unsigned int Matriz::GetFilas(void) {
    return filas;
}

unsigned int Matriz::GetColumnas(void) {
    return columnas;
}

void Matriz::Set(unsigned int f, unsigned int c, double v) {
    // Tengo que comparar f y c con filas y columnas para que el valor que
    // quiero ingresar esté dentro de la matriz
    if(GetFilas() >= f && GetColumnas() >= c)
        valores[f][c] = v;
    else
        throw ErrorRango();
}

std::ostream& operator<<(std::ostream& out, const Matriz& z) {
    for(unsigned int i = 0; i < z.filas; i++) {
        out << "(";
        for(unsigned int j = 0; j < z.columnas; j++) {
            if(1 + j < z.columnas)
                out << z.valores[i][j] << ", ";
            else
                out << z.valores[i][j];
        }
        out << ")" << endl;
    }
    return out;
}

Matriz & Matriz::operator=(const Matriz &m) {
    if(filas == m.filas && columnas == m.columnas) {
        for(unsigned int i = 0; i < filas; i++) {
            for(unsigned int j = 0; j < columnas; j++)
                Set(i, j, m.valores[i][j]);
        }
    }
    else
        throw ErrorDimensional();
    return *this;
}

Matriz Matriz::operator+(const Matriz &m) const {
    Matriz Maux(filas, columnas);
    Maux.valores = new double*[filas];
    for(int i = 0; (unsigned int)i < filas; i++)
        Maux.valores[i] = new double[columnas];
    if(filas == m.filas && columnas == m.columnas) {
        for(unsigned int i = 0; i < filas; i++) {
            for(unsigned int j = 0; j < columnas; j++)
                Maux.Set(i, j, (valores[i][j] + m.valores[i][j]));
        }
    }
    else
        throw ErrorDimensional();
    return Maux;
}

Matriz Matriz::operator+(double v) const {
    Matriz Maux(filas, columnas);
    Maux.valores = new double*[filas];
    for(int i = 0; (unsigned int)i < filas; i++)
        Maux.valores[i] = new double[columnas];
    for(unsigned int i = 0; i < filas; i++) {
        for(unsigned int j = 0; j < columnas; j++)
            Maux.Set(i, j, (valores[i][j] + v));
    }
    return Maux;
}

void Matriz::operator+=(const Matriz &m) {
    if(filas == m.filas && columnas == m.columnas) {
        for(unsigned int i = 0; i < filas; i++) {
            for(unsigned int j = 0; j < columnas; j++)
                Set(i, j, (valores[i][j] + m.valores[i][j]));
        }
    }
    else
        throw ErrorDimensional();
}

void Matriz::operator+=(double v) {
    for(unsigned int i = 0; i < filas; i++) {
        for(unsigned int j = 0; j < columnas; j++)
            Set(i, j, (valores[i][j] + v));
    }
}

Matriz Matriz::operator-(const Matriz &m) {
    Matriz Maux(filas, columnas);
    Maux.valores = new double*[filas];
    for(int i = 0; (unsigned int)i < filas; i++)
        Maux.valores[i] = new double[columnas];
    if(filas == m.filas && columnas == m.columnas) {
        for(unsigned int i = 0; i < filas; i++) {
            for(unsigned int j = 0; j < columnas; j++)
                Maux.Set(i, j, (valores[i][j] - m.valores[i][j]));
        }
    }
    else
        throw ErrorDimensional();
    return Maux;
}

Matriz Matriz::operator-(double v) {
    Matriz Maux(filas, columnas);
    Maux.valores = new double*[filas];
    for(int i = 0; (unsigned int)i < filas; i++)
        Maux.valores[i] = new double[columnas];
    for(unsigned int i = 0; i < filas; i++) {
        for(unsigned int j = 0; j < columnas; j++)
            Maux.Set(i, j, (valores[i][j] - v));
    }
    return Maux;
}

void Matriz::operator-=(const Matriz &m) {
    if(filas == m.filas && columnas == m.columnas) {
        for(unsigned int i = 0; i < filas; i++) {
            for(unsigned int j = 0; j < columnas; j++)
                Set(i, j, (valores[i][j] - m.valores[i][j]));
        }
    }
    else
        throw ErrorDimensional();
}

void Matriz::operator-=(double v) {
    for(unsigned int i = 0; i < filas; i++) {
        for(unsigned int j = 0; j < columnas; j++)
            Set(i, j, (valores[i][j] - v));
    }
}

Matriz Matriz::operator*(const Matriz &m) {
    Matriz Maux(filas, m.columnas);
    Maux.valores = new double*[filas];
    for(int i = 0; (unsigned int)i < filas; i++)
        Maux.valores[i] = new double[m.columnas];

    if(columnas == m.filas) {
        double suma = 0;
        for(unsigned int j = 0; j < filas; j++) {
            for(unsigned int k = 0; k < filas; k++) {
                for(unsigned int i = 0; i < columnas; i++)
                    suma += valores[j][i] * m.valores[i][k];
                Maux.Set(j, k, suma);
                suma = 0;
            }
        }
    }
    else
        throw ErrorDimensional();
    return Maux;
}

Matriz Matriz::operator*(double v) {
    Matriz Maux(filas, columnas);
    Maux.valores = new double*[filas];
    for(int i = 0; (unsigned int)i < filas; i++)
        Maux.valores[i] = new double[columnas];
    for(unsigned int i = 0; i < filas; i++) {
        for(unsigned int j = 0; j < columnas; j++)
            Maux.Set(i, j, (valores[i][j] * v));
    }
    return Maux;
}

void Matriz::operator*=(const Matriz &m) {
    if(columnas == m.filas) {
        double suma = 0;
        for(unsigned int j = 0; j < filas; j++) {
            for(unsigned int k = 0; k < filas; k++) {
                for(unsigned int i = 0; i < columnas; i++)
                    suma += valores[j][i] * m.valores[i][k];
                Set(j, k, suma);
                suma = 0;
            }
        }
    }
    else
        throw ErrorDimensional();
}

void Matriz::operator*=(double v) {
    for(unsigned int i = 0; i < filas; i++) {
        for(unsigned int j = 0; j < columnas; j++)
            Set(i, j, (valores[i][j] * v));
    }
}
