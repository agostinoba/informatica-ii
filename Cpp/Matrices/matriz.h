/*******************************************************************************
 *
 * @file                 mariz.h
 * @brief                Implementación del Módulo para el seteo de los GPIOs.
 * @version              1.00
 * @date                 03/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef MATRIZ_H
#define MATRIZ_H

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <iostream>

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
using namespace std;

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
class Matriz {
    public:
        Matriz(unsigned int, unsigned int);
        Matriz(const Matriz &);
        ~Matriz(void);
        void SetFilas(unsigned int);
        void SetColumnas(unsigned int);
        unsigned int GetFilas(void);
        unsigned int GetColumnas(void);
        void Set(unsigned int f, unsigned int c, double v);

        friend std::ostream& operator<<(std::ostream&, const Matriz&);
        Matriz & operator=(const Matriz &);
        Matriz operator+(const Matriz &) const;
        Matriz operator+(double) const;
        void operator+=(const Matriz &);
        void operator+=(double);
        Matriz operator-(const Matriz &);
        Matriz operator-(double);
        void operator-=(const Matriz &);
        void operator-=(double);
        Matriz operator*(const Matriz &);
        Matriz operator*(double);
        void operator*=(const Matriz &);
        void operator*=(double);

        class ErrorDimensional {};
        class ErrorRango {};

    private:
        unsigned int filas;
        unsigned int columnas;
        double** valores;
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // MATRIZ_H
