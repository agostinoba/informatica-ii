/*******************************************************************************
 *
 * @file       PR_UART.c
 * @brief      Módulo con la funciones primitivas de UART.
 * @version    1.00
 * @date       17/11/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "Primitivas/PR_UART.h"

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void UART0_IRQHandler(void) {
	uint8_t temp, pendiente, identificador;
    do { // el do lo pongo para analizar si tengo
        temp = U0IIR; // Lo guardo en temp porque una vez que lo leo se "borra"
        pendiente = temp & 1 ; // De esta manera almaceno el b0
        identificador = (temp >> 1) & 0x07; // Almaceno b2 y b1

        // Dato de color: la recepción tiene mayor prioridad que la transimisión

        switch(identificador) {
            case RX:
                U0RX();
                break;

            case TX:
                U0TX();
                break;

            // AGREGAR EL CASE DE ERROR

            default:
                break;
        }
    } while (0 == pendiente);
}

void U0RX(void) {
    // Los datos de Registros siempre guardar en una temp,
    // pueden fallar al leer directo
	uint8_t temp = U0RBR;
    // el tamaño del buffer depende del maximo dato que puedo recibir
    Push_U0RX(temp);
}

void U0TX(void) {
	int16_t dato;
    dato = Pop_U0TX();
    if(dato >= 0) // Para checkear que no haya error en el dato
        U0THR = dato;
    else
        flag_fin_tx = 0;
}

int16_t UART_Leer(void) {
	int16_t temp;
	temp = Pop_U0RX();
	if(0 <= temp) {
		Descifrar_Trama(temp);
		return temp;
	}
	else
		return -1;
}

void UART_Enviar(uint8_t* dato, uint8_t tamanio) {
	uint8_t i;
	for(i = 0; i < tamanio; i++)
		Push_U0TX(dato[i]);
}

void Push_U0TX(uint8_t dato) {
    Buffertx[intx] = dato;
    intx++;
    if(intx >= TAMANIO_TX)
        intx = 0;
    if(flag_fin_tx == 0) {
        U0THR = Pop_U0TX();
        flag_fin_tx = 1;
    }
}

int16_t Pop_U0RX(void) {
    // devuelvo int16 porque el dato es uint8, pero para poder devolver
    // -1 de error "agrando" la variable
	int16_t dato;
    if(inrx == outrx)
        return -1;
    dato = Bufferrx[outrx];
    outrx++;
    if(outrx >= TAMANIO_RX)
        outrx = 0;
    return dato;
}
