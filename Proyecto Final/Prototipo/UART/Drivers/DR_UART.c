/*******************************************************************************
 *
 * @file       DR_UART.c
 * @brief      Módulo con las funciones de UART.
 * @version    1.00
 * @date       17/11/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "Drivers/DR_UART.h"

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
uint8_t Buffertx[TAMANIO_TX];
uint8_t intx;
uint8_t outtx;
uint8_t flag_fin_tx;

uint8_t Bufferrx[TAMANIO_RX];
uint8_t inrx;
uint8_t outrx;

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void UART0_Init(uint32_t baudrate) {
    // Registro de Power Control (Sirve para "encender" la UART) Pag 63
    PCONP |= (0x01 << 3); // UART0 es el registro donde está UART0 en PCONP

    // Despues de PCONP configuro los Pines
    PCLKSEL0 &= ~(0x03 << 6); // asigo velocidad a la UART
    // PCLKSEL, puedo dividirlo por 2, 4 o 6

    // Line Control Register Pag 306
    U0LCR = 0x83; // DLAB = 1, Word lenght = 11 = 8 bits

    // Divido por 16 por norma para que la medición caiga a la mitad del dato
    uint32_t cuenta = PCLK / 4 / 16 / baudrate;
    U0DLM = cuenta / 256; // cuenta >> 8
    U0DLL = cuenta % 256; // % (resto)

    U0LCR = 0x03; // DLAB = 0

    // IER e ISER habilitan interrupciones de UART (con las dos se habilita)
    // Pag 322
    U0IER = 0x03; // Habilito interrupciones de la UART (recibir y transmitir)

    // Pag 77
    ISER0 |= (1 << 5); // Habilito interrupciones

    intx = 0;
    inrx = 0;
    outtx = 0;
    outrx = 0;
    flag_fin_tx = 0;
}

int16_t Pop_U0TX(void) {
	int16_t dato;

    if(intx == outtx)
        return -1;

    dato = Buffertx[outtx];
    outtx++;

    if(outtx >= TAMANIO_TX)
        outtx = 0;

    return dato;
}

void Push_U0RX(uint8_t dato) {
	Bufferrx[inrx] = dato;
	inrx++;

    if(inrx >= TAMANIO_RX)
        inrx = 0;
}
