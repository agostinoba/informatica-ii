/*******************************************************************************
 *
 * @file       DR_UART.h
 * @brief      Módulo con las funciones de UART.
 * @version    1.00
 * @date       17/11/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef DRIVERS_DR_UART_H_
#define DRIVERS_DR_UART_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "Types.h"
#include "Trama_Recibir_Qt.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

//0x400FC0C4UL : Registro de habilitación de dispositivos:
#define DIR_PCONP		( ( uint32_t  * ) 0x400FC0C4UL )
//0x400FC1A8UL : Registro de seleccion de los clks de los dispositivos:
#define PCLKSEL		( ( uint32_t  * ) 0x400FC1A8UL )
//0xE000E100UL : Registro habilitación (set) de interrup. en el NVIC:
#define ISER			( ( uint32_t  * ) 0xE000E100UL )

// UART0 (Pag 309):
//0x4000C000UL : Registro de recepcion de la UART0:
#define DIR_U0RBR		( ( uint32_t  * ) 0x4000C000UL )
//0x4000C000UL : Registro de transmision de la UART0:
#define DIR_U0THR		( ( uint32_t  * ) 0x4000C000UL )
//0x4000C000UL : Parte baja del divisor de la UART0:
#define DIR_U0DLL		( ( uint32_t  * ) 0x4000C000UL )
//0x4000C004UL : Parte alta del divisor de la UART0:
#define DIR_U0DLM		( ( uint32_t  * ) 0x4000C004UL )
//0x4000C004UL : Registro de habilitación de las interrupciones:
#define DIR_U0IER		( ( uint32_t  * ) 0x4000C004UL )
//0x4000C008UL : Registro de identificación de las interrupciones:
#define DIR_U0IIR		( ( uint32_t  * ) 0x4000C008UL )
//0x4000C008UL : Registro de control de FIFO:
#define DIR_U0FCR		( ( uint32_t  * ) 0x4000C008UL )
//0x4000C00CUL : Registro de control de línea:
#define DIR_U0LCR		( ( uint32_t  * ) 0x4000C00CUL )
//0x4000C014UL : Registro de recepción de la UART0:
#define DIR_U0LSR		( ( uint32_t  * ) 0x4000C014UL )
//0x4000C01CUL : Registro de SCRATCH PAD:
#define DIR_U0SCR		( ( uint32_t  * ) 0x4000C01CUL )
//0x4000C020UL : Registro de control de Auto-baud:
#define DIR_U0ACR		( ( uint32_t  * ) 0x4000C020UL )
//0x4000C024UL : Registro de control de irDA:
#define DIR_U0ICR		( ( uint32_t  * ) 0x4000C024UL )
//0x4000C028UL : Registro de divisor fraccional:
#define DIR_U0FDR		( ( uint32_t  * ) 0x4000C028UL )
//0x4000C030UL : Registro de habilitación del trasmisor:
#define DIR_U0TER		( ( uint32_t  * ) 0x4000C030UL )

// UART1 (Pag 329):
//0x40010000UL : Registro de recepcion de la UART1:
#define DIR_U1RBR		( ( uint32_t  * ) 0x40010000UL )
//0x40010000UL : Registro de transmision de la UART1:
#define DIR_U1THR		( ( uint32_t  * ) 0x40010000UL )
//0x40010000UL : Parte baja del divisor de la UART1:
#define DIR_U1DLL		( ( uint32_t  * ) 0x40010000UL )
//0x40010004UL : Parte alta del divisor de la UART1:
#define DIR_U1DLM		( ( uint32_t  * ) 0x40010004UL )
//0x40010004UL : Registro de habilitación de las interrupciones:
#define DIR_U1IER		( ( uint32_t  * ) 0x40010004UL )
//0x40010008UL : Registro de identificación de las interrupciones:
#define DIR_U1IIR		( ( uint32_t  * ) 0x40010008UL )
//0x40010008UL : Registro de control de FIFO:
#define DIR_U1FCR		( ( uint32_t  * ) 0x40010008UL )
//0x4001000CUL : Registro de control de la UART1:
#define DIR_U1LCR		( ( uint32_t  * ) 0x4001000CUL )
//0x40010010UL : Registro de control de modem:
#define DIR_U1MCR		( ( uint32_t  * ) 0x40010010UL )
//0x40010014UL : Registro de recepcion de la UART1:
#define DIR_U1LSR		( ( uint32_t  * ) 0x40010014UL )
//0x40010018UL : Registro de estado de modem:
#define DIR_U1MSR		( ( uint32_t  * ) 0x40010018UL )
//0x4001001CUL : Registro de SCRATCH PAD:
#define DIR_U1SCR		( ( uint32_t  * ) 0x4001001CUL )
//0x40010020UL : Registro de control de Auto-baud:
#define DIR_U1ACR		( ( uint32_t  * ) 0x40010020UL )
//0x40010028UL : Registro de divisor fraccional:
#define DIR_U1FDR		( ( uint32_t  * ) 0x40010028UL )
//0x40010030UL : Registro de habilitación del trasmisor:
#define DIR_U1TER		( ( uint32_t  * ) 0x40010030UL )

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

// Registro PCONP:
#define		PCONP		DIR_PCONP[0]

// Registros PCLKSEL
#define		PCLKSEL0	PCLKSEL[0]
#define		PCLKSEL1	PCLKSEL[1]

#define		ISER0		ISER[0]
#define		ISER1		ISER[1]

// Registros de la UART0:
#define		U0RBR		DIR_U0RBR[0]
#define		U0THR		DIR_U0THR[0]
#define		U0DLL		DIR_U0DLL[0]
#define		U0DLM		DIR_U0DLM[0]
#define		U0IER		DIR_U0IER[0]
#define		U0IIR		DIR_U0IIR[0]
#define		U0FCR		DIR_U0FCR[0]
#define		U0LCR		DIR_U0LCR[0]
#define		U0LSR		DIR_U0LSR[0]
#define		U0SCR		DIR_U0SCR[0]
#define		U0ACR		DIR_U0ACR[0]
#define		U0ICR		DIR_U0ICR[0]
#define		U0FDR		DIR_U0FDR[0]
#define		U0TER		DIR_U0TER[0]

#define		U0RDR		(U0LSR&0x01)
#define		U0THRE		((U0LSR&0x20)>>5)

// Registros de la UART1:
#define		U1RBR		DIR_U1RBR[0]
#define		U1THR		DIR_U1THR[0]
#define		U1DLL		DIR_U1DLL[0]
#define		U1DLM		DIR_U1DLM[0]
#define		U1IER		DIR_U1IER[0]
#define		U1IIR		DIR_U1IIR[0]
#define		U1FCR		DIR_U1FCR[0]
#define		U1LCR		DIR_U1LCR[0]
#define		U1MCR		DIR_U1MCR[0]
#define		U1LSR		DIR_U1LSR[0]
#define		U1MSR		DIR_U1MSR[0]
#define		U1SCR		DIR_U1SCR[0]
#define		U1ACR		DIR_U1ACR[0]
#define		U1FDR		DIR_U1FDR[0]
#define		U1TER		DIR_U1TER[0]

#define		U1RDR		(U1LSR&0x01)
#define		U1THRE		((U1LSR&0x20)>>5)

#define		PORT0		0

// UART0
#define		UART0_TX	PORT0,2
#define		UART0_RX	PORT0,3

// UART1
#define		UART1_TX	PORT0,15
#define		UART1_RX	PORT0,16
#define		UART1_RTS	PORT0,22
#define		UART1_CTS	PORT0,17

#define		PCLK 		100000000 // (100MHz)
#define		RX			2
#define		TX			1
#define		TAMANIO_TX	50
#define		TAMANIO_RX	50

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/
extern uint8_t Buffertx[TAMANIO_TX];
extern uint8_t intx;
extern uint8_t outtx;
extern uint8_t flag_fin_tx;

extern uint8_t Bufferrx[TAMANIO_RX];
extern uint8_t inrx;
extern uint8_t outrx;

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void UART0_Init(uint32_t baudrate)
 * @brief		Función para inicializar la UART0
 * @param[in]	baudrate de comunicación de la UART
 * @return		void
 ******************************************************************************/
void UART0_Init(uint32_t baudrate);

/*******************************************************************************
 * @fn			int16_t Pop_U0TX(void)
 * @brief		Función que devuelve datos del buffer de transmisión
 * @param[in]	void
 * @return		Datos guardados en el buffer de transmisión
 ******************************************************************************/
int16_t Pop_U0TX(void);

/*******************************************************************************
 * @fn			void Push_U0RX(uint8_t dato)
 * @brief		Función que carga datos en el buffer de recepción
 * @param[in]	dato
 * @return		void
 ******************************************************************************/
void Push_U0RX(uint8_t dato);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* DRIVERS_DR_UART_H_ */
