/*******************************************************************************
 *
 * @file       Registros.h
 * @brief      Módulo con la definición de tipos de variables, estructuras,
 *             registros y macros.
 * @version    1.00
 * @date       12/06/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef REGISTROS_H_
#define REGISTROS_H_

/*******************************************************************************
 *** DATA TYPES
 ******************************************************************************/

/*******************************************************************************
 *** DEFINES
 ******************************************************************************/

//0xE000E100UL : Registro de habilitación (set) de interrup. en el NVIC:
#define ISER			( ( uint32_t  * ) 0xE000E100UL )
//0xE000E180UL : Registros de deshabilitacion (clear) de interrup. en el NVIC:
#define ICER			( ( uint32_t  * ) 0xE000E180UL )
//0xE000E200UL : Registro de las interrupciones pendientes en el NVIC:
#define ISPR			( ( uint32_t  * ) 0xE000E200UL )
//0xE000E280UL : Registro para borrar las interrupciones pendientes en el NVIC:
#define ICPR			( ( uint32_t  * ) 0xE000E280UL )
//0xE000E300UL : Registro de actividad de interrupciones en el NVIC:
#define IABR			( ( uint32_t  * ) 0xE000E300UL )
//0xE000E400UL : Registro de prioridad de las interrupciones en el NVIC:
#define IPR			( ( uint32_t  * ) 0xE000E400UL )
//0xE000EF00UL : Software Trigger Interrupt Register en el NVIC:
#define STIR_			( ( uint32_t  * ) 0xE000EF00UL )

// Registros Interrupciones EXTERNAS
//contiene flags EINT0 EINT1 EINT2 EINT3  T.10
#define EXTINT			( ( uint32_t *  ) 0x400FC140UL )
//reg. seteo si interr.  flanco o nivel T.11
#define EXTMODE		( ( uint32_t *  ) 0x400FC148UL )
//reg. seteo si flanc./niv. es posit. o negat.  T.12
#define EXTPOLAR		( ( uint32_t *  ) 0x400FC14CUL )
//indica interrup pendiente si hay "1" en bit 0 P0,
//indica interrup pendiente si hay "1" en bit 2 P2 T113
#define IOintStatus	( ( uint32_t *  ) 0x40028080UL )

// P2[10] EINT0 NMI,
// P2[11] EINT1 I2STX_CLK,
// P2[12] EINT2 I2STX_WS,
// P2[13] EINT3 I2STX_SDA
#define EXTMODE0		EXTMODE[0]	// (0x400FC148)
// EXTMODE1   es EXTMODE[0] desplazado un bit si a la izquier. EXTMODE|=(1<<1);
// EXTMODE2   es EXTMODE[0] desplazado Dosb bit   (2<<1)     EXTMODE|=(2<<1);
// EXTMODE3   es EXTMODE[0] desplazado Tres bit   (3<<1)	 EXTMODE|=(3<<1);
//            selecto por Flanco con Uno

//	EINT0   es   EXTINT[0]   EXINT|=1;
//	        reseteo flag de EINT0
//	EINT1   es   EXTINT[0] desplazado un bit a la izquier. EXINT|=(1<<1);
//	        reseteo flag de EINT1
//	EINT2   es   EXTINT[0] desplazado Dosb bit   (2<<1)    EXINT|=(2<<1);
//	        reseteo flag de EINT2
//	EINT3   es   EXTINT[0] desplazado Tres bit   (3<<1)    EXINT|=(3<<1);
//	        reseteo Flag de EINT3


//Status int.port0 por flanco Ascendente T.118
#define IO0intStatR	( ( uint32_t *  ) 0x40028084UL )
//Status int.port0 por flanco Descendente T.120
#define IO0intStatF	( ( uint32_t *  ) 0x40028088UL )
//Escribo un uno limpia la interrupción  T.122
#define IO0intClr       ( ( uint32_t *  ) 0x4002808CUL )
//enable int.port0 por flanco ascendente T.114
#define IO0intEnR       ( ( uint32_t *  ) 0x40028090UL )
//enable int.port0 por flanco descendente T.116
#define IO0intEnF		( ( uint32_t *  ) 0x40028094UL )
//#define   RESERVED        ( ( uint32_t  * ) 0x40028098UL )//ERROR Tabla 102
//#define   RESERVED        ( ( uint32_t  * ) 0x4002809CUL )
//#define   RESERVED        ( ( uint32_t  * ) 0x400280A0UL )
//Status int.port2 por flanco Ascendente T.119
#define IO2intStatR	( ( uint32_t *  ) 0x400280A4UL )
//Status int.port2 por flanco Descendente T.121
#define IO2intStatF	( ( uint32_t *  ) 0x400280A8UL )
//Escribo un uno limpia la interrupción  T.123
#define IO2intClr       ( ( uint32_t *  ) 0x400280ACUL )
//enable int.port2 por flanco ascendente T.115
#define IO2intEnR		( ( uint32_t *  ) 0x400280B0UL )
//enable int.port2 por flanco descendente T.117
#define IO2intEnF		( ( uint32_t *  ) 0x400280B4UL )



//0x4008C000UL : Registro de conversion del DAC:
#define DIR_DACR		( ( uint32_t  * ) 0x4008C000UL )
//0x4008C004UL : Registro de control del DAC:
#define DIR_DACCTRL	( ( uint32_t  * ) 0x4008C004UL )
//0x4008C008UL : Registro de contador del DAC:
#define DIR_DACCNTVAL 	( ( uint32_t  * ) 0x4008C008UL )

/*
// VIEJOS Registros del SysTick:
#define 	MAX_TICKS		0x00FFFFFFUL
//0xE000E010UL: Registro de control del SysTick:
#define 	DIR_STCTRL		( ( uint32_t  * ) 0xE000E010UL )
//0xE000E014UL: Registro de recarga del SysTick:
#define 	DIR_STRELOAD	( ( uint32_t  * ) 0xE000E014UL )
//0xE000E018UL: Registro de cuenta del SysTick:
#define 	DIR_STCURR		( ( uint32_t  * ) 0xE000E018UL )
//0xE000E01CUL: Registro de calibracion del SysTick:
#define 	DIR_STCALIB		( ( uint32_t  * ) 0xE000E01CUL )
*/

/*******************************************************************************
 *** MACROS
 ******************************************************************************/

// Registros de Nvic:
#define		ISER0		ISER[0]
#define		ISER1		ISER[1]

#define		ICER0		ICER[0]
#define		ICER1		ICER[1]

#define		ISPR0		ISPR[0]
#define		ISPR1		ISPR[1]

#define		ICPR0		ICPR[0]
#define		ICPR1		ICPR[1]

#define		IABR0		IABR[0]
#define		IABR1		IABR[1]

#define		IPR0		IPR[0]
#define		IPR1		IPR[1]
#define		IPR2		IPR[2]
#define		IPR3		IPR[3]
#define		IPR4		IPR[4]
#define		IPR5		IPR[5]
#define		IPR6		IPR[6]
#define		IPR7		IPR[7]
#define		IPR8		IPR[8]

#define		STIR		STIR_[0]

// Registros del DAC:
#define		DACR		DIR_DACR[0]
#define		DACCTRL	DIR_DACCTRL[0]
#define		DACCNTVAL	DIR_DACCNTVAL[0]

/*******************************************************************************
 *** HARDWARE
 ******************************************************************************/

// Puertos
#define		PORT0				0
#define		PORT1				1
#define		PORT2				2
#define		PORT3				3
#define		PORT4				4

// Reles
#define		RELE1				PORT2,0
#define		RELE2				PORT0,23
#define		RELE3				PORT0,21
#define		RELE4				PORT0,27

// Entradas digitales:
#define		ED0				PORT1,26
#define		ED1				PORT4,29
#define		ED2				PORT2,11

// Identificación de los puertos de expansion:
#define		EXPANSION0			PORT2,7
#define		EXPANSION1			PORT1,29
#define		EXPANSION2			PORT4,28
#define		EXPANSION3			PORT1,23
#define		EXPANSION4			PORT1,20
#define		EXPANSION5			PORT0,19
#define		EXPANSION6			PORT3,26
#define		EXPANSION7			PORT1,25
#define		EXPANSION8			PORT1,22
#define		EXPANSION9			PORT1,19
#define		EXPANSION10		PORT0,20
#define		EXPANSION11		PORT3,25
#define		EXPANSION12		PORT1,27
#define		EXPANSION13		PORT1,24
#define		EXPANSION14		PORT1,21
#define		EXPANSION15		PORT1,18
#define		EXPANSION16		PORT2,8
#define		EXPANSION17		PORT2,12


/*******************************************************************************
 *** GENERALES
 ******************************************************************************/

#define		FPRINCIPAL			4000000 // Frecuencia para encender el led

//			NVIC
#define		IPR_0				0
#define		IPR_1				1
#define		IPR_2				2
#define		IPR_3				3
#define		IPR_4				4
#define		IPR_5				5
#define		IPR_6				6
#define		IPR_7				7
#define		IPR_8				8

#define		WDT_IRQn			0
#define		TIMER0_IRQn		1
#define		TIMER1_IRQn		2
#define		TIMER2_IRQn		3
#define		TIMER3_IRQn		4
#define		UART0_IRQn			5
#define		UART1_IRQn			6
#define		UART2_IRQn			7
#define		UART3_IRQn			8
#define		PWM1_IRQn			9
#define		I2C0_IRQn			10
#define		I2C1_IRQn			11
#define		I2C2_IRQn			12
#define		SPI_IRQn			13
#define		SSP0_IRQn			14
#define		SSP1_IRQn			15
#define		PLL0_IRQn			16
#define		RTC_IRQn			17
#define		EINT0_IRQn			18
#define		EINT1_IRQn			19
#define		EINT2_IRQn			20
#define		EINT3_IRQn			21
#define		ADC_IRQn			22
#define		BOD_IRQn			23
#define		USB_IRQn			24
#define		CAN_IRQn			25
#define		DMA_IRQn			26
#define		I2S_IRQn			27
#define		ENET_IRQn			28
#define		RIT_IRQn			29
#define		MCPWM_IRQn			30
#define		QEI_IRQn			31
#define		PLL1_IRQn			32
#define		USBActivity_IRQn	33
#define		CANActivity_IRQn	34

//			Buzzer
#define		BUZZ				PORT0,28
// INICIALIZACION BUZZ
//	GPIO_Pinsel(BUZZ,PINSEL_GPIO);			// Inicializa el pin como GPIO
//	GPIO_PinMode(BUZZ,PINMODE_PULLUP);		// Le asigna una resistencia de pull-up
//	GPIO_PinModeOD(BUZZ,PINMODE_NOT_OD);	// No le asigna open drain
//	GPIO_FioDir(BUZZ,SALIDA);				// Setea el pin como salida

//			Trama configuración
#define		INICIO_TRAMA		0
#define		DATO				1
#define		MODO				2
#define		HORA				3
#define		MINUTOS			4
#define		SEGUNDOS			5
#define		PUNTO				6
#define		TEMPERATURA_MAX	7
#define		TEMPERATURA_MIN	8
#define		CHECK_TRAMA		9
#define		FIN_TRAMA			10

#define		NADA				0
#define		RTC				1
#define		HORA_INICIO		2
#define		HORA_FIN			3


/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* REGISTROS_H_ */
