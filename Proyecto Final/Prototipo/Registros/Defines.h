/*******************************************************************************
 *
 * @file       Defines.h
 * @brief      Módulo con todos lo headers.
 * @version    1.00
 * @date       12/06/2020
 * @author	Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef DEFINES_H_
#define DEFINES_H_

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <cr_section_macros.h>
#include <Control_Garage/Primitivas/PR_Control_Garage.h>
#include <Maquina_Configuracion.h>
#include <Maquina_Menu.h>
#include <Maquina_Simulacion.h>
#include <Maquina_Domotica.h>
#include <Maquina_Luz.h>
#include <Primitivas/PR_RTC.h>
#include <Primitivas/PR_Teclado.h>
#include <Primitivas/PR_Timer.h>
#include <Primitivas/PR_LCD.h>
#include <Primitivas/PR_RGB.h>
#include <Primitivas/PR_I2C.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <Sensor_Humo/Primitivas/PR_Sensor_Humo.h>
#include <Sensor_Luz/Primitivas/PR_Sensor_Luz.h>
#include <Sensor_Porton/Primitivas/PR_Sensor_Porton.h>
#include "Registros.h"

// Función de inicialización
#include "Inicializacion.h"

// Drivers
#include "Oscilador.h"
#include "Maquina_Porton.h"
#include "Maquina_Teclado.h"	// ESTA AL PEDO
#include "Trama_Enviar_Qt.h"
#include "Trama_Recibir_Qt.h"

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* DEFINES_H_ */
