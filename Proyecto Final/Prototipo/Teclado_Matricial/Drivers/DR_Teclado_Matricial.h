/*******************************************************************************
 *
 * @file       DR_Teclado_Matricial.h
 * @brief      Módulo con las funciones para teclado matricial.
 * @version    1.00
 * @date       18/05/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef DRIVERS_DR_TECLADO_MATRICIAL_H_
#define DRIVERS_DR_TECLADO_MATRICIAL_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <Drivers/DR_GPIO.h>
#include "Types.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/
#define	PORT0							0
#define	PORT1							1
#define	PORT2							2
#define	PORT3							3
#define	PORT4							4

#define 	EXPANSION7						PORT1,25
#define 	EXPANSION8						PORT1,22
#define 	EXPANSION9						PORT1,19
#define 	EXPANSION10					PORT0,20
#define 	EXPANSION11					PORT3,25
#define 	EXPANSION12					PORT1,27

#define	FILA0							EXPANSION7
#define	FILA1							EXPANSION8
#define	FILA2							EXPANSION9
#define	FILA3							EXPANSION10

#define	COLUMNA0						EXPANSION11
#define	COLUMNA1						EXPANSION12

#define	TECLAMATRICIAL1				1
#define	TECLAMATRICIAL2				2
#define	TECLAMATRICIAL3				3
#define	TECLAMATRICIAL4				4
#define	TECLAMATRICIAL5				5
#define	TECLAMATRICIAL6				6
#define	TECLAMATRICIAL7				7
#define	TECLAMATRICIAL8				8
#define	TECLAMATRICIAL1_MANTENIDA		11
#define	TECLAMATRICIAL2_MANTENIDA		12
#define	TECLAMATRICIAL3_MANTENIDA		13
#define	TECLAMATRICIAL4_MANTENIDA		14
#define	TECLAMATRICIAL5_MANTENIDA		15
#define	TECLAMATRICIAL6_MANTENIDA		16
#define	TECLAMATRICIAL7_MANTENIDA		17
#define	TECLAMATRICIAL8_MANTENIDA		18

#define	NO_TECLA						1000
#define	CANT_REB_TECLADO				20
#define	CANT_RST_TECLADO				250
#define	TAMANIO_COLA					25

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			uint32_t Teclado_Matricial_Leer_Hardware(void)
 * @brief		Identifico a la tecla presionada
 * @param[in]	void
 * @return		Número que identifica a la tecla presionada
 ******************************************************************************/
uint32_t Teclado_Matricial_Leer_Hardware(void);

/*******************************************************************************
 * @fn			void Teclado_Matricial_Antirebote(uint32_t tecla_actual)
 * @brief		Determina si está presionada una tecla o es ruido
 * @param[in]	tecla_actual: Tecla que se detecta como presionada
 * @return		void
 ******************************************************************************/
void Teclado_Matricial_Antirebote(uint32_t tecla_actual);

/*******************************************************************************
 * @fn			void Barrido_Teclado_Matricial(void)
 * @brief		Verifica si hay alguna tecla presionada
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Barrido_Teclado_Matricial(void);

/*******************************************************************************
 * @fn			void Teclado_Matricial_Push(uint32_t tecla)
 * @brief		Guarda en el Buffer la tecla presionada
 * @param[in]	tecla: Tecla presionada
 * @return		void
 ******************************************************************************/
void Teclado_Matricial_Push(uint32_t tecla);

/*******************************************************************************
 * @fn			int32_t Teclado_Matricial_Pop(uint32_t *tecla)
 * @brief		Obtengo del Buffer la última tecla presionada
 * @param[in]	*tecla: Puntero a variable donde guardo la tecla presionada.
 * @return		void
 ******************************************************************************/
int32_t Teclado_Matricial_Pop(uint32_t *dato);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* DRIVERS_DR_TECLADO_MATRICIAL_H_ */
