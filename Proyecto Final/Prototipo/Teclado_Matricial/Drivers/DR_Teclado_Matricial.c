/*******************************************************************************
 *
 * @file       DR_Teclado_Matricial.c
 * @brief      Módulo con las funciones para teclado matricial.
 * @version    1.00
 * @date       18/05/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <Drivers/DR_Teclado_Matricial.h>

/*******************************************************************************
*** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
uint32_t Buffer_teclado_matricial[TAMANIO_COLA];
static uint8_t in	= 0;
static uint8_t out	= 0;

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
uint32_t Teclado_Matricial_Leer_Hardware(void) {

    // Si no llega a leer las teclas presionadas puedo duplicar esta linea
    // Pongo la cantidad que haga falta hasta que lea correctamente
	GPIO_Set(FILA0, BAJO);	GPIO_Set(FILA1, ALTO);
	GPIO_Set(FILA2, ALTO);	GPIO_Set(FILA3, ALTO);
//	GPIO_Set(FILA0, BAJO);	GPIO_Set(FILA1, ALTO);
//	GPIO_Set(FILA2, ALTO);	GPIO_Set(FILA3, ALTO);

    if (0 == GPIO_Get(COLUMNA0))	return TECLAMATRICIAL4;
    if (0 == GPIO_Get(COLUMNA1))	return TECLAMATRICIAL5;

	GPIO_Set(FILA0, ALTO);	GPIO_Set(FILA1, BAJO);
	GPIO_Set(FILA2, ALTO);	GPIO_Set(FILA3, ALTO);
//	GPIO_Set(FILA0, ALTO);	GPIO_Set(FILA1, BAJO);
//	GPIO_Set(FILA2, ALTO);	GPIO_Set(FILA3, ALTO);

    if (0 == GPIO_Get(COLUMNA0))	return TECLAMATRICIAL1;
    if (0 == GPIO_Get(COLUMNA1))	return TECLAMATRICIAL8;

	GPIO_Set(FILA0, ALTO);	GPIO_Set(FILA1, ALTO);
	GPIO_Set(FILA2, BAJO);	GPIO_Set(FILA3, ALTO);
//	GPIO_Set(FILA0, ALTO);	GPIO_Set(FILA1, ALTO);
//	GPIO_Set(FILA2, BAJO);	GPIO_Set(FILA3, ALTO);

    if (0 == GPIO_Get(COLUMNA0))	return TECLAMATRICIAL3;
    if (0 == GPIO_Get(COLUMNA1))	return TECLAMATRICIAL6;

	GPIO_Set(FILA0, ALTO);	GPIO_Set(FILA1, ALTO);
	GPIO_Set(FILA2, ALTO);	GPIO_Set(FILA3, BAJO);
//	GPIO_Set(FILA0, ALTO);	GPIO_Set(FILA1, ALTO);
//	GPIO_Set(FILA2, ALTO);	GPIO_Set(FILA3, BAJO);

    if (0 == GPIO_Get(COLUMNA0))	return TECLAMATRICIAL2;
    if (0 == GPIO_Get(COLUMNA1))	return TECLAMATRICIAL7;

    return NO_TECLA;
}

void Teclado_Matricial_Antirebote(uint32_t tecla_actual) {
    static uint32_t tecla_anterior = NO_TECLA;
    static uint32_t debounce = (CANT_REB_TECLADO + CANT_RST_TECLADO);

    if(tecla_anterior == tecla_actual) {
    	debounce--;
        if(CANT_RST_TECLADO == debounce)
        	Teclado_Matricial_Push(tecla_actual);
        if(CANT_REB_TECLADO == debounce)
        	Teclado_Matricial_Push(tecla_actual + 10);
	}
    else
    	debounce = (CANT_REB_TECLADO + CANT_RST_TECLADO);

    tecla_anterior = tecla_actual;
}

void Barrido_Teclado_Matricial(void) {
    uint32_t tecla = 0;
    tecla = Teclado_Matricial_Leer_Hardware();
    Teclado_Matricial_Antirebote(tecla);
}

void Teclado_Matricial_Push(uint32_t tecla) {
	Buffer_teclado_matricial[in] = tecla;
	in++;
	if (in >= TAMANIO_COLA)
		in = 0;
}

int32_t Teclado_Matricial_Pop(uint32_t *dato) {
	if(in == out) // Buffer vacio
		return -1;
	*dato = Buffer_teclado_matricial[out];
	out++;
	if(out >= TAMANIO_COLA)
		out = 0;
	return 0;
}
