/*******************************************************************************
 *
 * @file       PR_Teclado_Matricial.c
 * @brief      Módulo con las funciones primitivas para teclado matricial.
 * @version    1.00
 * @date       18/05/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <Primitivas/PR_Teclado_Matricial.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
uint32_t Teclado_Matricial_Leer(void) {
    int32_t temp;
    uint32_t tecla;

    temp = Teclado_Matricial_Pop(&tecla);

    if(-1 == temp)
        return NO_TECLA;
    else
        return tecla;

//	  ALTERNATIVA QUE ME PARECE MEJOR PERO HABRIA QUE PROBARLA
//    if(-1 == temp)
//    	tecla = NO_TECLA;
//
//    return tecla;
}
