/*******************************************************************************
 *
 * @file       DR_RGB.c
 * @brief      Módulo para el uso del led RGB.
 * @version    1.00
 * @date       26/10/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <Drivers/DR_RGB.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void Led_RGB_Rojo(void) {
	GPIO_Set(RGBB,BAJO);
	GPIO_Set(RGBG,BAJO);
	GPIO_Set(RGBR,ALTO);
}

void Led_RGB_Verde(void) {
	GPIO_Set(RGBB,BAJO);
	GPIO_Set(RGBG,ALTO);
	GPIO_Set(RGBR,BAJO);
}

void Led_RGB_Azul(void) {
	GPIO_Set(RGBB,ALTO);
	GPIO_Set(RGBG,BAJO);
	GPIO_Set(RGBR,BAJO);
}

void Led_RGB_Celeste(void) {
	GPIO_Set(RGBB,ALTO);
	GPIO_Set(RGBG,ALTO);
	GPIO_Set(RGBR,BAJO);
}

void Led_RGB_Violeta(void) {
	GPIO_Set(RGBB,ALTO);
	GPIO_Set(RGBG,BAJO);
	GPIO_Set(RGBR,ALTO);
}

void Led_RGB_Amarillo(void) {
	GPIO_Set(RGBB,BAJO);
	GPIO_Set(RGBG,ALTO);
	GPIO_Set(RGBR,ALTO);
}

void Led_RGB_Blanco(void) {
	GPIO_Set(RGBB,ALTO);
	GPIO_Set(RGBG,ALTO);
	GPIO_Set(RGBR,ALTO);
}

void Led_RGB_OFF(void) {
	GPIO_Set(RGBB,BAJO);
	GPIO_Set(RGBG,BAJO);
	GPIO_Set(RGBR,BAJO);
}
