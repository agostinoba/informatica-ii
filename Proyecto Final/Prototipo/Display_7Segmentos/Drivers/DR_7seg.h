/*******************************************************************************
 *
 * @file       DR_7seg.h
 * @brief      Módulo con las funciones para el display de 7 segmentos.
 * @version    1.00
 * @date       05/10/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef DRIVERS_DR_7SEG_H_
#define DRIVERS_DR_7SEG_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <Drivers/DR_GPIO.h>
#include "Types.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/
#define		PORT0			0
#define		PORT1			1
#define		PORT2			2
#define		PORT3			3
#define		PORT4			4
#define		PORT5			5

#define		EXPANSION0		PORT2,7
#define		EXPANSION1		PORT1,29
#define		EXPANSION2		PORT4,28
#define		EXPANSION3		PORT1,23
#define		EXPANSION4		PORT1,20
#define		EXPANSION5		PORT0,19
#define		EXPANSION6		PORT3,26
#define		EXPANSION9		PORT1,19
#define		EXPANSION10	PORT0,20
#define		EXPANSION11	PORT3,25
#define		EXPANSION12	PORT1,27
#define		EXPANSION13	PORT1,24
#define		EXPANSION14	PORT1,21
#define		EXPANSION15	PORT1,18

// DE EXPANSION 2
#define		DIGITO0		EXPANSION5
#define		DIGITO1		EXPANSION4
#define		DIGITO2		EXPANSION3
#define		DIGITO3		EXPANSION2
#define		DIGITO4		EXPANSION1
#define		DIGITO5		EXPANSION0
#define		SEGMENTO_A		EXPANSION6
#define		SEGMENTO_B		EXPANSION15
#define		SEGMENTO_C		EXPANSION14
#define		SEGMENTO_D		EXPANSION13
#define		SEGMENTO_E		EXPANSION12
#define		SEGMENTO_F		EXPANSION11
#define		SEGMENTO_G		EXPANSION10
#define		SEGMENTO_DP	EXPANSION9

// DE EXPANSION 3
//#define 		SEGMENTO_A				EXPANSION0
//#define 		SEGMENTO_B				EXPANSION1
//#define 		SEGMENTO_C				EXPANSION2
//#define 		SEGMENTO_D				EXPANSION3
//#define 		SEGMENTO_DP			EXPANSION4
//#define 		DIGITO_CONTADOR_RST	EXPANSION6
//#define 		DIGITO_CONTADOR_CLK	EXPANSION5

#define		MAX_DIGITOS		6

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/
extern uint8_t MSG_DSP[MAX_DIGITOS];

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void Display_7Seg_Inicializar(void)
 * @brief		Función de inicialización del display de 7 segmentos.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Display_7Seg_Inicializar(void);

/*******************************************************************************
 * @fn			void Barrido_Display_7Seg(void)
 * @brief		Función para el encendido en orden de los segmentos del display.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Barrido_Display_7Seg(void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* DRIVERS_DR_7SEG_H_ */
