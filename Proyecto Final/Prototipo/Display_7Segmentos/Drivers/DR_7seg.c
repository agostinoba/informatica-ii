/*******************************************************************************
 *
 * @file       DR_7seg.c
 * @brief      Módulo con las funciones para el display de 7 segmentos.
 * @version    1.00
 * @date       05/10/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "Drivers/DR_7seg.h"

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/
const uint8_t Tabla_Digitos_BCD_7seg[] = {
					// | Numero | g f e d c b a |
		0x3F,		// |   0	| 0 1 1 1 1 1 1 |
		0x06,		// |   1	| 0 0 0 0 1 1 0 |
		0x5B,		// |   2	| 1 0 1 1 0 1 1 |
		0x4F,		// |   3	| 1 0 0 1 1 1 1 |
		0x66,		// |   4	| 1 1 0 0 1 1 0 |
		0x6D,		// |   5	| 1 1 0 1 1 0 1 |
		0x7C,		// |   6	| 1 1 1 1 1 0 1 |
		0x07,		// |   7	| 0 0 0 0 1 1 1 |
		0x7F,		// |   8	| 1 1 1 1 1 1 1 |
		0x6F		// |   9	| 1 1 0 1 1 1 1 |
};

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/
const uint8_t Tabla_Digitos_BCD_7seg[] = {
uint8_t MSG_DSP[MAX_DIGITOS];

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void Display_7Seg_Inicializar(void) {
	GPIO_Pinsel(DIGITO0, PINSEL_GPIO);
	GPIO_FioDir(DIGITO0, SALIDA);
	GPIO_PinMode(DIGITO0, PINMODE_PULLUP);
	GPIO_PinModeOD(DIGITO0, PINMODE_NOT_OD);

	GPIO_Pinsel(DIGITO1, PINSEL_GPIO);
	GPIO_FioDir(DIGITO1, SALIDA);
	GPIO_PinMode(DIGITO1, PINMODE_PULLUP);
	GPIO_PinModeOD(DIGITO1, PINMODE_NOT_OD);

	GPIO_Pinsel(DIGITO2, PINSEL_GPIO);
	GPIO_FioDir(DIGITO2, SALIDA);
	GPIO_PinMode(DIGITO2, PINMODE_PULLUP);
	GPIO_PinModeOD(DIGITO2, PINMODE_NOT_OD);

	GPIO_Pinsel(DIGITO3, PINSEL_GPIO);
	GPIO_FioDir(DIGITO3, SALIDA);
	GPIO_PinMode(DIGITO3, PINMODE_PULLUP);
	GPIO_PinModeOD(DIGITO3, PINMODE_NOT_OD);

	GPIO_Pinsel(DIGITO4, PINSEL_GPIO);
	GPIO_FioDir(DIGITO4, SALIDA);
	GPIO_PinMode(DIGITO4, PINMODE_PULLUP);
	GPIO_PinModeOD(DIGITO4, PINMODE_NOT_OD);

	GPIO_Pinsel(DIGITO5, PINSEL_GPIO);
	GPIO_FioDir(DIGITO5, SALIDA);
	GPIO_PinMode(DIGITO5, PINMODE_PULLUP);
	GPIO_PinModeOD(DIGITO5, PINMODE_NOT_OD);

	GPIO_Pinsel(SEGMENTO_A, PINSEL_GPIO);
	GPIO_FioDir(SEGMENTO_A, SALIDA);
	GPIO_PinMode(SEGMENTO_A, PINMODE_PULLUP);
	GPIO_PinModeOD(SEGMENTO_A, PINMODE_NOT_OD);

	GPIO_Pinsel(SEGMENTO_B, PINSEL_GPIO);
	GPIO_FioDir(SEGMENTO_B, SALIDA);
	GPIO_PinMode(SEGMENTO_B, PINMODE_PULLUP);
	GPIO_PinModeOD(SEGMENTO_B, PINMODE_NOT_OD);

	GPIO_Pinsel(SEGMENTO_C, PINSEL_GPIO);
	GPIO_FioDir(SEGMENTO_C, SALIDA);
	GPIO_PinMode(SEGMENTO_C, PINMODE_PULLUP);
	GPIO_PinModeOD(SEGMENTO_C, PINMODE_NOT_OD);

	GPIO_Pinsel(SEGMENTO_D, PINSEL_GPIO);
	GPIO_FioDir(SEGMENTO_D, SALIDA);
	GPIO_PinMode(SEGMENTO_D, PINMODE_PULLUP);
	GPIO_PinModeOD(SEGMENTO_D, PINMODE_NOT_OD);

	GPIO_Pinsel(SEGMENTO_E, PINSEL_GPIO);
	GPIO_FioDir(SEGMENTO_E, SALIDA);
	GPIO_PinMode(SEGMENTO_E, PINMODE_PULLUP);
	GPIO_PinModeOD(SEGMENTO_E, PINMODE_NOT_OD);

	GPIO_Pinsel(SEGMENTO_F, PINSEL_GPIO);
	GPIO_FioDir(SEGMENTO_F, SALIDA);
	GPIO_PinMode(SEGMENTO_F, PINMODE_PULLUP);
	GPIO_PinModeOD(SEGMENTO_F, PINMODE_NOT_OD);

	GPIO_Pinsel(SEGMENTO_G, PINSEL_GPIO);
	GPIO_FioDir(SEGMENTO_G, SALIDA);
	GPIO_PinMode(SEGMENTO_G, PINMODE_PULLUP);
	GPIO_PinModeOD(SEGMENTO_G, PINMODE_NOT_OD);

	GPIO_Pinsel(SEGMENTO_DP, PINSEL_GPIO);
	GPIO_FioDir(SEGMENTO_DP, SALIDA);
	GPIO_PinMode(SEGMENTO_DP, PINMODE_PULLUP);
	GPIO_PinModeOD(SEGMENTO_DP, PINMODE_NOT_OD);
}

void Barrido_Display_7Seg(void) {
    static uint8_t indice_display = 0;
    uint8_t digito;

    digito = Tabla_Digitos_BCD_7seg[MSG_DSP[indice_display]];

    GPIO_Set(DIGITO0, BAJO);
    GPIO_Set(DIGITO1, BAJO);
    GPIO_Set(DIGITO2, BAJO);
    GPIO_Set(DIGITO3, BAJO);
    GPIO_Set(DIGITO4, BAJO);
    GPIO_Set(DIGITO5, BAJO);

    GPIO_Set(SEGMENTO_A, (digito >> 0) & 0x01);
    GPIO_Set(SEGMENTO_B, (digito >> 1) & 0x01);
    GPIO_Set(SEGMENTO_C, (digito >> 2) & 0x01);
    GPIO_Set(SEGMENTO_D, (digito >> 3) & 0x01);
    GPIO_Set(SEGMENTO_E, (digito >> 4) & 0x01);
    GPIO_Set(SEGMENTO_F, (digito >> 5) & 0x01);
    GPIO_Set(SEGMENTO_G, (digito >> 6) & 0x01);

    switch (indice_display) {

        case 0:
        	GPIO_Set(DIGITO0, ALTO);
            break;
        case 1:
        	GPIO_Set(DIGITO1, ALTO);
            break;
        case 2:
        	GPIO_Set(DIGITO2, ALTO);
            break;
        case 3:
        	GPIO_Set(DIGITO3, ALTO);
            break;
        case 4:
        	GPIO_Set(DIGITO4, ALTO);
            break;
        case 5:
        	GPIO_Set(DIGITO5, ALTO);
            break;
    }

    indice_display ++;
    indice_display %= MAX_DIGITOS;
}
