/*******************************************************************************
 *
 * @file       DR_Teclado.h
 * @brief      Módulo con las funciones primitivas de teclado.
 * @version    1.00
 * @date       12/05/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef DRIVERS_DR_TECLADO_H_
#define DRIVERS_DR_TECLADO_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <Drivers/DR_GPIO.h>
#include "Types.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

#define		PORT0				0
#define		PORT1				1
#define		PORT2				2

#define		SWITCH1			PORT2,10
#define		SWITCH2			PORT0,18
#define		SWITCH3			PORT0,11
#define		SWITCH4			PORT2,13
#define		SWITCH5			PORT1,26

#define		TECLA1				1
#define		TECLA2				2
#define		TECLA3				3
#define		TECLA4				4
#define		TECLA5				5
#define		TECLA1_MANTENIDA	11
#define		TECLA2_MANTENIDA	12
#define		TECLA3_MANTENIDA	13
#define		TECLA4_MANTENIDA	14
#define		TECLA5_MANTENIDA	15

#define		NO_TECLA			1000
#define		CANT_REB_TECLADO	20
#define		CANT_RST_TECLADO	250
#define		TAMANIO_COLA		25

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void Teclado_Antirebote(uint32_t tecla_actual)
 * @brief		Determina si está presionada una tecla o es ruido
 * @param[in]	tecla_actual: Tecla que se detecta como presionada
 * @return		void
 ******************************************************************************/
void Teclado_Antirebote(uint32_t tecla_actual);

/*******************************************************************************
 * @fn			void Barrido_Teclado(void)
 * @brief		Verifica si hay alguna tecla presionada
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Barrido_Teclado(void);

/*******************************************************************************
 * @fn			uint32_t Teclado_Leer_Hardware(void)
 * @brief		Identifico a la tecla presionada
 * @param[in]	void
 * @return		Número que identifica a la tecla presionada
 ******************************************************************************/
uint32_t Teclado_Leer_Hardware(void);

/*******************************************************************************
 * @fn			void Teclado_Push(uint32_t tecla)
 * @brief		Guarda en el Buffer la tecla presionada
 * @param[in]	tecla: Tecla presionada
 * @return		void
 ******************************************************************************/
void Teclado_Push (uint32_t tecla);

/*******************************************************************************
 * @fn			int32_t Teclado_Pop(uint32_t *tecla)
 * @brief		Obtengo del Buffer la última tecla presionada
 * @param[in]	tecla: Puntero a variable donde guardo que tecla se presionó
 * @return		void
 ******************************************************************************/
int32_t Teclado_Pop (uint32_t *tecla);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* DRIVERS_DR_TECLADO_H_ */
