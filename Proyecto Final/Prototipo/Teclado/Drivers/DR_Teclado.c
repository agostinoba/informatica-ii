/*******************************************************************************
 *
 * @file       DR_Teclado.c
 * @brief      Módulo con las funciones primitivas de teclado.
 * @version    1.00
 * @date       12/05/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <Drivers/DR_Teclado.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
uint32_t Buffer_teclado[TAMANIO_COLA];
static uint8_t in	= 0;
static uint8_t out	= 0;

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void Teclado_Antirebote(uint32_t tecla_actual) {
    static uint32_t tecla_anterior = NO_TECLA;
    static uint32_t debounce = (CANT_REB_TECLADO + CANT_RST_TECLADO);

    if(tecla_anterior == tecla_actual) {
    	debounce--;
        if(CANT_RST_TECLADO == debounce)
            Teclado_Push(tecla_actual);
        if(CANT_REB_TECLADO == debounce)
			Teclado_Push(tecla_actual + 10);
	}
    else
    	debounce = (CANT_REB_TECLADO + CANT_RST_TECLADO);

    tecla_anterior = tecla_actual;
}

uint32_t Teclado_Leer_Hardware(void) { // Checkea el estado de cada tecla
    if (GPIO_Get(SWITCH1) == 0)
        return TECLA1;
    if (GPIO_Get(SWITCH2) == 0)
        return TECLA2;
    if (GPIO_Get(SWITCH3) == 0)
        return TECLA3;
    if (GPIO_Get(SWITCH4) == 0)
        return TECLA4;
    if (GPIO_Get(SWITCH5) == 0)
        return TECLA5;
    return NO_TECLA;
}

void Barrido_Teclado(void) { // Corrobora que se haya presionado una tecla
    uint32_t tecla = 0;
    tecla = Teclado_Leer_Hardware();
    Teclado_Antirebote(tecla);
}

void Teclado_Push(uint32_t tecla) { // Guarda la tecla presionada en el Buffer
	Buffer_teclado[in] = tecla;
	in++;
	if (in >= TAMANIO_COLA)
		in = 0;
}

int32_t Teclado_Pop(uint32_t *dato) { // Retorna el estado del Buffer
	if(in == out) // Buffer vacio
		return -1;
	*dato = Buffer_teclado[out];
	out++;
	if(out >= TAMANIO_COLA)
		out = 0;
	return 0;
}
