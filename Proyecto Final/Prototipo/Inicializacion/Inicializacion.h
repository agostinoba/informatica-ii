/*******************************************************************************
 *
 * @file       Inicializacion.h
 * @brief      Módulo para inicializar el sistema.
 * @version    1.00
 * @date       23/06/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef INICIALIZACION_H_
#define INICIALIZACION_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "Registros.h"
#include "Types.h"
#include <Drivers/DR_GPIO.h>
#include <Drivers/DR_RTC.h>
#include <Drivers/DR_Systick.h>
#include <Drivers/DR_UART.h>
#include <Primitivas/PR_RGB.h>
#include "Drivers/DR_ADC.h"
#include "Drivers/DR_I2C.h"
#include "Oscilador.h"
#include "LCD.h"
#include "Trama_Recibir_Qt.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void Inicializar(void)
 * @brief		Función para inicializar todo el sistema
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Inicializar(void);

/*******************************************************************************
 * @fn			void GPIO_Inicializar(void)
 * @brief		Función de inicialización del GPIO
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void GPIO_Inicializar(void);

/*******************************************************************************
 * @fn			void SYSTICK_Inicializar(void)
 * @brief		Función de inicialización del SYSTICK
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Systick_Inicializar(void);

/*******************************************************************************
 * @fn			void Switch_Inicializar(void)
 * @brief		Función de inicialización los Switch
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Switch_Inicializar(void);

/*******************************************************************************
 * @fn			void Rele_Inicializar(void)
 * @brief		Función de inicialización los Reles
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Rele_Inicializar(void);

/*******************************************************************************
 * @fn			void RGB_Inicializar(void)
 * @brief		Función de inicialización el Led RGB
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void RGB_Inicializar(void);

/*******************************************************************************
 * @fn			void LCD_Inicializar(void)
 * @brief		Función de inicialización del LCD
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void LCD_Inicializar(void);

/*******************************************************************************
 * @fn			void UART_Inicializar(void)
 * @brief		Función de inicialización de la UART
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void UART_Inicializar(void);

/*******************************************************************************
 * @fn			void Teclado_Matricial_Inicializar(void)
 * @brief		Función de inicialización del teclado matricial
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Teclado_Matricial_Inicializar(void);

/*******************************************************************************
 * @fn			void Sensores_Inicializar(void)
 * @brief		Función de inicialización de los puertos de expansion
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Sensores_Inicializar(void);

/*******************************************************************************
 * @fn			void Salidas_Inicializar(void)
 * @brief		Función de inicialización de las luces
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Salidas_Inicializar(void);

/*******************************************************************************
 * @fn			void I2C0_Inicializar(void)
 * @brief		Función de inicialización de los puertos del I2C0
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void I2C0_Inicializar(void);

/*******************************************************************************
 * @fn			void I2C1_Inicializar(void)
 * @brief		Función de inicialización de los puertos del I2C1.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void I2C1_Inicializar(void);

/*******************************************************************************
 * @fn			void I2C2_Inicializar(void)
 * @brief		Función de inicialización de los puertos del I2C2.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void I2C2_Inicializar(void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* INICIALIZACION_H_ */
