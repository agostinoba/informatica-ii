/*******************************************************************************
 *
 * @file       Inicializar.c
 * @brief      Módulo de Firmware con las funciones inicializadoras.
 * @version    1.00
 * @date       23/06/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "Inicializacion.h"

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/

uint8_t hora[] = {23, 45, 56};

void Inicializar(void) {
	PLL_Inicializar();
//	Timer_Start(4, 400, 400, Enviar_Estado);
	GPIO_Inicializar();
	RTC_Inicializar();
//	RTC_SetTime(hora);
	UART0_Init(115200);

	ADC_Inicializar();
	Systick_Inicializar();
	Config_LCD();
//	Timer_Start(5, 20,20, ADC_Leer); // ESTABA COMENTADA EN EL PROYECTO FUNCIONANDO
	Inicializar_Valores();

}

void GPIO_Inicializar(void) {
	LCD_Inicializar();
	Rele_Inicializar();
	Switch_Inicializar();
	RGB_Inicializar();
//	Sensor_Inicializar();
	UART_Inicializar();
//	Teclado_Matricial_Inicializar();
//	Salidas_Inicializar();
}

void Systick_Inicializar(void) {
	Systick_Init ();	// Antes 25 - Tiempo al que interrumpe 1/frecuencia
}

void Switch_Inicializar(void) {
	// Inicializo el switch 1 de la placa
	GPIO_Pinsel(SWITCH1, PINSEL_GPIO);
	GPIO_FioDir(SWITCH1, ENTRADA);
	GPIO_PinMode(SWITCH1, PINMODE_PULLUP);
	GPIO_PinModeOD(SWITCH1, PINMODE_NOT_OD);

	// Inicializo el switch 2 de la placa
	GPIO_Pinsel(SWITCH2, PINSEL_GPIO);
	GPIO_FioDir(SWITCH2, ENTRADA);
	GPIO_PinMode(SWITCH2, PINMODE_PULLUP);
	GPIO_PinModeOD(SWITCH2, PINMODE_NOT_OD);

	// Inicializo el switch 3 de la placa
	GPIO_Pinsel(SWITCH3, PINSEL_GPIO);
	GPIO_FioDir(SWITCH3, ENTRADA);
	GPIO_PinMode(SWITCH3, PINMODE_PULLUP);
	GPIO_PinModeOD(SWITCH3, PINMODE_NOT_OD);

	// Inicializo el switch 4 de la placa
	GPIO_Pinsel(SWITCH4, PINSEL_GPIO);
	GPIO_FioDir(SWITCH4, ENTRADA);
	GPIO_PinMode(SWITCH4, PINMODE_PULLUP);
	GPIO_PinModeOD(SWITCH4, PINMODE_NOT_OD);

	// Inicializo el switch 5 de la placa
	GPIO_Pinsel(SWITCH5, PINSEL_GPIO);
	GPIO_FioDir(SWITCH5, ENTRADA);
	GPIO_PinMode(SWITCH5, PINMODE_PULLUP);
	GPIO_PinModeOD(SWITCH5, PINMODE_NOT_OD);
}

void Rele_Inicializar(void) {
	// Inicializo led del stick (Creo que coinicde con la UART
//	GPIO_Pinsel(0, 22, PINSEL_GPIO);
//	GPIO_FioDir(0, 22, SALIDA);
//	GPIO_PinMode(0, 22, PINMODE_PULLUP);
//	GPIO_PinModeOD(0, 22, PINMODE_NOT_OD);

	// Inicializo el rele 1 de la placa
	GPIO_Pinsel(RELE1, PINSEL_GPIO);
	GPIO_FioDir(RELE1, SALIDA);
	GPIO_PinMode(RELE1, PINMODE_PULLUP);
	GPIO_PinModeOD(RELE1, PINMODE_NOT_OD);

	// Inicializo el rele 2 de la placa
	GPIO_Pinsel(RELE2, PINSEL_GPIO);
	GPIO_FioDir(RELE2, SALIDA);
	GPIO_PinMode(RELE2, PINMODE_PULLUP);
	GPIO_PinModeOD(RELE2, PINMODE_NOT_OD);

	// Inicializo el rele 3 de la placa
	GPIO_Pinsel(RELE3, PINSEL_GPIO);
	GPIO_FioDir(RELE3, SALIDA);
	GPIO_PinMode(RELE3, PINMODE_PULLUP);
	GPIO_PinModeOD(RELE3, PINMODE_NOT_OD);

	// Inicializo el rele 4 de la placa
	GPIO_Pinsel(RELE4, PINSEL_GPIO);
	GPIO_FioDir(RELE4, SALIDA);
	GPIO_PinMode(RELE4, PINMODE_PULLUP);
	GPIO_PinModeOD(RELE4, PINMODE_NOT_OD);
}

void RGB_Inicializar(void) {
	GPIO_Pinsel(RGBB,PINSEL_GPIO);			// Inicializa el pin como GPIO
	GPIO_PinMode(RGBB,PINMODE_PULLUP);		// Le asigna una resistencia de pull-up
	GPIO_PinModeOD(RGBB,PINMODE_NOT_OD);	// No le asigna open drain
	GPIO_FioDir(RGBB,SALIDA);				// Setea el pin como salida
	GPIO_Set(RGBB,BAJO);					// Le asigna un estado de BAJO

	GPIO_Pinsel(RGBR,PINSEL_GPIO);			// Inicializa el pin como GPIO
	GPIO_PinMode(RGBR,PINMODE_PULLUP);		// Le asigna una resistencia de pull-up
	GPIO_PinModeOD(RGBR,PINMODE_NOT_OD);	// No le asigna open drain
	GPIO_FioDir(RGBR,SALIDA);				// Setea el pin como salida
	GPIO_Set(RGBR,BAJO);					// Le asigna un estado de BAJO

	GPIO_Pinsel(RGBG,PINSEL_GPIO);			// Inicializa el pin como GPIO
	GPIO_PinMode(RGBG,PINMODE_PULLUP);		// Le asigna una resistencia de pull-up
	GPIO_PinModeOD(RGBG,PINMODE_NOT_OD);	// No le asigna open drain
	GPIO_FioDir(RGBG,SALIDA);				// Setea el pin como salida
	GPIO_Set(RGBG,BAJO);					// Le asigna un estado de BAJO
}

void LCD_Inicializar(void) {
	GPIO_Pinsel(LCD_D4, PINSEL_GPIO);
	GPIO_FioDir(LCD_D4, SALIDA);
	GPIO_PinMode(LCD_D4, PINMODE_PULLUP);
	GPIO_PinModeOD(LCD_D4, PINMODE_OD);
	GPIO_Set(LCD_D4, BAJO);

	GPIO_Pinsel(LCD_D5, PINSEL_GPIO);
	GPIO_FioDir(LCD_D5, SALIDA);
	GPIO_PinMode(LCD_D5, PINMODE_PULLUP);
	GPIO_PinModeOD(LCD_D5, PINMODE_OD);
	GPIO_Set(LCD_D5, BAJO);

	GPIO_Pinsel(LCD_D6, PINSEL_GPIO);
	GPIO_FioDir(LCD_D6, SALIDA);
	GPIO_PinMode(LCD_D6, PINMODE_PULLUP);
	GPIO_PinModeOD(LCD_D6, PINMODE_OD);
	GPIO_Set(LCD_D6, BAJO);

	GPIO_Pinsel(LCD_D7, PINSEL_GPIO);
	GPIO_FioDir(LCD_D7, SALIDA);
	GPIO_PinMode(LCD_D7, PINMODE_PULLUP);
	GPIO_PinModeOD(LCD_D7, PINMODE_OD);
	GPIO_Set(LCD_D7, BAJO);

	GPIO_Pinsel(LCD_RS, PINSEL_GPIO);
	GPIO_FioDir(LCD_RS, SALIDA);
	GPIO_PinMode(LCD_RS, PINMODE_PULLUP);
	GPIO_PinModeOD(LCD_RS, PINMODE_OD);
	GPIO_Set(LCD_RS, BAJO);

	GPIO_Pinsel(LCD_BF, PINSEL_GPIO);
	GPIO_FioDir(LCD_BF, ENTRADA);
	GPIO_PinMode(LCD_BF, PINMODE_PULLUP);
	GPIO_PinModeOD(LCD_BF, PINMODE_OD);
	GPIO_Set(LCD_BF, BAJO);

	GPIO_Pinsel(LCD_E, PINSEL_GPIO);
	GPIO_FioDir(LCD_E, SALIDA);
	GPIO_PinMode(LCD_E, PINMODE_PULLUP);
	GPIO_PinModeOD(LCD_E, PINMODE_OD);
	GPIO_Set(LCD_E, BAJO);
}

void UART_Inicializar(void) {
	GPIO_Pinsel(UART0_TX, PINSEL_FUNC1);
	GPIO_FioDir(UART0_TX, SALIDA);
	GPIO_PinMode(UART0_TX, PINMODE_PULLUP);
	GPIO_PinModeOD(UART0_TX, PINMODE_OD);

	GPIO_Pinsel(UART0_RX, PINSEL_FUNC1);
	GPIO_FioDir(UART0_RX, ENTRADA);
	GPIO_PinMode(UART0_RX, PINMODE_PULLUP);
	GPIO_PinModeOD(UART0_RX, PINMODE_OD);
}

void Teclado_Matricial_Inicializar(void) {
	GPIO_Pinsel(EXPANSION6, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION6, ENTRADA);
	GPIO_PinMode(EXPANSION6, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION6, PINMODE_NOT_OD);

	GPIO_Pinsel(EXPANSION7, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION7, ENTRADA);
	GPIO_PinMode(EXPANSION7, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION7, PINMODE_NOT_OD);

	GPIO_Pinsel(EXPANSION8, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION8, ENTRADA);
	GPIO_PinMode(EXPANSION8, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION8, PINMODE_NOT_OD);

	GPIO_Pinsel(EXPANSION9, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION9, ENTRADA);
	GPIO_PinMode(EXPANSION9, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION9, PINMODE_NOT_OD);

	GPIO_Pinsel(EXPANSION11, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION11, ENTRADA);
	GPIO_PinMode(EXPANSION11, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION11, PINMODE_NOT_OD);

	GPIO_Pinsel(EXPANSION12, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION12, ENTRADA);
	GPIO_PinMode(EXPANSION12, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION12, PINMODE_NOT_OD);

	GPIO_Pinsel(EXPANSION13, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION13, ENTRADA);
	GPIO_PinMode(EXPANSION13, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION13, PINMODE_NOT_OD);
}

void Sensores_Inicializar(void) {
	// Sensor PIR (movimiento)
	GPIO_Pinsel(ED0, PINSEL_GPIO);		// COINCIDE CON EL SWITCH 5
	GPIO_FioDir(ED0, ENTRADA);
	GPIO_PinMode(ED0, PINMODE_PULLUP);
	GPIO_PinModeOD(ED0, PINMODE_NOT_OD);

	// Sensor LDR (luz)
	GPIO_Pinsel(ED1, PINSEL_GPIO);
	GPIO_FioDir(ED1, ENTRADA);
	GPIO_PinMode(ED1, PINMODE_PULLUP);
	GPIO_PinModeOD(ED1, PINMODE_NOT_OD);

	// Sensor RF (control)
	GPIO_Pinsel(ED2, PINSEL_GPIO);
	GPIO_FioDir(ED2, ENTRADA);
	GPIO_PinMode(ED2, PINMODE_PULLUP);
	GPIO_PinModeOD(ED2, PINMODE_NOT_OD);

	// Sensor Magnetico
	GPIO_Pinsel(EXPANSION0, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION0, ENTRADA);
	GPIO_PinMode(EXPANSION0, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION0, PINMODE_NOT_OD);

	GPIO_Pinsel(EXPANSION1, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION1, ENTRADA);
	GPIO_PinMode(EXPANSION1, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION1, PINMODE_NOT_OD);

	// Sensor de humo
	GPIO_Pinsel(EXPANSION2, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION2, ENTRADA);
	GPIO_PinMode(EXPANSION2, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION2, PINMODE_NOT_OD);

	// Sensor fin de carrera del porton
	GPIO_Pinsel(EXPANSION3, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION3, ENTRADA);
	GPIO_PinMode(EXPANSION3, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION3, PINMODE_NOT_OD);

	GPIO_Pinsel(EXPANSION4, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION4, ENTRADA);
	GPIO_PinMode(EXPANSION4, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION4, PINMODE_NOT_OD);
}

void Salidas_Inicializar(void) {
	// Luces interiores
	GPIO_Pinsel(EXPANSION5, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION5, SALIDA);
	GPIO_PinMode(EXPANSION5, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION5, PINMODE_OD);
	GPIO_Set(EXPANSION5, BAJO);

	GPIO_Pinsel(EXPANSION10, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION10, SALIDA);
	GPIO_PinMode(EXPANSION10, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION10, PINMODE_OD);
	GPIO_Set(EXPANSION10, BAJO);

	GPIO_Pinsel(EXPANSION14, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION14, SALIDA);
	GPIO_PinMode(EXPANSION14, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION14, PINMODE_OD);
	GPIO_Set(EXPANSION14, BAJO);

	GPIO_Pinsel(EXPANSION15, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION15, SALIDA);
	GPIO_PinMode(EXPANSION15, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION15, PINMODE_OD);
	GPIO_Set(EXPANSION15, BAJO);

	// Luz patio
	GPIO_Pinsel(EXPANSION16, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION16, SALIDA);
	GPIO_PinMode(EXPANSION16, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION16, PINMODE_OD);
	GPIO_Set(EXPANSION16, BAJO);

	// Luz frente
	GPIO_Pinsel(EXPANSION17, PINSEL_GPIO);
	GPIO_FioDir(EXPANSION17, SALIDA);
	GPIO_PinMode(EXPANSION17, PINMODE_PULLUP);
	GPIO_PinModeOD(EXPANSION17, PINMODE_OD);
	GPIO_Set(EXPANSION17, BAJO);
}

void I2C0_Inicializar(void) {
	// Pag 117
	GPIO_Pinsel(SDA0, PINSEL_FUNC1);
	GPIO_FioDir(SDA0, SALIDA);
	GPIO_PinMode(SDA0, PINMODE_NONE);
	GPIO_PinModeOD(SDA0, PINMODE_OD);

	GPIO_Pinsel(SCL0, PINSEL_FUNC1);
	GPIO_FioDir(SCL0, SALIDA);
	GPIO_PinMode(SCL0, PINMODE_NONE);
	GPIO_PinModeOD(SCL0, PINMODE_OD);
}

void I2C1_Inicializar(void) {
	GPIO_Pinsel(SDA1, PINSEL_FUNC3);
	GPIO_FioDir(SDA1, SALIDA);
	GPIO_PinMode(SDA1, PINMODE_NONE);
	GPIO_PinModeOD(SDA1, PINMODE_OD);

	GPIO_Pinsel(SCL1, PINSEL_FUNC3);
	GPIO_FioDir(SCL1, SALIDA);
	GPIO_PinMode(SCL1, PINMODE_NONE);
	GPIO_PinModeOD(SCL1, PINMODE_OD);
}

void I2C2_Inicializar(void) {
	GPIO_Pinsel(SDA2, PINSEL_FUNC2);
	GPIO_FioDir(SDA2, SALIDA);
	GPIO_PinMode(SDA2, PINMODE_NONE);
	GPIO_PinModeOD(SDA2, PINMODE_OD);

	GPIO_Pinsel(SCL2, PINSEL_FUNC2);
	GPIO_FioDir(SCL2, SALIDA);
	GPIO_PinMode(SCL2, PINMODE_NONE);
	GPIO_PinModeOD(SCL2, PINMODE_OD);
}
