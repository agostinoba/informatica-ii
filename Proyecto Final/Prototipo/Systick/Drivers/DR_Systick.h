/*******************************************************************************
 *
 * @file       DR_Systick.c
 * @brief      Módulo con la inicialización del Systick.
 * @version    1.00
 * @date       28/04/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef DRIVERS_DR_SYSTICK_H_
#define DRIVERS_DR_SYSTICK_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <Control_Garage/Primitivas/PR_Control_Garage.h>
#include <Drivers/DR_Teclado.h>
#include <Drivers/DR_Timer.h>
#include <Drivers/DR_LCD.h>
#include <Drivers/DR_ADC.h>
#include <Sensor_Humo/Primitivas/PR_Sensor_Humo.h>
#include <Sensor_Luz/Primitivas/PR_Sensor_Luz.h>
#include <Sensor_Porton/Primitivas/PR_Sensor_Porton.h>
#include "Types.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/
#define	SYSTICK			( (systick_t *) 0xE000E010UL )

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/
#define		STCTRL		SYSTICK->_STCTRL
#define		ENABLE		SYSTICK->bits._ENABLE
#define		TICKINT		SYSTICK->bits._TICKINT
#define		CLKSOURCE	SYSTICK->bits._CLKSOURCE
#define		COUNTFLAG	SYSTICK->bits._COUNTFLAG
#define		STRELOAD	SYSTICK->_STRELOAD
#define		STCURR		SYSTICK->_STCURR
#define		STCALIB		SYSTICK->_STCALIB

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/
typedef struct {
	union {
		volatile uint32_t _STCTRL;
		struct {
			volatile uint32_t _ENABLE:1;
			volatile uint32_t _TICKINT:1;
			volatile uint32_t _CLKSOURCE:1;
			volatile uint32_t _RESERVED0:13;
			volatile uint32_t _COUNTFLAG:1;
			volatile uint32_t _RESERVED1:15;
		} bits;
	};
	volatile uint32_t _STRELOAD;
	volatile uint32_t _STCURR;
	volatile const uint32_t  _STCALIB;
} systick_t;

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			uint32_t Systick_Init(void)
 * @brief		Función de inicialización del Systick
 * @param[in]	void
 * @return		retorna 0
 ******************************************************************************/
uint32_t Systick_Init(void);

/*******************************************************************************
 * @fn			void SysTick_Handler(void)
 * @brief		Función de interrupción del systick.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void SysTick_Handler(void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* DRIVERS_DR_SYSTICK_H_ */
