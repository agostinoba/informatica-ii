/*******************************************************************************
 *
 * @file       DR_Systick.c
 * @brief      Módulo con la inicialización del Systick.
 * @version    1.00
 * @date       28/04/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <Drivers/DR_Systick.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/

extern int Demora_LCD;

// Tiempo de interrumpción. Lo configuro en frecuencia para facilitar la cuenta.
// El systick es un contador descendente
uint32_t Systick_Init(void) {

	STRELOAD  	=	(STCALIB / 4) - 1; // 2,5 ms

    // Selecciona la fuente de clock (en este caso clk principal)
	CLKSOURCE	=	1;

    // Resetea la cuenta actual (Pone a cero el contador)
	STCURR		=	0;

    // Habilita la interrupción
	TICKINT	=	1;

    // Habilita el inicio de conteo
	ENABLE		=	1;

	return 0;
}

void SysTick_Handler(void) {
	Timer_Descontar();
    Barrido_Teclado();

    // Checkeo el estado de los sensores (activados/no activados)
    Checkeo_Control_Garage();
    Checkeo_Sensor_Luz();
    Checkeo_Sensores_Porton();

    ADC_Start();

	if (Demora_LCD)
		Demora_LCD--;

	Dato_LCD();
}
