/********************************************************************************************************
*
* @file		DR_LCD.c
* @brief	Módulo con la funcion primitiva de inicialización de LCD.
* @version	1.00
* @date
* @author	Agostino Barbetti
*
********************************************************************************************************/

/********************************************************************************************************
*** INCLUDES
********************************************************************************************************/
#include <Drivers/DR_LCD.h>

/********************************************************************************************************
*** DEFINES PRIVADOS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** MACROS PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** TIPOS DE DATOS PRIVADOS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** TABLAS PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** VARIABLES GLOBALES PUBLICAS
********************************************************************************************************/

/********************************************************************************************************
*** VARIABLES GLOBALES PRIVADAS AL MODULO
********************************************************************************************************/
volatile uint8_t Buffer_LCD[TOPE_BUFFER_LCD];
volatile uint32_t ptrInLCD;
volatile uint32_t ptrOutLCD;
volatile uint32_t cantidadColaLCD;
volatile int32_t Demora_LCD;
volatile uint32_t inxInLCD;
volatile uint32_t inxOutLCD;

/********************************************************************************************************
*** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** FUNCIONES PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** FUNCIONES GLOBALES AL MODULO
********************************************************************************************************/

// CLASE 27 DE INFO II

void Config_LCD(void) {
	unsigned int i;

	GPIO_Set(LCD_E, BAJO);
	Demora_LCD = 10;			// Demora inicial
	while (Demora_LCD);

	for(i = 0; i < 3; i++) {
		GPIO_Set(LCD_D4, ALTO);  // Configuración en 8 bits
		GPIO_Set(LCD_D5, ALTO);
		GPIO_Set(LCD_D6, BAJO);
		GPIO_Set(LCD_D7, BAJO);
		GPIO_Set(LCD_RS, BAJO);
		GPIO_Set(LCD_E, ALTO);
		GPIO_Set(LCD_E, BAJO);
		Demora_LCD = 2;
		while (Demora_LCD);
	}

	// Configuracion en 4 bits
	GPIO_Set(LCD_D4, BAJO);
	GPIO_Set(LCD_D5, ALTO);
	GPIO_Set(LCD_D6, BAJO);
	GPIO_Set(LCD_D7, BAJO);

	GPIO_Set(LCD_RS, BAJO);
	GPIO_Set(LCD_E, ALTO);
	GPIO_Set(LCD_E, BAJO);

	Demora_LCD = 1;	// Demora inicial
	while(Demora_LCD);

	// A partir de aca el LCD pasa a 4 bits !!!
	PushLCD(0x28, LCD_CONTROL);	// DL = 0: 4 bits de datos
									// N = 1 : 2 lineas
									// F = 0 : 5x7 puntos

	PushLCD(0x08, LCD_CONTROL);	// D = 0 : display OFF
									// C = 0 : Cursor OFF
									// B = 0 : Blink OFF

	PushLCD(0x01, LCD_CONTROL);	// clear display

	PushLCD(0x06, LCD_CONTROL);	// I/D = 1 : Incrementa puntero
									// S = 0 : NO Shift Display

	// Activo el LCD y listo para usar !
	PushLCD(0x0C, LCD_CONTROL);	// D = 1 : display ON
									// C = 0 : Cursor OFF
									// B = 0 : Blink OFF
}

uint8_t PushLCD(uint8_t dato, uint8_t control) {
	if (cantidadColaLCD >= TOPE_BUFFER_LCD)
		return -1;

	Buffer_LCD[inxInLCD] = (dato >> 4) & 0x0f;

	if ( control == LCD_CONTROL )
		Buffer_LCD[inxInLCD] |= 0x80;

	inxInLCD ++;
	inxInLCD %= TOPE_BUFFER_LCD;
	Buffer_LCD[inxInLCD] = dato & 0x0f;

	if (control == LCD_CONTROL)
		Buffer_LCD[inxInLCD] |= 0x80;

	cantidadColaLCD += 2;
	inxInLCD++;
	inxInLCD %= TOPE_BUFFER_LCD;

	return 0;
}

int PopLCD (void) {
	char dato;

	if ( cantidadColaLCD == 0 )
		return -1;

	dato = Buffer_LCD[inxOutLCD];

	cantidadColaLCD --;

	inxOutLCD ++;
	inxOutLCD %= TOPE_BUFFER_LCD;

	return dato;
}

void Dato_LCD(void) {
	int data;

	if ( (data = PopLCD ()) == -1 )
		return;

	GPIO_Set(LCD_D7, (((unsigned char ) data ) >> 3 & 0x01));
	GPIO_Set(LCD_D6, (((unsigned char ) data ) >> 2 & 0x01));
	GPIO_Set(LCD_D5, (((unsigned char ) data ) >> 1 & 0x01));
	GPIO_Set(LCD_D4, (((unsigned char ) data ) >> 0 & 0x01));

	if( ((unsigned char ) data ) & 0x80 )
		GPIO_Set(LCD_RS, BAJO);	//Comando
	else
		GPIO_Set(LCD_RS, ALTO);	//Dato

	GPIO_Set(LCD_E, ALTO);
	GPIO_Set(LCD_E, BAJO);
}

void Borrar_LCD(void) {
	Display_lcd("                                                  ", RENGLON_1, 0x00);
	Display_lcd("                                                  ", RENGLON_2, 0x00);
}


void Display_lcd(char * msg, char r , char p) {
	unsigned char i ;

	switch (r) {
		case RENGLON_1:
			PushLCD(p + 0x80, LCD_CONTROL);
			break;
		case RENGLON_2:
			PushLCD(p + 0xc0, LCD_CONTROL);
			break;
	}
	for(i = 0; msg[i] != '\0'; i++)
		PushLCD(msg [i], LCD_DATA);
}
