/*******************************************************************************
 *
 * @file       Sensor_Movimiento.h
 * @brief      Módulo con la funcion primitiva del sensor de movimiento.
 * @version    1.00
 * @date       22/11/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef SENSOR_MOVIMIENTO_H_
#define SENSOR_MOVIMIENTO_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <Maquina_Simulacion.h>
#include "Types.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/
#define NO_MOVIMIENTO		0
#define MOVIMIENTO			1

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			uint32_t Estado_Sensor_Movimiento(void)
 * @brief		Función para controlar el estado del control del garage.
 * @param[in]	void
 * @return		Botón presionado
 ******************************************************************************/
uint32_t Estado_Sensor_Movimiento(void);

/*******************************************************************************
 * @fn			void Checkeo_Control_Garage(void)
 * @brief		Función para actualizar el estado del control del garage.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Checkeo_Sensor_Movimiento(void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* SENSOR_MOVIMIENTO_H_ */
