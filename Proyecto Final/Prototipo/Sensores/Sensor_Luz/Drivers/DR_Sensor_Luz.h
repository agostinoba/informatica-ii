/*******************************************************************************
 *
 * @file       DR_Sensor_Luz.h
 * @brief      Módulo con la funcion de hardware del sensor de iluminación.
 * @version    1.00
 * @date       22/11/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef SENSOR_LUZ_DRIVERS_DR_SENSOR_LUZ_H_
#define SENSOR_LUZ_DRIVERS_DR_SENSOR_LUZ_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <Maquina_Simulacion.h>
#include "Types.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/
extern volatile uint32_t estado_luz;

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void Checkeo_Sensor_Luz(void)
 * @brief		Función para actualizar el estado del sensor de luz.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Checkeo_Sensor_Luz(void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* SENSOR_LUZ_DRIVERS_DR_SENSOR_LUZ_H_ */
