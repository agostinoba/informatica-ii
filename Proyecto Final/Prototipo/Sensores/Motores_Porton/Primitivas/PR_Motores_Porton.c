/********************************************************************************************************
*
* @file		PR_Motores_Porton.c
* @brief	Módulo con la funcion primitiva para abrir o cerrar el porton.
* @version	1.00
* @date
* @author	Agostino Barbetti
*
********************************************************************************************************/

/********************************************************************************************************
*** INCLUDES
********************************************************************************************************/
#include <Motores_Porton/Primitivas/PR_Motores_Porton.h>

/********************************************************************************************************
*** DEFINES PRIVADOS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** MACROS PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** TIPOS DE DATOS PRIVADOS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** TABLAS PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** VARIABLES GLOBALES PUBLICAS
********************************************************************************************************/

/********************************************************************************************************
*** VARIABLES GLOBALES PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** FUNCIONES PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** FUNCIONES GLOBALES AL MODULO
********************************************************************************************************/
void Porton(uint8_t accion) {
	switch (accion) {
		case ABRIR:
			GPIO_Set(RELE1, ALTO);
			GPIO_Set(RELE2, BAJO);
			break;

		case CERRAR:
			GPIO_Set(RELE1, BAJO);
			GPIO_Set(RELE2, ALTO);
			break;

		case PARAR:
			GPIO_Set(RELE1, BAJO);
			GPIO_Set(RELE2, BAJO);
			break;

		default:
			GPIO_Set(RELE1, BAJO);
			GPIO_Set(RELE2, BAJO);
			break;
	}
}
