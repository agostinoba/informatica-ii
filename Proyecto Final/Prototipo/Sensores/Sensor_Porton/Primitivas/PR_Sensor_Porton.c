/*******************************************************************************
 *
 * @file       PR_Sensor_Porton.c
 * @brief      Módulo con la funcion primitiva de los sensores del porton.
 * @version    1.00
 * @date       22/11/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <Sensor_Porton/Primitivas/PR_Sensor_Porton.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
uint32_t Estado_Sensor_Porton_Abierto(void) {
	uint32_t retorno = OFF;

	if(ON == estado_sensor_abierto) {
		estado_sensor_abierto = OFF;
		retorno = ON;
	}
	return retorno;
}

uint32_t Estado_Sensor_Porton_Cerrado(void) {
	uint32_t retorno = OFF;

	if(ON == estado_sensor_cerrado) {
		estado_sensor_cerrado = OFF;
		retorno = ON;
	}
	return retorno;
}
