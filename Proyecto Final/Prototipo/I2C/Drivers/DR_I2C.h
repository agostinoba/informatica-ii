/*******************************************************************************
 *
 * @file       DR_I2C.h
 * @brief      Funciones para el uso del módulo de I2C
 * @version    1.00
 * @date       26/01/2021
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef DRIVERS_DR_I2C_H_
#define DRIVERS_DR_I2C_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "Types.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

//0x400FC0C4UL : Registro de habilitación de dispositivos:
#define 	DIR_PCONP			( ( uint32_t  * ) 0x400FC0C4UL )
//0x400FC1A8UL : Registros de seleccion de los clks de los dispositivos:
#define		PCLKSEL				( ( uint32_t  * ) 0x400FC1A8UL )
//0xE000E100UL : Regigstro de habilitación (set) de interrup. en el NVIC:
#define		ISER				( ( uint32_t  * ) 0xE000E100UL )

#define		DIR_I2CPADCFG		( (i2cpadcfg_t*	) 0x4002C07CUL )

// REVISAR - PAG 448
#define		DIR_I2CONSET		( ( uint32_t  * ) 0x4001C000UL ) // PAG - 449

#define		DIR_I2STAT			( ( uint32_t  * ) 0x4001C004UL ) // PAG - 452

#define		DIR_I2DAT			( ( uint32_t  * ) 0x4001C008UL ) // PAG - 452

#define		DIR_I2ADR0			( ( uint32_t  * ) 0x4001C00CUL ) // PAG - 455

#define		DIR_I2SCLH			( ( uint32_t  * ) 0x4001C010UL ) // PAG - 456

#define		DIR_I2SCLL			( ( uint32_t  * ) 0x4001C014UL ) // PAG - 456

#define		DIR_I2CONCLR		( ( uint32_t  * ) 0x4001C018UL ) // PAG - 451

#define		DIR_MMCTRL			( ( uint32_t  * ) 0x4001C01CUL ) // PAG - 453

#define		DIR_I2ADR1			( ( uint32_t  * ) 0x4001C020UL ) // PAG - 455

#define		DIR_I2ADR2			( ( uint32_t  * ) 0x4001C024UL ) // PAG - 455

#define		DIR_I2ADR3			( ( uint32_t  * ) 0x4001C028UL ) // PAG - 455

#define		DIR_I2DATA_BUFFER	( ( uint32_t  * ) 0x4001C02CUL ) // PAG - 455

#define		DIR_I2MASK0			( ( uint32_t  * ) 0x4001C030UL ) // PAG - 456

#define		DIR_I2MASK1			( ( uint32_t  * ) 0x4001C034UL ) // PAG - 456

#define		DIR_I2MASK2			( ( uint32_t  * ) 0x4001C038UL ) // PAG - 456

#define		DIR_I2MASK3			( ( uint32_t  * ) 0x4001C03CUL ) // PAG - 456

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

// Registro PCONP:
#define		PCONP				DIR_PCONP[0]

// Registros PCLKSEL
#define		PCLKSEL0			PCLKSEL[0]
#define		PCLKSEL1			PCLKSEL[1]

#define		ISER0				ISER[0]

#define		I2CPADCFG			DIR_I2CPADCFG[0]

#define		I2C0CONSET			DIR_I2CONSET[0]
#define		I2C1CONSET			DIR_I2CONSET[1]
#define		I2C2CONSET			DIR_I2CONSET[2]

#define		I2C0STAT			DIR_I2STAT[0]
#define		I2C1STAT			DIR_I2STAT[1]
#define		I2C2STAT			DIR_I2STAT[2]

#define		I2C0DAT				DIR_I2DAT[0]
#define		I2C1DAT				DIR_I2DAT[1]
#define		I2C2DAT				DIR_I2DAT[2]

#define		I2C0ADR0			DIR_I2ADR0[0]
#define		I2C1ADR0			DIR_I2ADR0[1]
#define		I2C2ADR0			DIR_I2ADR0[2]

#define		I2C0SCLH			DIR_I2SCLH[0]
#define		I2C1SCLH			DIR_I2SCLH[1]
#define		I2C2SCLH			DIR_I2SCLH[2]

#define		I2C0SCLL			DIR_I2SCLL[0]
#define		I2C1SCLL			DIR_I2SCLL[1]
#define		I2C2SCLL			DIR_I2SCLL[2]

#define		I2C0CONCLR			DIR_I2CONCLR[0]
#define		I2C1CONCLR			DIR_I2CONCLR[1]
#define		I2C2CONCLR			DIR_I2CONCLR[2]

#define		I2C0MMCTRL			DIR_MMCTRL[0]
#define		I2C1MMCTRL			DIR_MMCTRL[1]
#define		I2C2MMCTRL			DIR_MMCTRL[2]

#define		I2C0ADR1			DIR_I2ADR1[0]
#define		I2C1ADR1			DIR_I2ADR1[1]
#define		I2C2ADR1			DIR_I2ADR1[2]

#define		I2C0ADR2			DIR_I2ADR2[0]
#define		I2C1ADR2			DIR_I2ADR2[1]
#define		I2C2ADR2			DIR_I2ADR2[2]

#define		I2C0ADR3			DIR_I2ADR3[0]
#define		I2C1ADR3			DIR_I2ADR3[1]
#define		I2C2ADR3			DIR_I2ADR3[2]

#define		I2C0DATA_BUFFER		DIR_I2DATA_BUFFER[0]
#define		I2C1DATA_BUFFER		DIR_I2DATA_BUFFER[1]
#define		I2C2DATA_BUFFER		DIR_I2DATA_BUFFER[2]

#define		I2C0MASK0			DIR_I2MASK0[0]
#define		I2C1MASK0			DIR_I2MASK0[1]
#define		I2C2MASK0			DIR_I2MASK0[2]

#define		I2C0MASK1			DIR_I2MASK1[0]
#define		I2C1MASK1			DIR_I2MASK1[1]
#define		I2C2MASK1			DIR_I2MASK1[2]

#define		I2C0MASK2			DIR_I2MASK2[0]
#define		I2C1MASK2			DIR_I2MASK2[1]
#define		I2C2MASK2			DIR_I2MASK2[2]

#define		I2C0MASK3			DIR_I2MASK3[0]
#define		I2C1MASK3			DIR_I2MASK3[1]
#define		I2C2MASK3			DIR_I2MASK3[2]



#define		PORT0				0

// I2C0
#define		SDA0				PORT0,27
#define		SCL0				PORT0,28

// I2C1 - Hay dos registros para SDA1 y SCL1
#define		SDA1				PORT0,0
#define		SCL1				PORT0,1
//#define		SDA1				PORT0,19
//#define		SCL1				PORT0,20

// I2C2
#define		SDA2				PORT0,10
#define		SCL2				PORT0,11

// Pag. 450
#define		I2CONSET_AA			(0x01 << 2)	// Assert acknowledge flag
#define		I2CONSET_SI			(0x01 << 3) // I2C interrupt flag
#define		I2CONSET_STO		(0x01 << 4) // Stop flag
#define		I2CONSET_STA		(0x01 << 5) // Start fflag
#define		I2CONSET_I2EN		(0x01 << 6)	// I2C interface enable

// Pag. 451
#define		I2CONCLR_AAC		(0x01 << 2)	// Assert acknowledge clear bit
#define		I2CONCLR_SIC		(0x01 << 3) // I2C interrupt clear bit
#define		I2CONCLR_STAC		(0x01 << 5) // Start flag clear bit
#define		I2CONCLR_I2ENC		(0x01 << 6)	// I2C interface disable bit

#define		I2SCLH_SCLH			0x80
#define		I2SCLL_SCLL			0x80
#define		I2SCLH_HS_SCLH		0x08
#define		I2SCLL_HS_SCLL		0x08

// Pag. 78
#define		ISE_I2C0			(0x01 << 10) // I2C0 interrupt enable
#define		ISE_I2C1			(0x01 << 11) // I2C1 interrupt enable
#define		ISE_I2C2			(0x01 << 12) // I2C2 interrupt enable

#define		BUFFSIZE			64
#define		MAX_TIMEOUT			0x00FFFFFF



#define		I2C_IDLE              0
#define		I2C_STARTED           1
#define		I2C_RESTARTED         2
#define		I2C_REPEATED_START    3
#define		DATA_ACK              4
#define		DATA_NACK             5
#define		I2C_BUSY              6
#define		I2C_NO_DATA           7
#define		I2C_ACK_ON_ADDRESS    9
#define		I2C_NACK_ON_ADDRESS   10
#define		I2C_ACK_ON_DATA       11
#define		I2C_NACK_ON_DATA      12
#define		I2C_ARBITRATION_LOST  13
#define		I2C_TIME_OUT          14
#define		I2C_OK                15
#define		I2C_BUS_ERROR         16
#define		I2C_ACK_ON_W          17
#define		I2C_ACK_ON_R          18
#define		I2C_NACK_ON_W         19
#define		I2C_NACK_ON_R         20


/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/
typedef struct {
	uint32_t SDADRV0:1;
	uint32_t SDAI2C0:1;
	uint32_t SCLDRV0:1;
	uint32_t SCLI2C0:1;
	uint32_t RESERVED:28;
}i2cpadcfg_t;

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/
extern volatile uint32_t I2CReadLength;
extern volatile uint32_t I2CWriteLength;
extern volatile uint8_t I2CMasterBuffer[BUFFSIZE];
extern volatile uint8_t I2CSlaveBuffer[BUFFSIZE];

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void I2C0_Init(void)
 * @brief		Función de inicialización del I2C0.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void I2C0_Init(void);

/*******************************************************************************
 * @fn			void I2C1_Init(void)
 * @brief		Función de inicialización del I2C1.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void I2C1_Init(void);

/*******************************************************************************
 * @fn			void I2C2_Init(void)
 * @brief		Función de inicialización del I2C2.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void I2C2_Init(void);

/*******************************************************************************
 * @fn			void I2C0_IRQHandler(void)
 * @brief		Función de interrupción del I2C0.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void I2C0_IRQHandler(void);

/*******************************************************************************
 * @fn			void I2C1_IRQHandler(void)
 * @brief		Función de interrupción del I2C1.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void I2C1_IRQHandler(void);

/*******************************************************************************
 * @fn			void I2C2_IRQHandler(void)
 * @brief		Función de interrupción del I2C2.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void I2C2_IRQHandler(void);

/*******************************************************************************
 * @fn			uint32_t I2C_Start(void)
 * @brief		Función para iniciar la comunicación del I2C.
 * @param[in]	void
 * @return      Retorna TRUE o FALSE según se cumpla o no el tiempo de espera.
 ******************************************************************************/
uint32_t I2C_Start(void);

/*******************************************************************************
 * @fn			uint32_t I2C_Engine(void)
 * @brief		Función para el envío y recepción de datos.
 * @param[in]	void
 * @return		Retorna I2C_TIME_OUT cuando se cumple el tiempo de espera.
 ******************************************************************************/
uint32_t I2C_Engine(void);

/*******************************************************************************
 * @fn			uint32_t I2C_Stop(void)
 * @brief		Función que detiene la comunicación mediante I2C.
 * @param[in]	void
 * @return		Retorna TRUE.
 ******************************************************************************/
uint32_t I2C_Stop(void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* DRIVERS_DR_I2C_H_ */
