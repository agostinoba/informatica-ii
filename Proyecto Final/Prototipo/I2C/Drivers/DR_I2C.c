/*******************************************************************************
 *
 * @file       DR_I2C.c
 * @brief      Funciones para el uso del módulo de I2C
 * @version    1.00
 * @date       26/01/2021
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <Drivers/DR_I2C.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
uint32_t timeout	= 0;
uint32_t I2CCount	= 0;
uint32_t RdIndex	= 0;
uint32_t WrIndex	= 0;
uint32_t I2CMasterState = I2C_IDLE;
volatile uint32_t I2CReadLength;
volatile uint32_t I2CWriteLength;
volatile uint8_t I2CMasterBuffer[BUFFSIZE];
volatile uint8_t I2CSlaveBuffer[BUFFSIZE];

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void I2C0_Init(void) {
    // Registro de Power Control (Sirve para "encender" el módulo de I2C) Pag 64
    PCONP |= (0x01 << 07); // I2C0
//  PCONP |= (0x01 << 19); // I2C1
//  PCONP |= (0x01 << 26); // I2C2

    // Despues de PCONP configuro los Pines (Pag. 57)
	// asigno velocidad al I2C0 (es un divisor de la velocidad del micro)
    PCLKSEL0 &= ~(0x03 << 14);
    // CHECKEAR LA VELOCIDAD 00 (CCLK/4) 01 (CCLK) 10 (CCLK/2) 11 (CCLK/8)
	//  PCLKSEL1 &= ~(0x03 << 06); // I2C1
	//  PCLKSEL1 &= ~(0x03 << 20); // I2C2

    // Limpio el registro de flags
	I2C0CONCLR = I2CONCLR_AAC | I2CONCLR_SIC | I2CONCLR_STAC | I2CONCLR_I2ENC;

    // Configuración I2C0 - PAG 128
    I2CPADCFG.SDAI2C0 = 0; // Filtro de fallos y contro de velocidad de
						   // respuesta. Habilitar = 0, Deshabilitar = 1
    I2CPADCFG.SDADRV0 = 0; // Standard = 0, Fast Mode Plus = 1
	I2CPADCFG.SCLI2C0 = 0; // Filtro de fallos y contro de velocidad de
						   // respuesta. Habilitar = 0, Deshabilitar = 1
	I2CPADCFG.SCLDRV0 = 0; // Standard = 0, Fast Mode Plus = 1

	I2C0SCLH = I2SCLH_SCLH;
	I2C0SCLL = I2SCLL_SCLL;

	/* Configuración Fast Mode Plus del I2C0
    I2CPADCFG.SDAI2C0 = 0; // Filtro de fallos y contro de velocidad de
						   // respuesta. Habilitar = 0, Deshabilitar = 1
    I2CPADCFG.SDADRV0 = 1; // Standard = 0, Fast Mode Plus = 1
	I2CPADCFG.SCLI2C0 = 0; // Filtro de fallos y contro de velocidad de
	                       // respuesta. Habilitar = 0, Deshabilitar = 1
	I2CPADCFG.SCLDRV0 = 1; // Standard = 0, Fast Mode Plus = 1

	I2C0SCLH = I2SCLH_HS_SCLH;
	I2C0SCLL = I2SCLL_HS_SCLL;
	*/

	// Habilito las interrupciones del i2c
	ISER0 |= ISE_I2C0;

	I2C0CONSET = I2CONSET_I2EN;
}

void I2C1_Init(void) {
    // Registro de Power Control (Sirve para "encender" el módulo de I2C) Pag 64
	PCONP |= (0x01 << 19); // I2C1

    // Despues de PCONP configuro los Pines (Pag. 57)
	// asigno velocidad al I2C1 (es un divisor de la velocidad del micro)
    PCLKSEL1 &= ~(0x03 << 06);
    // CHECKEAR LA VELOCIDAD 00 (CCLK/4) 01 (CCLK) 10 (CCLK/2) 11 (CCLK/8)

    // Limpio el registro de flags
	I2C1CONCLR = I2CONCLR_AAC | I2CONCLR_SIC | I2CONCLR_STAC | I2CONCLR_I2ENC;

	I2C1SCLH = I2SCLH_SCLH;
	I2C1SCLL = I2SCLL_SCLL;

	// Habilito las interrupciones del i2c
	ISER0 |= ISE_I2C1;

	I2C1CONSET = I2CONSET_I2EN;
}

void I2C2_Init(void) {
    // Registro de Power Control (Sirve para "encender" el módulo de I2C) Pag 64
	PCONP |= (0x01 << 26); // I2C2

    // Despues de PCONP configuro los Pines (Pag. 57)
	// asigno velocidad al I2C0 (es un divisor de la velocidad del micro)
    PCLKSEL1 &= ~(0x03 << 20);
    // CHECKEAR LA VELOCIDAD 00 (CCLK/4) 01 (CCLK) 10 (CCLK/2) 11 (CCLK/8)

    // Limpio el registro de flags
	I2C2CONCLR = I2CONCLR_AAC | I2CONCLR_SIC | I2CONCLR_STAC | I2CONCLR_I2ENC;

	I2C2SCLH = I2SCLH_SCLH;
	I2C2SCLL = I2SCLL_SCLL;

	// Habilito las interrupciones del i2c
	ISER0 |= ISE_I2C2;

	I2C2CONSET = I2CONSET_I2EN;
}

void I2C0_IRQHandler(void) {
	uint8_t StatValue;

	timeout = 0;

	StatValue = I2C0STAT;

	switch(StatValue) {

	}
}

void I2C1_IRQHandler(void) {
	uint8_t StatValue;

	timeout = 0;

	StatValue = I2C1STAT;

	switch(StatValue) {

	case 0x00:
		I2C1CONSET = I2CONSET_AA | I2CONSET_STO;
		I2C1CONCLR = I2CONCLR_SIC;
		I2CMasterState = I2C_BUS_ERROR;
		break;

	case 0x08:
		WrIndex = 0;
		RdIndex = 0;
		I2C1DAT = I2CMasterBuffer[WrIndex];
		I2C1CONSET = I2CONSET_AA;
		I2C1CONCLR = I2CONCLR_SIC;
		WrIndex++;
		I2CMasterState = I2C_STARTED;
		break;

	case 0x10:
		I2C1DAT = I2CMasterBuffer[WrIndex];
		I2C1CONSET = I2CONSET_AA;
		I2C1CONCLR = I2CONCLR_SIC;
		I2CMasterState = I2C_REPEATED_START;
		break;

	case 0x18:
		if(3 == I2CWriteLength) {
			I2C1CONSET = I2CONSET_STO;
			I2CMasterState = I2C_NO_DATA;
		}
		I2C1DAT = I2CMasterBuffer[WrIndex];
		I2C1CONSET = I2CONSET_AA;
		I2C1CONCLR = I2CONCLR_SIC;
		WrIndex++;
		I2CMasterState = I2C_ACK_ON_W;
		break;

	case 0x20:
		I2C1CONSET = I2CONSET_STO | I2CONSET_AA;
		I2C1CONCLR = I2CONCLR_SIC;
		I2CMasterState = I2C_NACK_ON_W;
		break;

	case 0x28:
		if((1 <= I2CReadLength) && (3 == WrIndex)) {
			I2C1CONSET = I2CONSET_STA | I2CONSET_AA;
			I2C1CONCLR = I2CONCLR_SIC;
			I2CMasterState = I2C_RESTARTED;
		}
		else if (WrIndex < I2CWriteLength) {
			I2C1DAT = I2CMasterBuffer[WrIndex];
			I2C1CONSET = I2CONSET_AA;
			I2C1CONCLR = I2CONCLR_SIC;
			WrIndex++;
			I2CMasterState = I2C_ACK_ON_DATA;
		}
		else {
			I2C1CONSET = I2CONSET_STO | I2CONSET_AA;
			I2C1CONCLR = I2CONCLR_SIC;
			I2CMasterState = I2C_OK;
		}
		break;

	case 0x30:
		I2C1CONSET = I2CONSET_STO | I2CONSET_AA;
		I2C1CONCLR = I2CONCLR_SIC;
		I2CMasterState = I2C_NACK_ON_DATA;
		break;

	case 0x38:
		I2C1CONSET = I2CONSET_STA | I2CONSET_AA;
		I2C1CONCLR = I2CONCLR_SIC;
		I2CMasterState = I2C_ARBITRATION_LOST;
		break;

	case 0x40:
		I2C1CONSET = I2CONSET_AA;
		I2C1CONCLR = I2CONCLR_SIC;
		I2C1CONCLR = I2CONCLR_STAC;
		I2CMasterState = I2C_ACK_ON_ADDRESS;
		break;

	case 0x48:
		I2C1CONSET = I2CONSET_STO | I2CONSET_AA;
		I2C1CONCLR = I2CONCLR_SIC;
		I2CMasterState = I2C_NACK_ON_ADDRESS;
		break;

	case 0x50:
		I2CSlaveBuffer[RdIndex] = I2C1DAT;
		if((1 + RdIndex) < I2CReadLength) {
			I2C1CONSET = I2CONSET_AA;
			I2C1CONCLR = I2CONCLR_SIC;
			RdIndex++;
			I2CMasterState = I2C_ACK_ON_DATA;
		}
		else {
			I2C1CONCLR = I2CONCLR_SIC | I2CONCLR_AAC;
			I2CMasterState = I2C_NACK_ON_DATA;
		}
		break;

	case 0x58:
		I2CSlaveBuffer[RdIndex] = I2C1DAT;
		I2C1CONSET = I2CONSET_STO | I2CONSET_AA;
		I2C1CONCLR = I2CONCLR_SIC;
		I2CMasterState = I2C_OK;
		break;

	default:
		I2C1CONCLR = I2CONCLR_SIC;
		I2CMasterState = I2C_ARBITRATION_LOST;
		break;
	}
}

void I2C2_IRQHandler(void) {

}

uint32_t I2C_Start(void) {
	uint32_t retValue = FALSE;

	timeout = 0;

	I2C1CONSET = I2CONSET_STA;

	while(TRUE) {
		if(I2C_STARTED == I2CMasterState) {
			retValue = TRUE;
			break;
		}
		if(MAX_TIMEOUT <= timeout) {
			retValue = FALSE;
			break;
		}
		timeout++;
	}
	return retValue;
}

uint32_t I2C_Engine(void) {
	I2C1CONSET = I2CONSET_STA;

	I2CMasterState = I2C_BUSY;

	while(I2C_BUSY == I2CMasterState) {
		if(MAX_TIMEOUT <= timeout) {
			I2CMasterState = I2C_TIME_OUT;
			break;
		}
		timeout++;
	}

	I2C1CONCLR = I2CONCLR_STAC;

	return I2CMasterState;
}

uint32_t I2C_Stop(void) {
	I2C1CONSET = I2CONSET_STO;
	I2C1CONCLR = I2CONCLR_SIC;

	while(I2C1CONSET & I2CONSET_STO);
	return TRUE;
}
