/*******************************************************************************
 *
 * @file       Trama_Recibir_Qt.c
 * @brief      Módulo que procesa la trama con la configuración.
 * @version    1.00
 * @date       15/09/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "Trama_Recibir_Qt.h"

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define TRAMA_INICIO				0
#define TRAMA_HORA_RTC				1
#define TRAMA_TEMP_AMB				2
#define TRAMA_TEMP_AMB_HORA_INIT	3
#define TRAMA_TEMP_AMB_HORA_FIN	4
#define TRAMA_ILUM_HORA_INIT		5
#define TRAMA_ILUM_AMB_HORA_FIN	6
#define TRAMA_SETVALORES			7
#define TRAMA_PASSWORD				8

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
uint32_t horaRTC[3];
uint8_t Hora[8];
volatile uint8_t Temp_Amb[2];
volatile uint8_t Temp_Amb_Hora_Inicio[5];
volatile uint8_t Temp_Amb_Hora_Fin[5];
volatile uint8_t Ilum_Hora_Inicio[5];
volatile uint8_t Ilum_Hora_Fin[5];
uint8_t password[3];

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
uint8_t Descifrar_Trama(uint8_t Dato) {
    static int Estado = 0;
    int Pos_Hora = 0;
    static int Pos = 0;
    static char First_Time = 0;
    uint8_t Trama_ok = 0;

    switch(Estado) {
        case TRAMA_INICIO:
            if(Dato == '<') {
                Estado++;
                Pos = 0;
            }
            break;

        case TRAMA_HORA_RTC:
            if(Dato == '|') {
                Estado++;
                if(!First_Time) {
                	Hora[Pos] = '\0';
                	int iteracionFor = 0;
                	for(int i = 0; i < 8; i++) {
                		if(i != 2 || i != 5) {
                			if(i == 0 || i == 3 || i == 6) {
                				horaRTC[Pos_Hora++] = StringToInt(Hora[i],
                                                                  Hora[i+1]);
                				iteracionFor ++;
                			}
                		}
                	}
                	RTC_SetTime(horaRTC);
                	Enviar_Password();
                }
                Pos = 0;
                First_Time = 1;
            }
            else if(Dato == 'P') {
                Estado = TRAMA_PASSWORD;
                Pos = 0;
            }
            else {
                Hora[Pos] = Dato;
                Pos++;
            }
            break;

        case TRAMA_TEMP_AMB:
            if(Dato == '|') {
            	Estado++;
            	Temp_Amb[Pos] = '\0';
            	Pos = 0;
            }
            else {
            	Temp_Amb[Pos] = Dato;
            	Pos++;
            }
            break;

        case TRAMA_TEMP_AMB_HORA_INIT:
            if(Dato == '|') {
            	Estado++;
            	Temp_Amb_Hora_Inicio[Pos] = '\0';
            	Pos = 0;
            }
            else {
            	Temp_Amb_Hora_Inicio[Pos] = Dato;
            	Pos++;
            }
            break;

        case TRAMA_TEMP_AMB_HORA_FIN:
            if(Dato == '|') {
            	Estado++;
            	Temp_Amb_Hora_Fin[Pos] = '\0';
            	Pos = 0;
            }
            else {
            	Temp_Amb_Hora_Fin[Pos] = Dato;
            	Pos++;
            }
            break;

        case TRAMA_ILUM_HORA_INIT:
            if(Dato == '|') {
            	Estado++;
            	Ilum_Hora_Inicio[Pos] = '\0';
            	Pos = 0;
            }
            else {
            	Ilum_Hora_Inicio[Pos] = Dato;
            	Pos++;
            }
            break;

        case TRAMA_ILUM_AMB_HORA_FIN:
            if(Dato == '>') {
            	Estado = 0;
            	Ilum_Hora_Fin[Pos] = '\0';
            	Trama_ok = 1;
            	setValores();
            }
            else {
            	Ilum_Hora_Fin[Pos] = Dato;
            	Pos++;
            }
            break;

        case TRAMA_SETVALORES:
        	Estado = 0;
        	setValores();
        	break;

        case TRAMA_PASSWORD:
            if(Dato == '>') {
            	EEPROM_Write(password);
            	Estado = 0;
            	Trama_ok = 1;
            }
            else {
            	password[Pos] = Dato;
            	Pos++;
            }
            break;
    }

    return Trama_ok;
}

uint32_t StringToInt(uint8_t Val1, uint8_t Val2) {
	uint32_t numero = 0;

	if(Val2 == '\0')
		Val2 = '0';

	numero = (Val2 - '0') + (Val1-'0')*10;

	return numero;
}

void setValores(void) {
	temperatura_ambiente = StringToInt(Temp_Amb[0],
                                       Temp_Amb[1]);

	temperatura_ambiente_hora_inicio[0] = StringToInt(Temp_Amb_Hora_Inicio[0],
                                                      Temp_Amb_Hora_Inicio[1]);

	temperatura_ambiente_hora_inicio[1] = StringToInt(Temp_Amb_Hora_Inicio[3],
                                                      Temp_Amb_Hora_Inicio[4]);

	temperatura_ambiente_hora_fin[1] = StringToInt(Temp_Amb_Hora_Fin[3],
                                                   Temp_Amb_Hora_Fin[4]);

	iluminacion_hora_inicio[0] = StringToInt(Ilum_Hora_Inicio[0],
                                             Ilum_Hora_Inicio[1]);

	iluminacion_hora_inicio[1] = StringToInt(Ilum_Hora_Inicio[3],
                                             Ilum_Hora_Inicio[4]);

	iluminacion_hora_fin[1] = StringToInt(Ilum_Hora_Fin[3],
                                          Ilum_Hora_Fin[4]);

	temperatura_ambiente_hora_fin[0] = StringToInt(Temp_Amb_Hora_Fin[0],
                                                   Temp_Amb_Hora_Fin[1]);

	iluminacion_hora_fin[0] = StringToInt(Ilum_Hora_Fin[0],
                                          Ilum_Hora_Fin[1]);
}
