/*******************************************************************************
 *
 * @file       Maquina_Porton.c
 * @brief      Módulo con la Maquina de Estados del porton.
 * @version    1.00
 * @date       15/09/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "Maquina_Porton.h"

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/
const void (*Maquina_Porton[]) () = {
		Funcion_Porton_Abriendo,
		Funcion_Porton_Abierto,
		Funcion_Porton_Cerrando,
		Funcion_Porton_Cerrado,
		Funcion_Porton_Alarma
};

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
volatile Estados_Porton estado_porton = PORTON_ABRIENDO;

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void MDE_Porton(void) {
	Maquina_Porton[estado_porton]();
}

void Funcion_Porton_Abriendo(void) {
	uint32_t sensor_porton_abierto	= Estado_Sensor_Porton_Abierto();
	uint32_t sensor_porton_cerrado	= Estado_Sensor_Porton_Cerrado();
	uint32_t control_porton			= Estado_Control_Garage();

	Porton(ABRIR);

	if(ON == sensor_porton_abierto) {
		estado_porton = PORTON_ABIERTO;
	}
	if(CERRAR == control_porton) {
		estado_porton = PORTON_CERRANDO;
	}
	if(ON == sensor_porton_cerrado) {
		estado_porton = PORTON_ALARMA;
	}
}

void Funcion_Porton_Abierto(void) {
	uint32_t sensor_porton_cerrado	= Estado_Sensor_Porton_Cerrado();
	uint32_t control_porton			= Estado_Control_Garage();

	Porton(PARAR);

	if(CERRAR == control_porton) {
		estado_porton = PORTON_CERRANDO;
	}
	if(ON == sensor_porton_cerrado) {
		estado_porton = PORTON_ALARMA;
	}
}

void Funcion_Porton_Cerrando(void) {
	uint32_t sensor_porton_abierto	= Estado_Sensor_Porton_Abierto();
	uint32_t sensor_porton_cerrado	= Estado_Sensor_Porton_Cerrado();
	uint32_t control_porton			= Estado_Control_Garage();

	Porton(CERRAR);

	if(ON == sensor_porton_cerrado) {
		estado_porton = PORTON_CERRADO;
	}
	if(ABRIR == control_porton) {
		estado_porton = PORTON_ABRIENDO;
	}
	if(ON == sensor_porton_abierto) {
		estado_porton = PORTON_ALARMA;
	}
}

void Funcion_Porton_Cerrado(void) {
	uint32_t sensor_porton_abierto	= Estado_Sensor_Porton_Abierto();
	uint32_t control_porton			= Estado_Control_Garage();

	Porton(PARAR);

	if(ABRIR == control_porton) {
		estado_porton = PORTON_ABRIENDO;
	}
	if(ON == sensor_porton_abierto) {
		estado_porton = PORTON_ALARMA;
	}
}

void Funcion_Porton_Alarma(void) {
	Led_RGB(RGB_ROJO);
	if(sensores_alarma){
		Porton(ABRIR);
	}
	else {
		Porton(PARAR);
	}
}
