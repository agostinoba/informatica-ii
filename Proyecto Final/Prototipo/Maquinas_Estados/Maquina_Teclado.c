/*******************************************************************************
 *
 * @file       Maquina_Teclado.c
 * @brief      Módulo con la Maquina de Estados del teclado para hacer puebas.
 * @version    1.00
 * @date       06/08/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "Maquina_Teclado.h"

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/
const void (*Maquina_Teclado[]) () = {
		Funcion_Tecla1,
		Funcion_Tecla2,
		Funcion_Tecla3,
		Funcion_Tecla4,
		Funcion_Tecla5,
		Funcion_Tecla1_Mantenida,
		Funcion_Tecla2_Mantenida,
		Funcion_Tecla3_Mantenida,
		Funcion_Tecla4_Mantenida,
		Funcion_Tecla5_Mantenida
};

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/
extern uint32_t horaRTC[];
extern uint8_t Hora[];

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
uint8_t Modo = '0';

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void MDE_Teclado(void) {
	uint32_t tecla;
	tecla = Teclado_Leer();
	if(0 < tecla && tecla < 6)
		Maquina_Teclado[tecla - 1]();
	if(10 < tecla && tecla < 16)
		Maquina_Teclado[tecla - 6]();
}

void Funcion_Tecla1(void) {
//	uint32_t dato[] = {7, 10, 1991};
//	EEPROM_Write(dato);

	Timer_Stop(2);
	Timer_Start(2, 20, 400, Mostrar_Modo);
}

void Funcion_Tecla2(void) {
	Timer_Stop(2);
//	Modo = 1;
//	Timer_Start(2, 20, 400, Mostrar_Modo);

//	Mostrar_Memoria();
}

void Funcion_Tecla3(void) {
	uint32_t dato[] = {1, 2, 3};
	EEPROM_Write(dato);
}

void Funcion_Tecla4(void) {
	Timer_Start(2, 20, 400, Mostrar_Hora_Funcionamiento);
	Timer_Stop(2);
}

void Funcion_Tecla5(void) {
	Borrar_LCD();
	Led_RGB(RGB_OFF);
	Timer_Stop(2);
}

void Funcion_Tecla1_Mantenida(void) {
//	Led_RGB(RGB_ROJO);

	// SOLO PARA DEMOSTRACION
	if(sensor_control_porton == OFF)
		sensor_control_porton = ON;
	else
		sensor_control_porton = OFF;

	Timer_Start(2, 20, 400, Mostrar_Estado_Luz);
}

void Funcion_Tecla2_Mantenida(void) {
//	Led_RGB(RGB_AZUL);

	// SOLO PARA DEMOSTRACIONv
	if(sensor_luz == OFF)
		sensor_luz = ON;
	else
		sensor_luz = OFF;

	Timer_Start(2, 20, 400, Mostrar_Estado_Control);
}

void Funcion_Tecla3_Mantenida(void) {
//	Led_RGB(RGB_VERDE);

	// SOLO PARA DEMOSTRACION
	if(sensor_porton_abierto == OFF)
		sensor_porton_abierto = ON;
	else
		sensor_porton_abierto = OFF;

	Timer_Start(2, 20, 400, Mostrar_Estado_Porton_Cerrar);
}

void Funcion_Tecla4_Mantenida(void) {
//	Led_RGB(RGB_VIOLETA);

	// SOLO PARA DEMOSTRACION
	if(sensor_porton_cerrado == OFF)
		sensor_porton_cerrado = ON;
	else
		sensor_porton_cerrado = OFF;

	Timer_Start(2, 20, 400, Mostrar_Estado_Porton_Abrir);
}

void Funcion_Tecla5_Mantenida(void) {
//	Timer_Stop(2);
//	Led_RGB(RGB_OFF);

	// SOLO PARA DEMOSTRACION
	estado_control = CONTROL_NO_PRESIONADO;
}

/* -------------- Fin maquina de estados con puntero a funcion -------------- */

void Mostrar_Memoria(void) {
//	char mensaje[50];
	EEPROM_Read();
//	sprintf(mensaje, "Memoria:   %02d:%02d:%02d",
//	I2CSlaveBuffer[0],
//	I2CSlaveBuffer[1],
//	I2CSlaveBuffer[2]);
//	Display_lcd(mensaje, RENGLON_1, 0x00);
//	Display_lcd(mensaje, RENGLON_2, 0x00);
}

void Mostrar_Modo(void) {
//	char mensaje[50];
//	sprintf(mensaje, "Hora    %02d:%02d:%02d",
//	horaRTC[0],
//	horaRTC[1],
//	horaRTC[2]);
//	Display_lcd(mensaje, RENGLON_1, 0x00);
//	Display_lcd(mensaje, RENGLON_2, 0x00);

	char mensaje[50];
	Mensaje_LCD("--Temperatura---", RENGLON_1);
	sprintf(mensaje, "      %.02f      ", temp_amb_sensor);
	Mensaje_LCD(mensaje, RENGLON_2);
}

void Mostrar_Hora_Funcionamiento(void) {
	char mensaje[50];
	sprintf(mensaje, "Hora    %02d:%02d:%02d",
			horaRTC[0],
			horaRTC[1],
			horaRTC[2]);
	Display_lcd(mensaje, RENGLON_1, 0x00);
	Display_lcd(mensaje, RENGLON_2, 0x00);
}

void Mostrar_Estado_Control(void) {
	char mensaje[50];
	char hora[50];
	uint32_t control = Estado_Control_Garage();
	if(CONTROL_NO_PRESIONADO == control) {
		sprintf(mensaje, "Control: NO PRESIONADO              ");
//		Led_RGB(RGB_ROJO);
	}
	if(ABRIR == control) {
		sprintf(mensaje, "Control: ABRIR                      ");
//		Led_RGB(RGB_VERDE);
	}
	if(CERRAR == control) {
		sprintf(mensaje, "Control: CERRAR                     ");
//		Led_RGB(RGB_AZUL);
	}

	Display_lcd(mensaje, RENGLON_1, 0x00);
	sprintf(hora, "Hora    %02d:%02d:%02d",
			horaRTC[0],
			horaRTC[1],
			horaRTC[2]);
	Display_lcd(hora, RENGLON_2, 0x00);
}

void Mostrar_Estado_Luz(void) {
	char mensaje[50];
	char hora[50];
	uint32_t luz = Estado_Sensor_Luz();
	if(OFF == luz) {
		sprintf(mensaje, "Luz: NO LUZ                         ");
//		Led_RGB(RGB_ROJO);
	}
	if(ON == luz) {
		sprintf(mensaje, "Luz: LUZ                            ");
//		Led_RGB(RGB_VERDE);
	}

	Display_lcd(mensaje, RENGLON_1, 0x00);
	sprintf(hora, "Hora    %02d:%02d:%02d",
			horaRTC[0],
			horaRTC[1],
			horaRTC[2]);
	Display_lcd(hora, RENGLON_2, 0x00);
}

void Mostrar_Estado_Porton_Abrir(void) {
	char mensaje[50];
	char hora[50];
	uint32_t abierto = Estado_Sensor_Porton_Abierto();
	if(OFF == abierto) {
		sprintf(mensaje, "Abrir: NO PRESIONADO                ");
//		Led_RGB(RGB_ROJO);
	}
	if(ON == abierto) {
		sprintf(mensaje, "Abrir: PRESIONADO                   ");
//		Led_RGB(RGB_VERDE);
	}

	Display_lcd(mensaje, RENGLON_1, 0x00);
	sprintf(hora, "Hora    %02d:%02d:%02d",
			horaRTC[0],
			horaRTC[1],
			horaRTC[2]);
	Display_lcd(hora, RENGLON_2, 0x00);
}

void Mostrar_Estado_Porton_Cerrar(void) {
	char mensaje[50];
	char hora[50];
	uint32_t cerrado = Estado_Sensor_Porton_Cerrado();
	if(OFF == cerrado) {
		sprintf(mensaje, "Cerrar: NO PRESIONADO               ");
//		Led_RGB(RGB_ROJO);
	}
	if(ON == cerrado) {
		sprintf(mensaje, "Cerrar: PRESIONADO                  ");
//		Led_RGB(RGB_VERDE);
	}

	Display_lcd(mensaje, RENGLON_1, 0x00);
	sprintf(hora, "Hora    %02d:%02d:%02d",
			horaRTC[0],
			horaRTC[1],
			horaRTC[2]);
	Display_lcd(hora, RENGLON_2, 0x00);
}
