/*******************************************************************************
*
* @file        Maquina_Configuracion.c
* @brief       Módulo con la Maquina de Estados para ver las configurariones.
* @version     1.00
* @date        15/09/2020
* @author      Agostino Barbetti
*
 ******************************************************************************/

/*******************************************************************************
*** INCLUDES
 ******************************************************************************/
#include <Maquina_Configuracion.h>

/*******************************************************************************
*** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
*** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
*** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/
typedef enum {
	CONF_INICIO,
	CONF_HORARIO_ILUM,
	CONF_HORARIO_CALEFACCION,
	CONF_TEMP_CALEFACCION,
	CONF_N_ESTADOS
}Estados_Configuracion;

/*******************************************************************************
*** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/
const void (*Maquina_Configuracion[]) () = {
		Funcion_Configuracion_Inicio,
		Funcion_Configuracion_Horario_Iluminacion,
		Funcion_Configuracion_Horario_Calefaccion,
		Funcion_Configuracion_Temperatura_Calefaccion
};

/*******************************************************************************
*** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
*** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
static Estados_Configuracion estado_configuracion = CONF_INICIO;
static Estados_Configuracion indice_mensajes_configuracion = CONF_HORARIO_ILUM;

/*******************************************************************************
*** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
*** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
*** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void MDE_Configuracion(void) {
	Maquina_Configuracion[estado_configuracion]();
}

void Funcion_Configuracion_Inicio(void) {
	uint32_t tecla;

	Mensaje_LCD("-Configuracion--", RENGLON_1);

	tecla = Teclado_Leer();
	if(TECLA1 == tecla) {
		estado_configuracion = indice_mensajes_configuracion;
		indice_mensajes_configuracion = CONF_INICIO;
		Mensaje_LCD("                ", RENGLON_2);
	}
	if(TECLA2 == tecla) {
		indice_mensajes_configuracion--;
		if(CONF_HORARIO_ILUM > indice_mensajes_configuracion)
			indice_mensajes_configuracion = CONF_TEMPERATURA_CALEFACCION;
	}
	if(TECLA3 == tecla) {
		indice_mensajes_configuracion++;
		if(CONF_TEMP_CALEFACCION < indice_mensajes_configuracion)
			indice_mensajes_configuracion = CONF_HORARIO_ILUMINACION;
	}
	if(TECLA5 == tecla) {
		estado_menu = MENU_INICIO;
		Mensaje_LCD("                ", RENGLON_2);
	}

	if(CONF_HORARIO_ILUM == indice_mensajes_configuracion) {
			Mensaje_LCD("Hora Iluminacion", RENGLON_2);
	}

	if(CONF_HORARIO_CALEFACCION == indice_mensajes_configuracion) {
		Mensaje_LCD("Hora Calefaccion", RENGLON_2);
	}

	if(CONF_TEMP_CALEFACCION == indice_mensajes_configuracion) {
		Mensaje_LCD("Temp Calefaccion", RENGLON_2);
	}
}

void Funcion_Configuracion_Horario_Iluminacion(void) {
	uint32_t tecla;
	char mensaje[50];

	sprintf(mensaje, "I %02d:%02d  F %02d:%02d",
			iluminacion_hora_inicio[0],
			iluminacion_hora_inicio[1],
			iluminacion_hora_fin[0],
			iluminacion_hora_fin[1]);
	Mensaje_LCD("Hora Iluminacion", RENGLON_1);
	Mensaje_LCD(mensaje, RENGLON_2);

	tecla = Teclado_Leer();
	if(TECLA5 == tecla) {
		estado_configuracion = CONF_INICIO;
		Mensaje_LCD("                ", RENGLON_2);
	}
}

void Funcion_Configuracion_Horario_Calefaccion(void) {
	uint32_t tecla;
	char mensaje[50];

	sprintf(mensaje, "I %02d:%02d  F %02d:%02d",
			temperatura_ambiente_hora_inicio[0],
			temperatura_ambiente_hora_inicio[1],
			temperatura_ambiente_hora_fin[0],
			temperatura_ambiente_hora_fin[1]);
	Mensaje_LCD("Hora Calefaccion", RENGLON_1);
	Mensaje_LCD(mensaje, RENGLON_2);

	tecla = Teclado_Leer();
	if(TECLA5 == tecla) {
		estado_configuracion = CONF_INICIO;
		Mensaje_LCD("                ", RENGLON_2);
	}
}

void Funcion_Configuracion_Temperatura_Calefaccion(void) {
	uint32_t tecla;
	char mensaje[50];

	sprintf(mensaje, "     %.01d C      ", temperatura_ambiente);
	Mensaje_LCD("Temp Calefaccion", RENGLON_1);
	Mensaje_LCD(mensaje, RENGLON_2);

	tecla = Teclado_Leer();
	if(TECLA5 == tecla) {
		estado_configuracion = CONF_INICIO;
		Mensaje_LCD("                ", RENGLON_2);
	}
}
