/********************************************************************************************************
*
* @file		Maquina_Memoria.c
* @brief	Módulo con la Maquina de Estados para el manejo de la memoria I2C
* @version	1.00
* @date
* @author	Agostino Barbetti
*
********************************************************************************************************/

/********************************************************************************************************
*** INCLUDES
********************************************************************************************************/
#include <Maquina_Memoria.h>

/********************************************************************************************************
*** DEFINES PRIVADOS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** MACROS PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** TIPOS DE DATOS PRIVADOS AL MODULO
********************************************************************************************************/
typedef enum {
	MEMORIA_INICIO,
	MEMORIA_LEER,
	MEMORIA_ESCRIBIR,
	MEMORIA_ALARMA,
	MEMORIA_N_ESTADOS
}Estados_Memoria;

/********************************************************************************************************
*** TABLAS PRIVADAS AL MODULO
********************************************************************************************************/
const void (*Maquina_Memoria[]) () = {
		Funcion_Memoria_Inicio,
		Funcion_Memoria_Leer,
		Funcion_Memoria_Escribir,
		Funcion_Memoria_Alarma
};

/********************************************************************************************************
*** VARIABLES GLOBALES PUBLICAS
********************************************************************************************************/

/********************************************************************************************************
*** VARIABLES GLOBALES PRIVADAS AL MODULO
********************************************************************************************************/
static Estados_Memoria estado_memoria = MEMORIA_INICIO;
static Estados_Memoria indice_mensajes_memoria = MEMORIA_LEER;
static int i = 1;

/********************************************************************************************************
*** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** FUNCIONES PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** FUNCIONES GLOBALES AL MODULO
********************************************************************************************************/

void MDE_Memoria(void) {
	Maquina_Memoria[estado_memoria]();
}

void Funcion_Memoria_Inicio(void) {
	uint32_t tecla;

	Mensaje_LCD("--Memoria I2C---", RENGLON_1);

	tecla = Teclado_Leer();
	if(TECLA1 == tecla) {
		estado_memoria = indice_mensajes_memoria;
		Mensaje_LCD("                ", RENGLON_2);
	}
	if(TECLA2 == tecla) {
		indice_mensajes_memoria--;
		if(MEMORIA_LEER > indice_mensajes_memoria)
			indice_mensajes_memoria = MEMORIA_ESCRIBIR;
	}
	if(TECLA3 == tecla) {
		indice_mensajes_memoria++;
		if(MEMORIA_ESCRIBIR < indice_mensajes_memoria)
			indice_mensajes_memoria = MEMORIA_LEER;
	}
	if(TECLA5 == tecla) {
		estado_menu = MENU_INICIO;
		Mensaje_LCD("                ", RENGLON_2);
	}


	if(MEMORIA_LEER == indice_mensajes_memoria) {
		Mensaje_LCD("Leer            ", RENGLON_2);
	}
	if(MEMORIA_ESCRIBIR == indice_mensajes_memoria) {
		Mensaje_LCD("Escribir        ", RENGLON_2);
	}
}

void Funcion_Memoria_Leer(void) {
	char mensaje[50];
	uint32_t tecla;
	static int count = 0;
	if(i) {
		EEPROM_Read();
		i = 0;
	}
	if(count >= 50000) {
		sprintf(mensaje, "Memoria %c%c%c", I2CSlaveBuffer[0], I2CSlaveBuffer[1], I2CSlaveBuffer[2]);
		Mensaje_LCD(mensaje, RENGLON_2);
		i = 1;
		count = 0;
	}

	count++;

	tecla = Teclado_Leer();
	if(TECLA5 == tecla) {
		estado_memoria = MEMORIA_INICIO;
		Mensaje_LCD("                ", RENGLON_2);
	}
}

void Funcion_Memoria_Escribir(void) {
	uint32_t dato[3];
	RTC_GetCurrentTime(dato);
//	EEPROM_Write(dato);
	i = 1;
	estado_memoria = MEMORIA_INICIO;
}

void Funcion_Memoria_Alarma(void) {

}
