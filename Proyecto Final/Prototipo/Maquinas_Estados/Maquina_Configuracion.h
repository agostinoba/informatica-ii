/*******************************************************************************
 *
 * @file       Maquina_Configuracion.h
 * @brief      Módulo con la Maquina de Estados para ver las configurariones.
 * @version    1.00
 * @date       15/09/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef MENU_MAQUINA_CONFIGURACION_H_
#define MENU_MAQUINA_CONFIGURACION_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <Maquina_Menu.h>
#include <Primitivas/PR_Teclado.h>
#include <Primitivas/PR_LCD.h>
#include <Maquina_Domotica.h>
#include "Types.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void MDE_Configuracion(void)
 * @brief		Máquina de estados para ver la configuración.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void MDE_Configuracion(void);

/*******************************************************************************
 * @fn			void Funcion_Configuracion_Inicio(void)
 * @brief		Menú para seleccionar cuál de las configuraciones quiero ver.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Configuracion_Inicio(void);

/*******************************************************************************
 * @fn			void Funcion_Configuracion_Horario_Iluminacion(void)
 * @brief		Menú que me permite ver la hora de ilumnación configurada.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Configuracion_Horario_Iluminacion(void);

/*******************************************************************************
 * @fn			void Funcion_Configuracion_Horario_Calefaccion(void)
 * @brief		Menú que me permite ver la hora de la calefacción configurada.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Configuracion_Horario_Calefaccion(void);

/*******************************************************************************
 * @fn			void Funcion_Configuracion_Temperatura_Calefaccion(void)
 * @brief		Menú que me permite ver la temperatura  configurada.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Configuracion_Temperatura_Calefaccion(void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* MENU_MAQUINA_CONFIGURACION_H_ */
