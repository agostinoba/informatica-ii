/*******************************************************************************
 *
 * @file       Maquina_Simulacion.c
 * @brief      Módulo con la Maquina de Estados para simular el Hardware.
 * @version    1.00
 * @date       15/09/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <Maquina_Simulacion.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/
typedef enum {
	SIMULACION_INICIO,
	SIMULACION_CONTROL_PORTON,
	SIMULACION_SENSOR_LUZ,
	SIMULACION_SENSORES_ALARMA,
	SIMULACION_N_ESTADOS
}Estados_Simulacion;

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/
const void (*Maquina_Simulacion[]) () = {
		Funcion_Simulacion_Inicio,
		Funcion_Simulacion_Control_Porton,
		Funcion_Simulacion_Sensor_Luz,
		Funcion_Simulacion_Sensores_Alarma
};

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
static Estados_Simulacion estado_simulacion = SIMULACION_INICIO;
static Estados_Simulacion indice_mensajes_simulacion =
	SIMULACION_CONTROL_PORTON;

volatile uint32_t sensor_control_porton	= CONTROL_NO_PRESIONADO;
volatile uint32_t sensor_porton_abierto	= OFF;
volatile uint32_t sensor_porton_cerrado	= OFF;
volatile uint32_t sensor_luz			= OFF;
volatile uint32_t sensores_alarma		= OFF;

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/

void MDE_Simulacion(void) {
	Maquina_Simulacion[estado_simulacion]();
}

void Funcion_Simulacion_Inicio(void) {

	uint32_t tecla;

	Mensaje_LCD("---Simulacion---", RENGLON_1);

	tecla = Teclado_Leer();
	if(TECLA1 == tecla) {
		estado_simulacion = indice_mensajes_simulacion;
		indice_mensajes_simulacion = SIMULACION_INICIO;
		Mensaje_LCD("                ", RENGLON_2);
	}
	if(TECLA2 == tecla) {
		indice_mensajes_simulacion--;
		if(SIMULACION_CONTROL_PORTON > indice_mensajes_simulacion)
			indice_mensajes_simulacion = SIMULACION_SENSORES_ALARMA;
	}
	if(TECLA3 == tecla) {
		indice_mensajes_simulacion++;
		if(SIMULACION_SENSORES_ALARMA < indice_mensajes_simulacion)
			indice_mensajes_simulacion = SIMULACION_CONTROL_PORTON;
	}
	if(TECLA5 == tecla)
		estado_menu = MENU_INICIO;

	if(SIMULACION_CONTROL_PORTON == indice_mensajes_simulacion) {
		Mensaje_LCD("Control Porton     ", RENGLON_2);
	}

	if(SIMULACION_SENSOR_LUZ == indice_mensajes_simulacion) {
		Mensaje_LCD("Sensor Luz       ", RENGLON_2);
	}

	if(SIMULACION_SENSORES_ALARMA == indice_mensajes_simulacion) {
		Mensaje_LCD("Sensor Alarma       ", RENGLON_2);
	}
}

void Funcion_Simulacion_Control_Porton(void) {

	uint32_t tecla;

	Mensaje_LCD("-Control Porton-", RENGLON_1);

	tecla = Teclado_Leer();
	if(TECLA1 == tecla) {
		sensor_control_porton = ABRIR;
		Mensaje_LCD("Control: Abrir  ", RENGLON_2);
	}
	if(TECLA2 == tecla) {
		sensor_control_porton = CERRAR;
		Mensaje_LCD("Control: Cerrar ", RENGLON_2);
	}
	if(TECLA3 == tecla) {
		sensor_porton_abierto = !sensor_porton_abierto;
		if(ON == sensor_porton_abierto) {
			Mensaje_LCD("S. Abierto: ON  ", RENGLON_2);
		}
		else {
			Mensaje_LCD("S. Abierto: OFF ", RENGLON_2);
		}
	}
	if(TECLA4 == tecla) {
		sensor_porton_cerrado = !sensor_porton_cerrado;
		if(ON == sensor_porton_cerrado) {
			Mensaje_LCD("S. Cerrado: ON  ", RENGLON_2);
		}
		else {
			Mensaje_LCD("S. Cerrado: OFF ", RENGLON_2);
		}
	}
	if(TECLA5 == tecla) {
		estado_simulacion = SIMULACION_INICIO;
		estado_porton = PORTON_ABRIENDO;
		Led_RGB(RGB_OFF);
		Mensaje_LCD("                ", RENGLON_2);
	}
}

void Funcion_Simulacion_Sensor_Luz(void) {

	uint32_t tecla;

	Mensaje_LCD("---Sensor Luz---", RENGLON_1);

	tecla = Teclado_Leer();
	if(TECLA2 == tecla) {
		sensor_luz = ON;
		Mensaje_LCD("Sensor Luz: ON  ", RENGLON_2);
	}
	if(TECLA3 == tecla) {
		sensor_luz = OFF;
		Mensaje_LCD("Sensor Luz: OFF ", RENGLON_2);
	}
	if(TECLA5 == tecla) {
		estado_simulacion = SIMULACION_INICIO;
		Mensaje_LCD("                ", RENGLON_2);
	}
}

void Funcion_Simulacion_Sensores_Alarma(void) {

	uint32_t tecla;

	Mensaje_LCD("-Sensor Alarma--", RENGLON_1);

	tecla = Teclado_Leer();
	if(TECLA2 == tecla) {
		Mensaje_LCD("-----> 3 <------", RENGLON_2);
		sensores_alarma = ON;
		estado_porton = PORTON_ALARMA;
	}
	if(TECLA3 == tecla) {
		estado_porton = PORTON_ABRIENDO;
		sensor_porton_cerrado = OFF;
		sensor_porton_abierto = OFF;
		Mensaje_LCD("                ", RENGLON_2);
		Led_RGB(RGB_OFF);
	}
	if(TECLA5 == tecla) {
		estado_simulacion = SIMULACION_INICIO;
		estado_porton = PORTON_ABRIENDO;
		Mensaje_LCD("                ", RENGLON_2);
	}
}

