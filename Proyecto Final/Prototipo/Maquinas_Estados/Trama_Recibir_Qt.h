/*******************************************************************************
 *
 * @file       Trama_Recibir_Qt.h
 * @brief      Módulo para el uso del Timer.
 * @version    1.00
 * @date       15/09/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef TRAMA_RECIBIR_QT_H_
#define TRAMA_RECIBIR_QT_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <Maquina_Domotica.h>
#include <Primitivas/PR_RTC.h>
#include <Primitivas/PR_I2C.h>
#include "Trama_Enviar_Qt.h"
#include "Types.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/
extern volatile uint8_t Temp_Amb[2];
extern volatile uint8_t Temp_Amb_Hora_Inicio[5];
extern volatile uint8_t Temp_Amb_Hora_Fin[5];
extern volatile uint8_t Ilum_Hora_Inicio[5];
extern volatile uint8_t Ilum_Hora_Fin[5];

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			uint8_t Descifrar_Trama(uint8_t Dato)
 * @brief		Función que extrae los datos de la trama recibida y los guarda
 *              en variables
 * @param[in]	Dato:		Trama recibida por UART
 * @return		Si la trama es correcta retorna 1
 ******************************************************************************/
uint8_t Descifrar_Trama(uint8_t Dato);

/*******************************************************************************
 * @fn			uint32_t StringToInt(uint8_t Val1, uint8_t Val2)
 * @brief		Convierte strings en enteros. Se usan para los números recibidos
 *              por UART
 * @param[in]	Val1:		Decena del número que quiero convertir
 * @param[in]	Val2:		Unidad del número que quiero convertir
 * @return		Entero recibido por UART
 ******************************************************************************/
uint32_t StringToInt(uint8_t Val1, uint8_t Val2);

/*******************************************************************************
 * @fn			void setValores(void)
 * @brief		Función que setea los valores recibidos por UART en las
 *              variables globales.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void setValores(void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* TRAMA_RECIBIR_QT_H_ */
