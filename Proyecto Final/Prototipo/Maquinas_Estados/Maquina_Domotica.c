/********************************************************************************************************
*
* @file		Maquina_Domotica.c
* @brief	Módulo con la Maquina de Estados que automatiza la casa.
* @version	1.00
* @date
* @author	Agostino Barbetti
*
********************************************************************************************************/

/********************************************************************************************************
*** INCLUDES
********************************************************************************************************/
#include <Maquina_Domotica.h>

/********************************************************************************************************
*** DEFINES PRIVADOS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** MACROS PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** TIPOS DE DATOS PRIVADOS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** TABLAS PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** VARIABLES GLOBALES PUBLICAS
********************************************************************************************************/
volatile uint32_t temperatura_ambiente;
volatile uint32_t temperatura_ambiente_hora_inicio[2];
volatile uint32_t temperatura_ambiente_hora_fin[2];
volatile uint32_t iluminacion_hora_inicio[2];
volatile uint32_t iluminacion_hora_fin[2];

/********************************************************************************************************
*** VARIABLES GLOBALES PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** FUNCIONES PRIVADAS AL MODULO
********************************************************************************************************/

/********************************************************************************************************
*** FUNCIONES GLOBALES AL MODULO
********************************************************************************************************/
void MDE_Domotica(void) {

	float temperatura_actual;
	uint32_t hora_actual[3];

	temperatura_actual = GetTemperatura();
	RTC_GetCurrentTime(hora_actual);

	if((temperatura_ambiente_hora_inicio[0] < hora_actual[0]) || ((temperatura_ambiente_hora_inicio[0] == hora_actual[0]) && (hora_actual[1] >= temperatura_ambiente_hora_inicio[1]))) {

		if((temperatura_ambiente_hora_fin[0] > hora_actual[0]) || ((temperatura_ambiente_hora_fin[0] == hora_actual[0]) && (hora_actual[1] < temperatura_ambiente_hora_fin[1]))) {

			if(temperatura_actual <= (temperatura_ambiente - RANGO_TEMPERATURA)) {
				SetCalefaccion(ON);
			}
			else if(temperatura_actual >= (temperatura_ambiente + RANGO_TEMPERATURA)) {
				SetCalefaccion(OFF);
			}
		}
		else {
			SetCalefaccion(OFF);
		}
	}
	else {
		SetCalefaccion(OFF);
	}

	if((iluminacion_hora_inicio[0] < hora_actual[0]) || ((iluminacion_hora_inicio[0] == hora_actual[0]) && (hora_actual[1] >= iluminacion_hora_inicio[1]))) {

		if((iluminacion_hora_fin[0] > hora_actual[0]) || ((iluminacion_hora_fin[0] == hora_actual[0]) && (hora_actual[1] < iluminacion_hora_fin[1]))) {
			SetIluminacion(ON);
		}
		else {
			SetIluminacion(OFF);
		}
	}
	else {
		SetIluminacion(OFF);
	}

}

