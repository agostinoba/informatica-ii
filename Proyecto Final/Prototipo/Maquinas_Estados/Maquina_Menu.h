/*******************************************************************************
 *
 * @file       Maquina_Menu.h
 * @brief      Módulo con la Maquina de Estados con las opciones del menú.
 * @version    1.00
 * @date       15/09/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef MENU_MAQUINA_MENU_H_
#define MENU_MAQUINA_MENU_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <Maquina_Configuracion.h>
#include <Maquina_Simulacion.h>
#include <Maquina_Memoria.h>
#include <Primitivas/PR_LCD.h>
#include <Primitivas/PR_Teclado.h>
#include <Primitivas/PR_Timer.h>
#include <Primitivas/PR_ADC.h>
#include <Primitivas/PR_RTC.h>

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/
typedef enum {
	MENU_INICIO,
	MENU_HORA,
	MENU_TEMPERATURA,
	MENU_CONFIGURACION,
	MENU_SIMULACION,
	MENU_MEMORIA,
	MENU_ALARMA,
	MENU_N_ESTADOS
}Estados_Menu;

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/
extern volatile Estados_Menu estado_menu;
extern volatile uint8_t flag_alarma;

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void MDE_Menu(void)
 * @brief		Máquina de estados para ver las opciones de menú.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void MDE_Menu(void);

/*******************************************************************************
 * @fn			void Funcion_Menu_Inicio(void)
 * @brief		Menú para seleccionar cuál de los menús quiero ingresar.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Menu_Inicio(void);

/*******************************************************************************
 * @fn			void Funcion_Menu_Hora(void)
 * @brief		Opción para ver la hora actual seteada en el RTC.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Menu_Hora(void);

/*******************************************************************************
 * @fn			void Funcion_Menu_Temperatura(void)
 * @brief		Opción para ver la temperatura ambiente actual.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Menu_Temperatura(void);

/*******************************************************************************
 * @fn			void Funcion_Menu_Configuracion(void)
 * @brief		Opción para entrar en el menú de configuración.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Menu_Configuracion(void);

/*******************************************************************************
 * @fn			void Funcion_Menu_Simulacion(void)
 * @brief		Opción para entrar en el menú de simulación.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Menu_Simulacion(void);

/*******************************************************************************
 * @fn			void Funcion_Menu_Memoria(void)
 * @brief		Opción para entrar en el menú de la memoria EEPROM.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Menu_Memoria(void);

/*******************************************************************************
 * @fn			void Funcion_Menu_Alarma(void)
 * @brief		Estado en el entra al detectar algún error.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Menu_Alarma(void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* MENU_MAQUINA_MENU_H_ */
