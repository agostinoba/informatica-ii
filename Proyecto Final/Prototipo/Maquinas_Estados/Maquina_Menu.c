/*******************************************************************************
 *
 * @file       Maquina_Menu.c
 * @brief      Módulo con la Maquina de Estados con las opciones del menú.
 * @version    1.00
 * @date       15/09/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <Maquina_Menu.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/
const void (*Maquina_Menu[]) () = {
		Funcion_Menu_Inicio,
		Funcion_Menu_Hora,
		Funcion_Menu_Temperatura,
		Funcion_Menu_Configuracion,
		Funcion_Menu_Simulacion,
		Funcion_Menu_Memoria,
		Funcion_Menu_Alarma
};

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
volatile Estados_Menu estado_menu = MENU_INICIO;
static Estados_Menu indice_mensajes_menu = MENU_HORA;
volatile uint8_t flag_alarma = 0;

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void MDE_Menu(void) {
	Maquina_Menu[estado_menu]();
}

void Funcion_Menu_Inicio(void) {

	uint32_t tecla;

	Mensaje_LCD("-Menu Principal-", RENGLON_1);

	tecla = Teclado_Leer();
	if(TECLA1 == tecla) {
		estado_menu = indice_mensajes_menu;
		Mensaje_LCD("                ", RENGLON_2);
	}
	if(TECLA2 == tecla) {
		indice_mensajes_menu--;
		if(MENU_HORA > indice_mensajes_menu)
			indice_mensajes_menu = MENU_MEMORIA;
	}
	if(TECLA3 == tecla) {
		indice_mensajes_menu++;
		if(MENU_MEMORIA < indice_mensajes_menu)
			indice_mensajes_menu = MENU_HORA;
	}

	if(MENU_HORA == indice_mensajes_menu) {
		Mensaje_LCD("Hora actual     ", RENGLON_2);
	}
	if(MENU_TEMPERATURA == indice_mensajes_menu) {
		Mensaje_LCD("Temp. ambiente  ", RENGLON_2);
	}
	if(MENU_CONFIGURACION == indice_mensajes_menu) {
		Mensaje_LCD("Configuracion      ", RENGLON_2);
	}
	if(MENU_SIMULACION == indice_mensajes_menu) {
		Mensaje_LCD("Simulacion       ", RENGLON_2);
	}
	if(MENU_MEMORIA == indice_mensajes_menu) {
		Mensaje_LCD("Memoria         ", RENGLON_2);
	}
}

void Funcion_Menu_Hora(void) {
	uint32_t tecla;
	char mensaje[50];
	uint32_t hora_actual[3];

	RTC_GetCurrentTime(hora_actual);

	sprintf(mensaje, "    %02d:%02d:%02d    ",
			hora_actual[0],
			hora_actual[1],
			hora_actual[2]);
	Mensaje_LCD("--Hora Actual---", RENGLON_1);
	Mensaje_LCD(mensaje, RENGLON_2);

	tecla = Teclado_Leer();
	if(TECLA5 == tecla) {
		estado_menu = MENU_INICIO;
		Mensaje_LCD("                ", RENGLON_2);
	}
}

void Funcion_Menu_Temperatura(void) {
	uint32_t tecla;

	char mensaje[50];
	float temp;

	temp = GetTemperatura();

	sprintf(mensaje, "     %.01f C      ", temp);
	Mensaje_LCD("-Temp. Ambiente-", RENGLON_1);
	Mensaje_LCD(mensaje, RENGLON_2);

	tecla = Teclado_Leer();
	if(TECLA5 == tecla) {
		estado_menu = MENU_INICIO;
		Mensaje_LCD("                ", RENGLON_2);
	}
}

void Funcion_Menu_Configuracion(void) {
	MDE_Configuracion();
}

void Funcion_Menu_Simulacion(void) {
	MDE_Simulacion();
}

void Funcion_Menu_Memoria(void) {
	MDE_Memoria();
}

void Funcion_Menu_Alarma(void) {

}
