/*******************************************************************************
 *
 * @file       Maquina_Porton.h
 * @brief      Módulo con la Maquina de Estados del porton.
 * @version    1.00
 * @date       15/09/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef MAQUINA_PORTON_H_
#define MAQUINA_PORTON_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <Primitivas/PR_RGB.h>
#include <Control_Garage/Primitivas/PR_Control_Garage.h>
#include <Sensor_Porton/Primitivas/PR_Sensor_Porton.h>
#include <Motores_Porton/Primitivas/PR_Motores_Porton.h>
#include "Types.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/
typedef enum {
	PORTON_ABRIENDO,
	PORTON_ABIERTO,
	PORTON_CERRANDO,
	PORTON_CERRADO,
	PORTON_ALARMA,
	N_ESTADOS_TECLADO
}Estados_Porton;

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/
extern volatile Estados_Porton estado_porton;

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void MDE_Porton(void)
 * @brief		Máquina de estados para el manejo del porton.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void MDE_Porton(void);

/*******************************************************************************
 * @fn			void Funcion_Porton_Abriendo(void)
 * @brief		Función que abre el portón.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Porton_Abriendo(void);

/*******************************************************************************
 * @fn			void Funcion_Porton_Abierto(void)
 * @brief		Función de portón abierto.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Porton_Abierto(void);

/*******************************************************************************
 * @fn			void Funcion_Porton_Cerrando(void)
 * @brief		Función que cierra el portón.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Porton_Cerrando(void);

/*******************************************************************************
 * @fn			void Funcion_Porton_Cerrado(void)
 * @brief		Función de porton cerrado.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Porton_Cerrado(void);

/*******************************************************************************
 * @fn			void Funcion_Porton_Alarma(void)
 * @brief	    Estado en el entra al detectar algún error.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Porton_Alarma(void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* MAQUINA_PORTON_H_ */
