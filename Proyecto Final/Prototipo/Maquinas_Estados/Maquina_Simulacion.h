/*******************************************************************************
 *
 * @file       Maquina_Simulacion.h
 * @brief      Módulo con la Maquina de Estados para simular el Hardware.
 * @version    1.00
 * @date       15/09/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef MENU_MAQUINA_SIMULACION_H_
#define MENU_MAQUINA_SIMULACION_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <Maquina_Menu.h>
#include <Primitivas/PR_Teclado.h>
#include <Primitivas/PR_LCD.h>
#include <Primitivas/PR_RGB.h>
#include "Types.h"
#include "Maquina_Porton.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/


/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/
extern volatile uint32_t sensor_control_porton;
extern volatile uint32_t sensor_porton_abierto;
extern volatile uint32_t sensor_porton_cerrado;
extern volatile uint32_t sensor_luz;
extern volatile uint32_t sensores_alarma;

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void MDE_Simulacion(void)
 * @brief		Máquina de estados para simular hardware (sensores).
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void MDE_Simulacion(void);

/*******************************************************************************
 * @fn			void Funcion_Simulacion_Inicio(void)
 * @brief		Menú para seleccionar lo que deseo simular.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Simulacion_Inicio(void);

/*******************************************************************************
 * @fn			void Funcion_Simulacion_Control_Porton(void)
 * @brief		Opción para simular el control del portón.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Simulacion_Control_Porton(void);

/*******************************************************************************
 * @fn			void Funcion_Simulacion_Sensor_Luz(void);
 * @brief		Opción para simular el sensor de luz de la puerta de entrada.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Simulacion_Sensor_Luz(void);

/*******************************************************************************
 * @fn			void Funcion_Simulacion_Sensores_Alarma(void)
 * @brief		Opción para simular los sensores de alarma (incendio, robo).
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Simulacion_Sensores_Alarma(void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* MENU_MAQUINA_SIMULACION_H_ */
