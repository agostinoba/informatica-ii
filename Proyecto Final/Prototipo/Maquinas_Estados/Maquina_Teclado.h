/*******************************************************************************
 *
 * @file       Maquina_Teclado.h
 * @brief      Módulo con la Maquina de Estados del teclado para hacer puebas.
 * @version    1.00
 * @date       06/08/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef MAQUINA_TECLADO_H_
#define MAQUINA_TECLADO_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <Primitivas/PR_Teclado.h>
#include <Primitivas/PR_Timer.h>
#include <Primitivas/PR_RTC.h>
#include <Primitivas/PR_RGB.h>
#include <Primitivas/PR_I2C.h>
#include <Primitivas/PR_LCD.h>
#include <Primitivas/PR_ADC.h>
#include "Types.h"
#include "Trama_Enviar_Qt.h"

#include <Control_Garage/Drivers/DR_Control_Garage.h>
#include <Control_Garage/Primitivas/PR_Control_Garage.h>
#include <Sensor_Luz/Drivers/DR_Sensor_Luz.h>
#include <Sensor_Luz/Primitivas/PR_Sensor_Luz.h>
#include <Sensor_Porton/Drivers/DR_Sensor_Porton.h>
#include <Sensor_Porton/Primitivas/PR_Sensor_Porton.h>

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

/*typedef enum {
	TECLA1,
	TECLA2,
	TECLA3,
	TECLA4,
	TECLA5,
	TECLA1_MANTENIDA,
	TECLA2_MANTENIDA,
	TECLA3_MANTENIDA,
	TECLA4_MANTENIDA,
	TECLA5_MANTENIDA,
	N_ESTADOS_TECLADO
}Estados_Teclado;*/

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void MDE_Teclado(void)
 * @brief		Máquina de estados para las acciones de las teclas.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void MDE_Teclado(void);

/*******************************************************************************
 * @fn			void Funcion_Tecla1(void)
 * @brief		Función que ejecuta los comandos definidos al presionar
 *              la Tecla1.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Tecla1(void);

/*******************************************************************************
 * @fn			void Funcion_Tecla2(void)
 * @brief		Función que ejecuta los comandos definidos al presionar
 *              la Tecla2.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Tecla2(void);

/*******************************************************************************
 * @fn			void Funcion_Tecla3(void)
 * @brief		Función que ejecuta los comandos definidos al presionar
 *              la Tecla3.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Tecla3(void);

/*******************************************************************************
 * @fn			void Funcion_Tecla4(void)
 * @brief		Función que ejecuta los comandos definidos al presionar
 *              la Tecla4.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Tecla4(void);

/*******************************************************************************
 * @fn			void Funcion_Tecla5(void)
 * @brief		Función que ejecuta los comandos definidos al presionar
 *              la Tecla5.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Tecla5(void);

/*******************************************************************************
 * @fn			void Funcion_Tecla1_Mantenida(void)
 * @brief		Función que ejecuta los comandos definidos al mantener
 *              presionada la Tecla1.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Tecla1_Mantenida(void);

/*******************************************************************************
 * @fn			void Funcion_Tecla2_Mantenida(void)
 * @brief		Función que ejecuta los comandos definidos al mantener
 *              presionada la Tecla2.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Tecla2_Mantenida(void);

/*******************************************************************************
 * @fn			void Funcion_Tecla3_Mantenida(void)
 * @brief		Función que ejecuta los comandos definidos al mantener
 *              presionada la Tecla3.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Tecla3_Mantenida(void);

/*******************************************************************************
 * @fn			void Funcion_Tecla4_Mantenida(void)
 * @brief		Función que ejecuta los comandos definidos al mantener
 *              presionada la Tecla4.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Tecla4_Mantenida(void);

/*******************************************************************************
 * @fn			void Funcion_Tecla5_Mantenida(void)
 * @brief		Función que ejecuta los comandos definidos al mantener
 *              presionada la Tecla5.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Funcion_Tecla5_Mantenida(void);

/*******************************************************************************
 * @fn			void Mostrar_Modo(void)
 * @brief		Función de prueba que muestra en el LCD la temperatura.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Mostrar_Modo(void);

/*******************************************************************************
 * @fn			void Mostrar_Hora(void)
 * @brief		Función de prueba que muestra en el LCD la hora actual.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Mostrar_Hora_Funcionamiento(void);

/*******************************************************************************
 * @fn			void Mostrar_Estado_Control(void)
 * @brief		Función de prueba para ver que botón presiono del control
 *              del portón.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Mostrar_Estado_Control(void);

/*******************************************************************************
 * @fn			void Mostrar_Estado_Luz(void)
 * @brief		Función de prueba que muestra el estado del sensor de luz.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Mostrar_Estado_Luz(void);

/*******************************************************************************
 * @fn			void Mostrar_Estado_Porton_Abrir(void)
 * @brief		Función de prueba que muestra el estado del sensor de apertura
 *              del portón.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Mostrar_Estado_Porton_Abrir(void);

/*******************************************************************************
 * @fn			void Mostrar_Estado_Porton_Cerrar(void)
 * @brief		Función de prueba que muestra el estado del sensor de apertura
 *              del portón.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Mostrar_Estado_Porton_Cerrar(void);

/*******************************************************************************
 * @fn			void Mostrar_Memoria(void)
 * @brief		Función de prueba que lee la memoria EEPROM.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Mostrar_Memoria(void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* MAQUINA_TECLADO_H_ */
