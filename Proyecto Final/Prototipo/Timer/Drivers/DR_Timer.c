/*******************************************************************************
 *
 * @file       DR_Timer.c
 * @brief      Módulo con la funcion de Timer.
 * @version    1.00
 * @date       06/06/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <Drivers/DR_Timer.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
volatile STRUCT_TIMER Timer[CANT_TIMER];

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void Timer_Init(void) {
    // Habilito el Timer 0
	PCONP		|=	(1<<1);

    //  Clock de Timer (PCLK = CCLK), lo configuro para Clock/1
    // 00 = CCLK / 4;
    // 01 = CCLK / 1;
    // 10 = CCLK / 2;
    // 11 = CCLK / 8;
	PCLKSEL0	|=	1 << 2;

	// Pag. 499
    // Prescaler, valor en microsegundos (10^8 = segundos).
    // Cuando el prescaler llega al valor seteado, aumenta el contador del
    // timer y resetea el prescaler
	T0PR		=	100000000;

	// Pag. 502 Count Control Register
    // Lo seteo como modo Timer, se incrementa el Timer cada vez que que el
    // contador del prescaler es igual al valor de registro del prescaler.
	T0CTCR		=	0;

    // Habilito flanco ascendente, descendente y la interrupción.
	T0CCR		=	0X6;
	//NVIC_EnableIRQ(TIMER0_IRQn);	// Habilito la interrupción en el NVIC.
	T0TCR		=	2;				// Reinicio el timer.
	T0TCR		=	1; 			// Habilito el timer.
}

void Timer_Descontar(void) {
    uint32_t i;
    for(i = 0; i < CANT_TIMER; i++) {
        if(Timer[i].contador > 0) {
            Timer[i].contador--;
            if(0 == Timer[i].contador) {
                Timer[i].flag_fin = 1;
                if(Timer[i].reload > 0)
                    Timer[i].contador = Timer[i].reload;
            }
        }
    }
}
