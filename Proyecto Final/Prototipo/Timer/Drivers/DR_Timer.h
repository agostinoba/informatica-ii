/*******************************************************************************
 *
 * @file       DR_Timer.h
 * @brief      Módulo con la funcion de Timer.
 * @version    1.00
 * @date       06/06/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef DRIVERS_DR_TIMER_H_
#define DRIVERS_DR_TIMER_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "Types.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

//0x400FC0C4UL : Registro de habilitación de dispositivos:
#define 	DIR_PCONP		( ( uint32_t  * ) 0x400FC0C4UL )
//0x400FC1A8UL : Registros de seleccion de los clks de los dispositivos:
#define		PCLKSEL			( ( uint32_t  * ) 0x400FC1A8UL )

//0x40004000UL : Direccion de inicio de los registros del Timer0
#define		DIR_TIMER0		( ( timer_t  * ) 0x40004000UL )
//0x40008000UL : Direccion de inicio de los registros del Timer1
#define		DIR_TIMER1		( ( timer_t  * ) 0x40008000UL )
//0x40090000UL : Direccion de inicio de los registros del Timer2
#define		DIR_TIMER2		( ( timer_t  * ) 0x40090000UL )
//0x40094000UL : Direccion de inicio de los registros del Timer3
#define		DIR_TIMER3		( ( timer_t  * ) 0x40094000UL )

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

// Registro PCONP (Power Control of Peripherals register):
#define		PCONP		DIR_PCONP[0]

// Registros PCLKSEL
#define		PCLKSEL0	PCLKSEL[0]
#define		PCLKSEL1	PCLKSEL[1]

#define		TIMER0		DIR_TIMER0[0]
#define		TIMER1		DIR_TIMER1[0]
#define		TIMER2		DIR_TIMER2[0]
#define		TIMER3		DIR_TIMER3[0]

#define		T0IR		TIMER0.IR
#define		T1IR		TIMER1.IR
#define		T2IR		TIMER2.IR
#define		T3IR		TIMER3.IR

#define		T0TCR		TIMER0.TCR
#define		T1TCR		TIMER1.TCR
#define		T2TCR		TIMER2.TCR
#define		T3TCR		TIMER3.TCR

#define		T0TC		TIMER0.TC
#define		T1TC		TIMER1.TC
#define		T2TC		TIMER2.TC
#define		T3TC		TIMER3.TC

#define		T0PR		TIMER0.PR
#define		T1PR		TIMER1.PR
#define		T2PR		TIMER2.PR
#define		T3PR		TIMER3.PR

#define		T0PC		TIMER0.PC
#define		T1PC		TIMER1.PC
#define		T2PC		TIMER2.PC
#define		T3PC		TIMER3.PC

#define		T0MCR		TIMER0.MCR
#define		T1MCR		TIMER1.MCR
#define		T2MCR		TIMER2.MCR
#define		T3MCR		TIMER3.MCR

#define		T0MR0		TIMER0.MR0
#define		T1MR0		TIMER1.MR0
#define		T2MR0		TIMER2.MR0
#define		T3MR0		TIMER3.MR0

#define		T0MR1		TIMER0.MR1
#define		T1MR1		TIMER1.MR1
#define		T2MR1		TIMER2.MR1
#define		T3MR1		TIMER3.MR1

#define		T0MR2		TIMER0.MR2
#define		T1MR2		TIMER1.MR2
#define		T2MR2		TIMER2.MR2
#define		T3MR2		TIMER3.MR2

#define		T0MR3		TIMER0.MR3
#define		T1MR3		TIMER1.MR3
#define		T2MR3		TIMER2.MR3
#define		T3MR3		TIMER3.MR3

#define		T0CCR		TIMER0.CCR
#define		T1CCR		TIMER1.CCR
#define		T2CCR		TIMER2.CCR
#define		T3CCR		TIMER3.CCR

#define		T0CR0		TIMER0.CR0
#define		T1CR0		TIMER1.CR0
#define		T2CR0		TIMER2.CR0
#define		T3CR0		TIMER3.CR0

#define		T0CR1		TIMER0.CR1
#define		T1CR1		TIMER1.CR1
#define		T2CR1		TIMER2.CR1
#define		T3CR1		TIMER3.CR1

// EXTERNAL MATCH REGISTER
#define		T0EMR		(*( ( uint32_t *  ) 0x4000403CUL ))
#define		T1EMR		(*( ( uint32_t *  ) 0x4000803CUL ))
#define		T2EMR		(*( ( uint32_t *  ) 0x4009003CUL ))
#define		T3EMR		(*( ( uint32_t *  ) 0x4009403CUL ))

// COUNT CONTROL REGISTER
#define		T0CTCR		(*( ( uint32_t *  ) 0x40004070UL ))
#define		T1CTCR		(*( ( uint32_t *  ) 0x40008070UL ))
#define		T2CTCR		(*( ( uint32_t *  ) 0x40090070UL ))
#define		T3CTCR		(*( ( uint32_t *  ) 0x40094070UL ))

#define		CANT_TIMER			8

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/
typedef struct {			// Estructura para los registros
	uint32_t	IR;			// IR 	- INTERRUPT REGISTER
	uint32_t	TCR;		// TCR 	- TIMER CONTROL REGISTER
	uint32_t 	TC;			// TC 	- TIMER COUNTER REGISTER
	uint32_t 	PR;			// PR 	- PRESCALE REGISTER
	uint32_t 	PC;			// PC 	- PRESCALE COUNTER REGISTER
	uint32_t 	MCR;		// MCR 	- MATCH CONTROL REGISTER
	uint32_t	MR0;		// MR 	- MATCH REGISTER
	uint32_t	MR1;		// MR 	- MATCH REGISTER
	uint32_t	MR2;		// MR 	- MATCH REGISTER
	uint32_t	MR3;		// MR 	- MATCH REGISTER
	uint32_t	CCR;		// CCR 	- CAPTURE CONTROL REGISTER
	uint32_t	CR0;		// CR	- CAPTURE REGISTER
	uint32_t	CR1;		// CR	- CAPTURE REGISTER
} timer_t;

typedef struct {
	uint8_t		flag_fin;
	uint32_t	contador;
	uint32_t	reload;
	void 		(*funcion)(void);
}STRUCT_TIMER;

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/
extern volatile STRUCT_TIMER Timer[CANT_TIMER];

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void Timer_Init(void)
 * @brief		Inicializa el Timer
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Timer_Init(void);

/*******************************************************************************
 * @fn			void Timer_Descontar(void)
 * @brief		Función de contador descendente de los timers.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Timer_Descontar (void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* DRIVERS_DR_TIMER_H_ */
