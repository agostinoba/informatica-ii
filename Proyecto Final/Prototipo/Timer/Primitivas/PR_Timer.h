/*******************************************************************************
 *
 * @file       PR_Timer.h
 * @brief      Módulo  con las funciones primitivas para el uso del Timer.
 * @version    1.00
 * @date       06/06/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef PRIMITIVAS_PR_TIMER_H_
#define PRIMITIVAS_PR_TIMER_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "Types.h"
#include <Drivers/DR_Timer.h>

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void Timer_Start(uint32_t, uint32_t, uint32_t, void *)
 * @brief		Comienza a funcionar el Timer con funciones que cree para
 *              interrumpir periodicamente
 * @param[in]	N_timer:	Número del Timer que voy a utilizar.
 * @param[in]	tiempo:	Tiempo inicial hasta que interrumpe por primera vez
 * @param[in]	recarga:	Tiempo en el cuál se va a interrumpir periodicamente
 * @param[in]	FUNCION:	Función a la que se llama en la interrupción
 * @return		void
 ******************************************************************************/
void Timer_Start (uint32_t N_timer, uint32_t tiempo,
                  uint32_t recarga, void (*) (void));

/*******************************************************************************
 * @fn			void Timer_Stop(uint32_t N_timer)
 * @brief		Detiene una de las interrupciones
 * @param[in]	N_timer:	Número del Timer que voy a utilizar.
 * @return		void
 ******************************************************************************/
void Timer_Stop(uint32_t N_timer);

/*******************************************************************************
 * @fn			void Timer_Reset(void)
 * @brief		Función de reset de los Timer
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Timer_Reset(void);

/*******************************************************************************
 * @fn			void Timer_Analizar(void)
 * @brief		Llama a la función correspondiente según corresponda por
 *              la interrupción
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void Timer_Analizar (void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* PRIMITIVAS_PR_TIMER_H_ */
