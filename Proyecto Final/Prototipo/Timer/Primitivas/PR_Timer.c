/*******************************************************************************
 *
 * @file       PR_Timer.c
 * @brief      Módulo  con las funciones primitivas para el uso del Timer.
 * @version    1.00
 * @date       06/06/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#include <Primitivas/PR_Timer.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void Timer_Start(uint32_t N_timer, uint32_t tiempo,
                 uint32_t recarga, void (*Funcion_Ejecutar) (void)) {
    // Checkea que no te excedas en la cantidad de timer que puedo tener
    if (N_timer < CANT_TIMER) {
        // Flag_fin me indica si se termino de contar
        Timer[N_timer].flag_fin = 0;
        // Tiempo seteado al que interrumpe
        Timer[N_timer].contador = tiempo;
        // Valor al cuál interrumpe después de la primera vez
        Timer[N_timer].reload = recarga;
        // Función que se ejecuta cuando contador vale 0
        Timer[N_timer].funcion = Funcion_Ejecutar;
    }
}

void Timer_Stop(uint32_t N_timer) {
    if (N_timer < CANT_TIMER) {
        Timer[N_timer].contador = 0;
        Timer[N_timer].reload = 0;
        Timer[N_timer].flag_fin = 0;
        Timer[N_timer].funcion = NULL;
    }
}

void Timer_Reset(void) {
	T0TCR = 2;	//
	T0TCR = 0;	//
	T0TCR = 1;	//
}

void Timer_Analizar(void) {
    uint32_t i;
    for (i = 0; i < CANT_TIMER; i++) {
        if (Timer[i].flag_fin > 0) {
            if(Timer[i].funcion != 0)
                (*Timer[i].funcion)(); // Puedo agregar más funciones
            Timer[i].flag_fin = 0;
        }
    }
}
