/*******************************************************************************
 *
 * @file       ADC.c
 * @brief      Módulo con las funciones primitivas del ADC.
 * @version    1.00
 * @date       05/10/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "Primitivas/PR_ADC.h"

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/
const int32_t Tabla_NTC[] = {
		3887, 3828, 3756, 3670, 3568, 3449, 3314, 3163,
		2997, 2818, 2630, 2437, 2241, 2048, 1859, 1678,
		1524, 1349, 1203, 1070, 950, 842, 746, 661, 585,
		519, 460, 408, 363, 324, 289, 257, 231
};

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
int32_t TablaNTC(int32_t dato) {
	int32_t j;
	for(j = 0; j < 33; j++)	{
		if(dato > Tabla_NTC[j])
			break;
	}

	dato =  (((Tabla_NTC[j-1] - dato) * 50) / (Tabla_NTC[j-1] - Tabla_NTC[j]));
	dato = dato + 50 * (j - 1) - 400;

	if(dato < -400)
		dato = -400;

	if(dato > 1200)
		dato = 1200;

	return dato;
}

int32_t ADC_Promedio(uint8_t canal) {
	int32_t promedio = 0;
	int32_t i;

	switch(canal) {

		case CANAL1:
			for(i = 0; i < MAX_MUESTRAS; i++)
				promedio += valorADC1[i];
			promedio /= MAX_MUESTRAS;
			break;

		case CANAL2:
			for(i = 0; i < MAX_MUESTRAS; i++)
				promedio += valorADC2[i];
			promedio /= MAX_MUESTRAS;
			break;

		case CANAL5:
			for (i = 0; i < MAX_MUESTRAS; i++)
				promedio += valorADC5[i];
			promedio /= MAX_MUESTRAS;
			break;

		default:
			promedio = -1;
			break;
	}

	return promedio;
}

int32_t Temperatura(void) {
	int32_t cuentas;

	cuentas = ADC_Promedio(CANAL1);
	cuentas = TablaNTC(cuentas);
	return cuentas;
}

int32_t ADC_Externa(void) {
	int32_t cuentas;

	cuentas = ADC_Promedio(CANAL2);

	return cuentas;
}

int32_t Potenciometro(void) {
	int32_t cuentas;

	cuentas = ADC_Promedio(CANAL5);

	cuentas = (3300 * cuentas) / 4096;
	// 4096 es la cantidad de cuentas, 2^12 (porque son 12 bits) 4096 es el
	// numero maximo que puedo tener
	// 3300 es por la tension max 3.3V
	// Entonces lo multiplico por 3300 para saber que tension tengo
	return cuentas;
}
