/*******************************************************************************
 *
 * @file       DR_ADC.h
 * @brief      Módulo con las funciones del ADC.
 * @version    1.00
 * @date       05/10/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef DRIVERS_DR_ADC_H_
#define DRIVERS_DR_ADC_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <Drivers/DR_GPIO.h>
#include "Types.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

//0x400FC0C4UL : Registro de habilitación de dispositivos:
#define DIR_PCONP        ( ( uint32_t  * ) 0x400FC0C4UL )
//0x400FC1A8UL : Registros de seleccion de los clks de los dispositivos:
#define PCLKSEL          ( ( uint32_t  * ) 0x400FC1A8UL )

//0xE000E100UL : Registros de habilitación (set) de interrup. en el NVIC:
#define ISER             ( ( uint32_t  * ) 0xE000E100UL )

//0x40034000UL: Registro de control del ADC:
#define DIR_AD0CR        ( ( uint32_t  * ) 0x40034000UL )
//0x40034030UL: Registro de Status del ADC:
#define DIR_AD0STAT      ( ( uint32_t  * ) 0x40034030UL )
//0x40034004UL: Registro de estado del ADC:
#define DIR_AD0GDR       ( ( uint32_t  * ) 0x40034004UL )
//0x4003400CUL: Registro de interrupcion del ADC
#define DIR_AD0INTEN     ( ( uint32_t  * ) 0x4003400CUL )
//0x40034010UL: Registros de estado de los ADCx
#define AD0DR            ( ( uint32_t  * ) 0x40034010UL )

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

// Registro PCONP:
#define		PCONP			DIR_PCONP[0]

// Registros PCLKSEL
#define		PCLKSEL0		PCLKSEL[0]
#define		PCLKSEL1		PCLKSEL[1]

#define		AD0CR			DIR_AD0CR[0]
#define		AD0STAT		DIR_AD0STAT[0]
#define		AD0GDR			DIR_AD0GDR[0]
#define		AD0INTEN		DIR_AD0INTEN[0]
#define		AD1INTEN		DIR_AD1INTEN[1]
#define		AD2INTEN		DIR_AD2INTEN[2]

#define		AD0DR0			AD0DR[0]
#define		AD0DR1			AD0DR[1]
#define		AD0DR2			AD0DR[2]
#define		AD0DR3			AD0DR[3]
#define		AD0DR4			AD0DR[4]
#define		AD0DR5			AD0DR[5]
#define		AD0DR6			AD0DR[6]
#define		AD0DR7			AD0DR[7]

// Registros de Nvic:
#define		ISER0			ISER[0]
#define		ISER1			ISER[1]

#define		PORT0			0
#define		PORT1			1

#define 		ADC1			PORT0,24 // termistor
#define 		ADC2			PORT0,25 // entrada libre
#define 		ADC5			PORT1,31 // es el pote

#define		CANAL1			1
#define		CANAL2			2
#define		CANAL5			5
#define		MAX_MUESTRAS	50

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/
extern volatile int32_t valorADC1[];
extern volatile int32_t valorADC2[];
extern volatile int32_t valorADC5[];

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void ADC_Inicializar(void)
 * @brief		Función de inicialización del ADC
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void ADC_Inicializar(void);

/*******************************************************************************
 * @fn			void ADC_IRQHandler(void)
 * @brief		Función de interrupción del ADC
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void ADC_IRQHandler(void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* DRIVERS_DR_ADC_H_ */
