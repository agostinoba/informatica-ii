/*******************************************************************************
 *
 * @filei      DR_ADC.c
 * @brief      Módulo con las funciones del ADC.
 * @version    1.00
 * @date       05/10/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "Drivers/DR_ADC.h"

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/
volatile int32_t valorADC1[MAX_MUESTRAS];
volatile int32_t valorADC2[MAX_MUESTRAS];
volatile int32_t valorADC5[MAX_MUESTRAS];

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
volatile static uint8_t indice_muestra = 0;

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void ADC_Inicializar(void) {

	//  Activo el modulo de ADC (Pag. 64)
	PCONP		|= (1 << 12);
	// Selecciono el clock del ADC - 100MHz/4 = 25MHz (Pag. 57 y 58)
	PCLKSEL0	&= ~(3<<24);
	// Selecciono divisor del clock como 1, para muestrear a 200kHz (Pag. 586)
	AD0CR		|= 0x00000100;

	GPIO_Pinsel(ADC1, PINSEL_FUNC1);
	GPIO_FioDir(ADC1, ENTRADA);
	GPIO_PinMode(ADC1, PINMODE_NONE);

	GPIO_Pinsel(ADC2, PINSEL_FUNC1);
	GPIO_FioDir(ADC2, ENTRADA);
	GPIO_PinMode(ADC2, PINMODE_NONE);

	GPIO_Pinsel(ADC5, PINSEL_FUNC3);
	GPIO_FioDir(ADC5, ENTRADA);
	GPIO_PinMode(ADC5, PINMODE_NONE);

	// Deactivo las interrupciones
	AD0INTEN	&= 0xFFFFFE00;
	// Activo las interrupciones de los canales 1 - 2 - 5 (Pag. 587)
	AD0INTEN	|= 0x00000026;

	// Selecciono el canal del que voy a tomar muestras - canal 1 (Pag. 586)
	AD0CR		|=  0x00000002;
	// PDN Operational
	AD0CR		|=  (1 << 21);
	// No Burst (0 = No, 1 = Si)
	AD0CR		&= ~(1 << 16);
	// Limpio el registro de Start
	AD0CR		&= ~(0x0F << 24);
	// Arranca la conversion (Start = 001)
	AD0CR		|=  (1 << 24);

	// Habilito la interrupcion del ADC Pag. 78
	// ISER es un registro que al poner un 1 en el bit de cada módulo,
	// habilito las interrupciones de lo que quiero. En este caso del ADC
	ISER0		|= (1 << 22);
}

void ADC_IRQHandler(void) {
	int32_t valor;			// Leo todo el registro
	int32_t finconv;		// Veo si termino la conversion
	int32_t resultado;		// Tengo el valor de la conversion en los bits 15:4

	static uint8_t indice_adc = 1;

	switch(indice_adc) {

		case CANAL1:
			valor	= AD0DR1;
			finconv	= (valor >> 31) & 0x01;
		    if(finconv) {
		    	resultado = (valor >>  4) & 0xFFF;
				// Guardo en un array para realizar un promedio de las
				// lecturas para disminuir el ruido
		    	valorADC1[indice_muestra] = resultado;
				indice_muestra++;
				// Asigna 0 a indice_muestra cuando sea igual a MAX_MUESTRAS
				indice_muestra %= MAX_MUESTRAS;
				// Con esto checkeo si se lleno el buffer de muestras.
				// Cuando se llena el buffer del adc1 paso al siguiente y asi
				if(!indice_muestra) {
					indice_adc = 2;			// Disparo el canal 2
					AD0CR &= (~0x07f);		// Limpio los canales
					AD0CR |= (1 << 2);		// Habilito el canal 2
					AD0CR |= (1 <<24);		// Arranca conversión START = 1;
				}
				else {						// Redisparo canal 1
					AD0CR &= (~0x07f);		// Limpio los canales
					AD0CR |= (1 << 1);		// Habilito el canal 1
					AD0CR |= (1 <<24);		// Arranca conversión START = 1;
				}
		    }
			break;

		case CANAL2:
			valor	= AD0DR2;
			finconv	= (valor >> 31) & 0x01;
		    if(finconv) {
		    	resultado = (valor >>  4) & 0xFFF;
				// Guardo en un array para realizar un promedio de las
				// lecturas para disminuir el ruido
				valorADC2[indice_muestra] = resultado;
				indice_muestra++;
				// Asigna 0 a indice_muestra cuando sea igual a MAX_MUESTRAS
				indice_muestra %= MAX_MUESTRAS;
				// Con esto checkeo si se lleno el buffer de muestras.
				// Cuando se llena el buffer del adc2 paso al siguiente y asi
				if(!indice_muestra) {
					indice_adc = 5;			// Disparo el canal 5
					AD0CR &= (~0x07f);		// Limpio los canales
					AD0CR |= (1 << 5);		// Habilito el canal 5
					AD0CR |= (1 <<24);		// Arranca conversión START = 1;
				}
				else {						// Redisparo canal 2
					AD0CR &= (~0x07f);		// Limpio los canales
					AD0CR |= (1 << 2);		// Habilito el canal 2
					AD0CR |= (1 <<24);		// Arranca conversión START = 1;
				}
		    }
			break;

		case CANAL5:
			valor	= AD0DR5;
			finconv	= (valor >> 31) & 0x01;
		    if(finconv) {
		    	resultado = (valor >>  4) & 0xFFF;
				valorADC5[indice_muestra] = resultado;
				// Guardo en un array para realizar un promedio de las
				// lecturas para disminuir el ruido
				indice_muestra++;
				// Asigna 0 a indice_muestra cuando sea igual a MAX_MUESTRAS
				indice_muestra %= MAX_MUESTRAS;
				// Con esto checkeo si se lleno el buffer de muestras.
				// Cuando se llena el buffer del adc2 paso al siguiente y asi
				if(!indice_muestra) {
					indice_adc = 1;			// Disparo el canal 1
					AD0CR &= (~0x07f);		// Limpio los canales
					AD0CR |= (1 << 1);		// Habilito el canal 1
					AD0CR |= (1 <<24);		// Arranca conversión START = 1;
				}
				else {						// Redisparo canal 2
					AD0CR &= (~0x07f);		// Limpio los canales
					AD0CR |= (1 << 5);		// Habilito el canal 5
					AD0CR |= (1 <<24);		// Arranca conversión START = 1;
				}
		    }
			break;

		default:
			break;
	}
}
