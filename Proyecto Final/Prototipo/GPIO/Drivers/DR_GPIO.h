/*******************************************************************************
*
* @file        GPIO.h
* @brief       Módulo para el seteo de puertos y pines.
* @version     1.00
* @date        25/04/2020
* @author      Agostino Barbetti
*
 ******************************************************************************/

/*******************************************************************************
*** MODULE
 ******************************************************************************/
#ifndef DRIVERS_DR_GPIO_H_
#define DRIVERS_DR_GPIO_H_

/*******************************************************************************
*** INCLUDES GLOBALES
 ******************************************************************************/
#include "Types.h"

/*******************************************************************************
*** DEFINES GLOBALES
 ******************************************************************************/

//0x2009C000UL : Registros de GPIOs
#define	GPIOs			 ( ( gpio_t	  * ) 0x2009C000UL )
//0x4002C000UL : Registros PINSEL
#define	PINSEL			 ( ( uint32_t  * ) 0x4002C000UL )
//0x4002C040UL : Registros de modo de los pines del GPIO
#define	PINMODE			 ( ( uint32_t  * ) 0x4002C040UL )
//0x4002C068UL : Registros de modo OD de los pines del GPIO
#define	PINMODEOD		 ( ( uint32_t  * ) 0x4002C068UL )

/*******************************************************************************
*** MACROS GLOBALES
 ******************************************************************************/

// Registros GPIOs:
#define		GPIO0		GPIOs[0]
#define		GPIO1		GPIOs[1]
#define		GPIO2		GPIOs[2]
#define		GPIO3		GPIOs[3]
#define		GPIO4		GPIOs[4]

// Registros PINSEL:
#define		PINSEL0		PINSEL[0]	//!< PINSEL0--->P0[15:0]	(0x4002C000)
#define		PINSEL1		PINSEL[1]	//!< PINSEL1--->P0[31:16]	(0x4002C004)
#define		PINSEL2		PINSEL[2]	//!< PINSEL2--->P1[15:0] 	(0x4002C008)
#define		PINSEL3		PINSEL[3]	//!< PINSEL3--->P1[31:16]	(0x4002C00C)
#define		PINSEL4		PINSEL[4]	//!< PINSEL4--->P2[15:0] 	(0x4002C010)
#define		PINSEL5		PINSEL[5]	//!< PINSEL5--->P2[31:16]	 NOT USED
#define		PINSEL6		PINSEL[6]	//!< PINSEL6--->P3[15:0]	 NOT USED
#define		PINSEL7		PINSEL[7]	//!< PINSEL7--->P3[31:16]	(0x4002C01C)
#define		PINSEL8		PINSEL[8]	//!< PINSEL8--->P4[15:0]	 NOT USED
#define		PINSEL9		PINSEL[9]	//!< PINSEL9--->P4[31:16]	(0x4002C024)

// Registros PINMODE
#define		PINMODE0	PINMODE[0]
#define		PINMODE1	PINMODE[1]
#define		PINMODE2	PINMODE[2]
#define		PINMODE3	PINMODE[3]
#define		PINMODE4	PINMODE[4]
#define		PINMODE5	PINMODE[5]
#define		PINMODE6	PINMODE[6]
#define		PINMODE7	PINMODE[7]
#define		PINMODE8	PINMODE[8]
#define		PINMODE9	PINMODE[9]

// Registros PINMODEOD
#define		PINMODEOD0	PINMODE[0]
#define		PINMODEOD1	PINMODE[1]
#define		PINMODEOD2	PINMODE[2]
#define		PINMODEOD3	PINMODE[3]
#define		PINMODEOD4	PINMODE[4]

#define		FIO0DIR		GPIO0.FIODIR
#define		FIO1DIR		GPIO1.FIODIR
#define		FIO2DIR		GPIO2.FIODIR
#define		FIO3DIR		GPIO3.FIODIR
#define		FIO4DIR		GPIO4.FIODIR

#define		FIO0MASK	GPIO0.FIOMASK
#define		FIO1MASK	GPIO1.FIOMASK
#define		FIO2MASK	GPIO2.FIOMASK
#define		FIO3MASK	GPIO3.FIOMASK
#define		FIO4MASK	GPIO4.FIOMASK

#define		FIO0CLR		GPIO0.FIOCLR
#define		FIO1CLR		GPIO1.FIOCLR
#define		FIO2CLR		GPIO2.FIOCLR
#define		FIO3CLR		GPIO3.FIOCLR
#define		FIO4CLR		GPIO4.FIOCLR

#define		FIO0SET		GPIO0.FIOSET
#define		FIO1SET		GPIO1.FIOSET
#define		FIO2SET		GPIO2.FIOSET
#define		FIO3SET		GPIO3.FIOSET
#define		FIO4SET		GPIO4.FIOSET

#define		FIO0PIN		GPIO0.FIOPIN
#define		FIO1PIN		GPIO1.FIOPIN
#define		FIO2PIN		GPIO2.FIOPIN
#define		FIO3PIN		GPIO3.FIOPIN
#define		FIO4PIN		GPIO4.FIOPIN

//			Estados de PINSEL:
#define	 	PINSEL_GPIO			0	   // Default: tipico puerto GPIO
#define		PINSEL_FUNC1		1      // Primera funcion alternativa
#define		PINSEL_FUNC2		2      // Segunda funcion alternativa
#define	 	PINSEL_FUNC3		3      // Tercera funcion alternativa

//			Estados del pin
#define 	BAJO 				0
#define		ALTO 				1

//			Estados de FIODIR:
#define		ENTRADA				0
#define		SALIDA				1

//			Estados de PINMODE
#define		PINMODE_PULLUP 		0
#define		PINMODE_REPEAT 		1
#define		PINMODE_NONE 		2
#define		PINMODE_PULLDOWN 	3

//			Estados de PINMODE_OD
#define		PINMODE_NOT_OD 		0
#define		PINMODE_OD	 		1

/*******************************************************************************
*** TIPO DE DATOS GLOBALES
 ******************************************************************************/
typedef struct {
	uint32_t	FIODIR;
	uint32_t	RESERVED[3];
	uint32_t 	FIOMASK;
	uint32_t 	FIOPIN;
	uint32_t 	FIOSET;
	uint32_t 	FIOCLR;
} gpio_t;

/*******************************************************************************
*** VARIABLES GLOBALES
 ******************************************************************************/

/*******************************************************************************
*** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void GPIO_Pinsel(uint32_t Port, uint32_t Pin, uint32_t Func)
 * @brief		Seteo la función del Pin
 * @param[in]	Port:		Número de Puerto
 * 	                        - PORT_0
 *                          - PORT_1
 *                          - PORT_2
 *                          - PORT_3
 * @param[in]	Pin:		Número de Pin
 *                          - Desde PIN_0 hasta PIN_31
 * @param[in]	Func:   	Función del Pin
 * 	                        - PINSEL_GPIO
 *                          - PINSEL_FUNC1
 *                          - PINSEL_FUNC2
 *                          - PINSEL_FUNC3
 * @return		void
 ******************************************************************************/
void GPIO_Pinsel(uint32_t Puerto, uint32_t Pin, uint32_t Funcion);

/*******************************************************************************
 * @fn			void GPIO_FioDir(uint32_t Port, uint32_t Pin, uint32_t Dir)
 * @brief		Seteo si va a ser un Pin de ENTRADA o SALIDA
 * @param[in]	Port:		Número de Puerto
 * 	                        - PORT_0
 *                          - PORT_1
 *                          - PORT_2
 *                          - PORT_3
 * @param[in]	Pin:		Número de Pin
 *                          - Desde PIN_0 hasta PIN_31
 * @param[in]	Dir:    	Dirección del Pin
 * 	                        - ENTRADA
 * 	                        - SALIDA
 * @return		void
 ******************************************************************************/
void GPIO_FioDir(uint32_t Puerto, uint32_t Pin, uint32_t Direccion);

/*******************************************************************************
 * @fn			void GPIO_PinMode(uint32_t Port, uint32_t Pin, uint32_t Mode)
 * @brief		Seteo el modo de la resistencia asociada al Pin
 * @param[in]	Port		Número de Puerto: Debe ser uno de los siguientes:
 *                          - PORT_0
 *                          - PORT_1
 *                          - PORT_2
 *                          - PORT_3
 * @param[in]	Pin 		Número de Pin
 *                          - Desde PIN_0 hasta PIN_31
 *                          Nota: Los pines P0[27] a P0[30] no pueden ser
 *                          seteados ya que	no tiene resistencia de pull-up
 *                          o pull-donw.
 * @param[in]	Mode:		Modo de la Resistencia
 * 							- PINMODE_PULLUP
 * 							- PINMODE_REPEAT
 * 							- PINMODE_NONE
 * 							- PINMODE_PULLDOWN
 * @return		void
 ******************************************************************************/
void GPIO_PinMode(uint32_t Puerto, uint32_t Pin, uint32_t Modo);

/*******************************************************************************
 * @fn			void GPIO_PinModeOD(uint32_t Port, uint32_t Pin, uint32_t OD)
 * @brief		Seteo el modo de la salida asociada al Pin
 * @param[in]	Port		Número de Puerto: Debe ser uno de los siguientes:
 *                          - PORT_0
 *                          - PORT_1
 *                          - PORT_2
 *                          - PORT_3
 * @param[in]	Pin 		Número de Pin
 *                          - Desde PIN_0 hasta PIN_31
 *                          Nota: Los pines P0[27] a P0[28] no pueden ser
 *                          seteados ya que	siempre son open-drain.
 * @param[in] 	OD  		Modo de la Salida
 * 							- PINMODE_NOT_OD
 * 							- PINMODE_OD
 * @return		void
 ******************************************************************************/
void GPIO_PinModeOD(uint32_t Puerto, uint32_t Pin, uint32_t ODMode);

/*******************************************************************************
 * @fn			void GPIO_Set(uint32_t Port, uint32_t Pin, uint32_t Set)
 * @brief		Seteo un valor en el Pin
 * @param[in]	Port:		Número de Puerto
 * 	                        - PORT_0
 *                          - PORT_1
 *                          - PORT_2
 *                          - PORT_3
 * @param[in]	Pin:		Número de Pin
 *                          - Desde PIN_0 hasta PIN_31
 * @param[in]	Set:		Valor que seteo:
 * 							- ALTO
 * 							- BAJO
 * @return		void
 ******************************************************************************/
void GPIO_Set(uint32_t Puerto, uint32_t Pin, uint32_t Set);

/*******************************************************************************
 * @fn			uint32_t GPIO_Get(uint32_t Port, uint32_t Pin)
 * @brief		Obtengo el valor en el Pin
 * @param[in]	Port:		Número de Puerto
 * 	                        - PORT_0
 *                          - PORT_1
 *                          - PORT_2
 *                          - PORT_3
 * @param[in]	Pin:		Número de Pin
 *                          - Desde PIN_0 hasta PIN_31
 * @return		Valor del Pin
 ******************************************************************************/
uint32_t GPIO_Get(uint32_t Puerto, uint32_t Pin);

/*******************************************************************************
 * @fn			uint32_t GPIO_PinToggle(uint32_t Port, uint32_t Pin)
 * @brief		Invierte el valor en el Pin
 * @param[in]	Port:		Número de Puerto
 * 	                        - PORT_0
 *                          - PORT_1
 *                          - PORT_2
 *                          - PORT_3
 * @param[in]	Pin:		Número de Pin
 *                          - Desde PIN_0 hasta PIN_31
 * @return		void
 ******************************************************************************/
void GPIO_PinToggle(uint32_t Puerto, uint32_t Pin);

/*******************************************************************************
*** MODULE END
 ******************************************************************************/
#endif /* DRIVERS_DR_GPIO_H_ */
