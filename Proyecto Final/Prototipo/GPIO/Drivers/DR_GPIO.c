/*******************************************************************************
 *
 * @file       GPIO.c
 * @brief      Implementación del Módulo para el seteo de los GPIOs.
 * @version    1.00
 * @date       25/04/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <Drivers/DR_GPIO.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void GPIO_Pinsel(uint32_t Puerto, uint32_t Pin, uint32_t Funcion) {
	// Primero fijo los ceros y despues fijo los 1 donde haga falta
	PINSEL[(Puerto * 2) + (Pin / 16)] &= ~(3 << (Pin % 16) * 2);
	PINSEL[(Puerto * 2) + (Pin / 16)] |= (Funcion << (Pin % 16) * 2);
}

void GPIO_FioDir(uint32_t Puerto, uint32_t Pin, uint32_t Direccion) {
	// Setea la dirección del PIN para que sea de ENTRADA o de SALIDA
	if(SALIDA == Direccion)
		GPIOs[Puerto].FIODIR |= (1 << Pin);
	else
		GPIOs[Puerto].FIODIR &= ~(0 << Pin);
}

void GPIO_PinMode(uint32_t Puerto, uint32_t Pin, uint32_t Modo) {
	//Primero fijo los ceros y despues fijo los 1 donde haga falta
	PINMODE[(Puerto * 2) + (Pin / 16)] &= ~(3 << (Pin % 16) * 2);
	PINMODE[(Puerto * 2) + (Pin / 16)] |= (Modo << (Pin % 16) * 2);
	// 00 - Pull-up resistor enabled
	// 01 - Repeater mode enabled
	// 10 - Neither Pull-up nor Pull-down
	// 11 - Pull-down resistor enabled
}

void GPIO_PinModeOD(uint32_t Puerto, uint32_t Pin, uint32_t ODMode) {
	//Primero fijo los ceros y despues fijo los 1 donde haga falta
	PINMODE[(Puerto * 2) + (Pin / 16)] &= ~(1 << (Pin % 16) * 2);
	PINMODE[(Puerto * 2) + (Pin / 16)] |= (ODMode << (Pin % 16) * 2);
	// 0 - Not Open Drain
	// 1 - Open Drain
}

void GPIO_Set(uint32_t Puerto, uint32_t Pin, uint32_t Estado) {
	if(ALTO == Estado)
		GPIOs[Puerto].FIOSET = (1 << Pin);
	else
		GPIOs[Puerto].FIOCLR = (1 << Pin);
}

uint32_t GPIO_Get(uint32_t Puerto, uint32_t Pin) {
	return (GPIOs[Puerto].FIOPIN & (1 << Pin)) ? 1 : 0;
}

void GPIO_PinToggle(uint32_t Puerto, uint32_t Pin) {
	GPIO_Set(Puerto, Pin, !(GPIO_Get(Puerto, Pin)));
}
