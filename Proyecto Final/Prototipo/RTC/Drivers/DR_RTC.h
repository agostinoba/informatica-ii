/*******************************************************************************
 *
 * @file       DR_RTC.h
 * @brief      Módulo para setear el Real Time Clock.
 * @version    1.00
 * @date       18/10/2020
 * @author     Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef DRIVERS_DR_RTC_H_
#define DRIVERS_DR_RTC_H_

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "Types.h"

/*******************************************************************************
 *** DEFINES GLOBALES
 ******************************************************************************/

//0x400FC0C4UL : Registro de habilitación de dispositivos:
#define DIR_PCONP	( ( uint32_t  * ) 0x400FC0C4UL )

#define CCRRTC	  	(*((uint32_t *) 0x40024008UL))

#define CIIR 		(*((uint32_t *) 0x4002400CUL))

#define RTC_AUX 	(*((uint32_t *) 0x4002405CUL))

#define CTIME0 	(*((uint32_t *) 0x40024014UL))

#define SEC 		(*((uint32_t *) 0x40024020UL))

#define MIN		(*((uint32_t *) 0x40024024UL))

#define HOUR		(*((uint32_t *) 0x40024028UL))

/*******************************************************************************
 *** MACROS GLOBALES
 ******************************************************************************/

// Registro PCONP:
#define		PCONP		DIR_PCONP[0]

/*******************************************************************************
 *** TIPO DE DATOS GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/

/*******************************************************************************
 *** PROTOTIPOS DE FUNCIONES
 ******************************************************************************/

/*******************************************************************************
 * @fn			void RTC_Inicializar(void)
 * @brief		Inicializa y activa el RTC.
 * @param[in]	void
 * @return		void
 ******************************************************************************/
void RTC_Inicializar(void);

/*******************************************************************************
 * @fn			void RTC_Set_Hora(uint32_t hora)
 * @brief		Escribe la hora en el módulo RTC.
 * @param[in]	hora:	Hora que se le asignará al RTC
 * @return		void
 ******************************************************************************/
void RTC_Set_Hora(uint32_t hora);

/*******************************************************************************
 * @fn			void RTC_Set_Minutos(uint32_t minutos)
 * @brief		Escribe los minutos en el módulo RTC.
 * @param[in]	minutos:	Minutos que se le asignará al RTC
 * @return		void
 ******************************************************************************/
void RTC_Set_Minutos(uint32_t minutos);

/*******************************************************************************
 * @fn			void RTC_Set_Segundos(uint32_t segundos)
 * @brief		Escribe los segundos en el módulo RTC.
 * @param[in]	segundos:	Segundos que se le asignará al RTC
 * @return		void
 ******************************************************************************/
void RTC_Set_Segundos(uint32_t segundos);

/*******************************************************************************
 * @fn			uint32_t RTC_Get_Hora(void)
 * @brief		Obtiene la hora del RTC.
 * @param[in]	void
 * @return		uint32_t
 ******************************************************************************/
uint32_t RTC_Get_Hora(void);

/*******************************************************************************
 * @fn			uint32_t RTC_Get_Minutos(void)
 * @brief		Obtiene los minutos del RTC.
 * @param[in]	void
 * @return		uint32_t
 ******************************************************************************/
uint32_t RTC_Get_Minutos(void);

/*******************************************************************************
 * @fn			uint32_t RTC_Get_Segundos(void)
 * @brief		Obtiene los segundos del RTC.
 * @param[in]	void
 * @return		uint32_t
 ******************************************************************************/
uint32_t RTC_Get_Segundos(void);

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif /* DRIVERS_DR_RTC_H_ */
