/*******************************************************************************
 *
 * @file                 config.h
 * @brief                Módulo para setear una configuración.
 * @version              1.00
 * @date                 15/10/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef CONFIG_H
#define CONFIG_H

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <QDialog>
#include <QTime>

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
namespace Ui {
class config;
}

class config : public QDialog
{
    Q_OBJECT

public:
    explicit config(QWidget *parent = nullptr);
    ~config();
    void SetTempAmb(int);
    void SetTempAmbHoraInic(QTime);
    void SetTempAmbHoraFin(QTime);
    void SetIlumHoraInic(QTime);
    void SetIlumHoraFin(QTime);
    void SetRTC(QTime);

    int GetTempAmb();
    QTime GetTempAmbHoraInic();
    QTime GetTempAmbHoraFin();
    QTime GetIlumHoraInic();
    QTime GetIlumHoraFin();
    QTime GetRTC();

    QString GuardarConfiguracion();
    void CargarConfiguracion();
    void EnviarConfiguracion(QString);

    void setCheck(bool);
    bool getCheck() const;

    private slots:
        void on_pushButton_Aceptar_clicked();

        void on_pushButton_Cerrar_clicked();

private:
    Ui::config *ui;
    bool check_correcto;
    int temperatura_ambiente;
    QTime temp_amb_hora_inicio;
    QTime temp_amb_hora_fin;
    QTime iluminacion_hora_inicio;
    QTime iluminacion_hora_fin;
    QTime RTC;
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // CONFIG_H
