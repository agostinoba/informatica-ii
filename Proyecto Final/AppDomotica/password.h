/*******************************************************************************
 *
 * @file                 password.h
 * @brief                Ventana para cambiar la contraseña.
 * @version              1.00
 * @date                 15/10/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef PASSWORD_H
#define PASSWORD_H

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <QDialog>

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
namespace Ui {
class Password;
}

class Password : public QDialog
{
    Q_OBJECT

public:
    explicit Password(QWidget *parent = nullptr);
    ~Password();


    void setUsername(QString);
    void setPassword(QString);
    QString getUsername() const;
    QString getPassword() const;

private slots:
    void on_pushButton_Cambiar_clicked();

private:
    Ui::Password *ui;
    QString username_seteado;
    QString password_seteado;
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // PASSWORD_H
