/*******************************************************************************
 *
 * @file                 historial.cpp
 * @brief                Módulo para ver las configuraciones seteadas.
 * @version              1.00
 * @date                 15/10/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "historial.h"
#include "ui_historial.h"
#include <QFile>
#include <QTextStream>

/*******************************************************************************
*** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define PATH_HISTORIAL "/home/agostino/Documents/UTN/config.txt"

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
historial::historial(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::historial) {
    ui->setupUi(this);
    QFile file(PATH_HISTORIAL);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream textoConfig(&file);
    QString line = "";
    while(!textoConfig.atEnd()) {
        QString linea = textoConfig.readLine();
        QStringList fields = linea.split("|");
        line += fields[0] + " | ";
        line += QString(" T. Amb: ");
        line += fields[1];
        line += QString(" | Calef - H. inic: ");
        line += fields[2];
        line += QString(", H. fin: ");
        line += fields[3];
        line += QString(" | Ilum - H. inic: ");
        line += fields[4];
        line += QString(", H. fin: ");
        line += fields[5];
        line += "\n";
    }
    file.close();
    ui->ListaHistorial->setText(line);
}

historial::~historial() {
    delete ui;
}
