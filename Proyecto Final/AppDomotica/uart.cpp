/*******************************************************************************
 *
 * @file                 uart.cpp
 * @brief                Módulo para la comunicación serie mediante UART.
 * @version              1.00
 * @date                 15/10/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "uart.h"

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/
QextSerialPort *Uart = new QextSerialPort();
static volatile char Buffer_UART[1024];
static volatile int Index_Push = 0, Index_Pop = 0;
static int Pop_Data(char *Data);

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
void Push_Data(char Data) {
    Buffer_UART[Index_Push] = Data;
    Index_Push++;
    return;
}

static int Pop_Data(char * Data) {
    if(Index_Pop < Index_Push) {
        *Data = Buffer_UART[Index_Pop];
        Index_Pop++;
        printf("%c", *Data);
        return 1;
    }
    else
        return 0;
}

int Send_Data(void) {
    char Data = 0;
    if(Pop_Data(&Data)) {
        Uart->putChar(Data);
        return 1;
    }
    else
        return 0;
}
