/*******************************************************************************
 *
 * @file                 mainwindow.cpp
 * @brief                Ventana principal de la aplicación de Domótica.
 * @version              1.00
 * @date                 04/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "historial.h"
#include "config.h"
#include "uart.h"
#include "login.h"
#include "password.h"
#include "qextserialenumerator.h"
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QVector>
#include <QDebug>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include "qextserialport.h"
#include <QtSerialPort/qserialportinfo.h>

/*******************************************************************************
*** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define PATH_HISTORIAL "/home/agostino/Documents/UTN/config.txt"

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/
extern QextSerialPort *Uart;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {
    ui->setupUi(this);
    this->psw = new Password(this);
    this->psw->setUsername("info");
    this->psw->setPassword("123");
    this->psw->setModal(true);
    log = new Login(this, this->psw);
    log->setWindowFlags(Qt::Window
                        | Qt::WindowTitleHint
                        | Qt::CustomizeWindowHint);
    this->CargarConfiguracion();
    nombrePuerto = "";
    connect(Uart, SIGNAL(readyRead()), this, SLOT(LeerUART()));
    TraerPuertos();
    ui->pushButton_Configuracion->setDisabled(1);
    ui->pushButton_Historial->setDisabled(1);
    ui->pushButton_Password->setDisabled(1);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_pushButton_Configuracion_clicked() {
    log->setModal(true);
    log->exec();
    if(log->getLog()) {
        conf = new config(this);
        conf->setWindowFlags(Qt::Window
                             | Qt::WindowTitleHint
                             | Qt::CustomizeWindowHint);
        conf->setModal(true);
        conf->exec();
        if((conf != NULL) && (conf->getCheck())) {
            ui->lblValorTempAmbSeteada
                ->setText(QString::number(this->conf->GetTempAmb()) + " C");
            ui->VentHoraInic->setTime(this->conf->GetTempAmbHoraInic());
            ui->VentHoraFin->setTime(this->conf->GetTempAmbHoraFin());
            ui->IlumHoraInic->setTime(this->conf->GetIlumHoraInic());
            ui->IlumHoraFin->setTime(this->conf->GetIlumHoraFin());
        }
    }
    else {
        log->hide();
    }
}

void MainWindow::on_pushButton_Historial_clicked() {
QWidget *history = new historial();
history->show();
}

void MainWindow::on_pushButton_Conectar_clicked() {
    if(!Uart->isOpen()) {
        Uart->setBaudRate(BAUD115200);
        Uart->setDataBits(DATA_8);
        Uart->setStopBits(STOP_1);
        Uart->setParity(PAR_NONE);
        Uart->setFlowControl(FLOW_OFF);

        Uart->setPortName(nombrePuerto);

        if(Uart->open(QextSerialPort::ReadWrite)) {
            Uart->write(("<"
                         + QTime::currentTime().toString("HH:mm:ss")
                         + "|||||>").toUtf8().constData(), 15);
            ui->pushButton_Configuracion->setDisabled(0);
            ui->pushButton_Historial->setDisabled(0);
            ui->pushButton_Password->setDisabled(0);
            ui->pushButton_Conectar->setText("Desconectar");
        }
        else {
            QMessageBox::critical(this, "Error",
             "No se puede abrir el puerto " + nombrePuerto);
        }
    }
    else {
        Uart->close();
        ui->pushButton_Conectar->setText("Conectar");
        ui->pushButton_Configuracion->setDisabled(1);
        ui->pushButton_Historial->setDisabled(1);
        ui->pushButton_Password->setDisabled(1);
    }
}

void MainWindow::on_comboBox_Conexiones_currentIndexChanged(int index) {
    nombrePuerto = ui->comboBox_Conexiones->itemText(index);
}

void MainWindow::TraerPuertos() {
    ui->comboBox_Conexiones->clear();
    QList<QSerialPortInfo> puertos = QSerialPortInfo::availablePorts();

    for(int i = 0; i < puertos.size(); i++)
        ui->comboBox_Conexiones->addItem(puertos.at(i).portName());
}

void MainWindow::CargarConfiguracion() {
    if(QFile::exists(PATH_HISTORIAL)) {
        QStringList model;
        QFile file(PATH_HISTORIAL);
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream textoConfig(&file);
        while(!textoConfig.atEnd()) {
            QString line = textoConfig.readLine();
            QStringList fields = line.split("|");
            model.append(fields);
        }
        file.close();
    }
}

void MainWindow::LeerUART() {
        QByteArray data = Uart->readAll();
        DesentramarUART(QString::fromStdString(data.toStdString()));
}

void MainWindow::DesentramarUART(QString trama) {
    QStringList valores = trama.split(">");

    if(valores.size() != 1) {
        if(valores.count() >= 1) {
            if(valores[0].length() == 7) {
                QString temp_amb_actual = valores[0];
                QString temperatura_ambiente = temp_amb_actual.mid(2, 4);
                char VerificarTrama = temp_amb_actual.toStdString()[1];
                if('E' == VerificarTrama) {
                    if(!temperatura_ambiente.isEmpty()
                       && !temperatura_ambiente.isNull()) {
                       ui->lblValorTempAmbActual->setText(temperatura_ambiente);
                    }
                }
            }
            else if(valores[0].length() == 5) {
                QString contrasena = valores[0];
                QString psw_mem = contrasena.mid(2, 4);
                char VerificarTrama = contrasena.toStdString()[1];
                if('P' == VerificarTrama) {
                    if(!psw_mem.isEmpty() && !psw_mem.isNull()) {
                        this->psw->setPassword(psw_mem);
                    }
                }
            }
        }
    }
}

void MainWindow::on_pushButton_Password_clicked() {
    this->psw->exec();
}

