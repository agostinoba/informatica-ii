/*******************************************************************************
 *
 * @file                 login.h
 * @brief                Módulo para loguearse previo al menú de configuración.
 * @version              1.00
 * @date                 15/10/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef LOGIN_H
#define LOGIN_H

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <QDialog>
#include <password.h>

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
namespace Ui {
class Login;
}

class Login : public QDialog
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = nullptr);
    Login(QWidget *parent, Password*password);
    ~Login();

    void setLog(bool);
    bool getLog() const;
    void Limpiar_valores(void);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_Cerrar_clicked();

private:
    Ui::Login *ui;
    bool log_correcto;
    Password *psw;
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // LOGIN_H
