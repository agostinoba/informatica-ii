/*******************************************************************************
 *
 * @file                 login.cpp
 * @brief                Módulo para loguearse previo al menú de configuración.
 * @version              1.00
 * @date                 15/10/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "login.h"
#include "ui_login.h"
#include "mainwindow.h"
#include "password.h"
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QVector>
#include <QDebug>

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
Login::Login(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Login) {
    ui->setupUi(this);
}

Login::Login(QWidget *parent, Password*password) :
    QDialog(parent),
    ui(new Ui::Login) {
    ui->setupUi(this);
    psw = password;
}

Login::~Login() {
    delete ui;
}

void Login::setLog(bool aux) {
   this->log_correcto = aux;
}

bool Login::getLog() const {
    return this->log_correcto;
}

void Login::on_pushButton_clicked() {
    QString username = ui->lineEdit_username->text();
    QString password = ui->lineEdit_password->text();

    if(psw->getUsername() == username && psw->getPassword() == password) {
        Limpiar_valores();
        this->setLog(true);
        this->hide();
    }
    else {
        QMessageBox::warning(this, "Login",
         "El usuario o contraseña son incorrectos");
    }
}

void Login::on_pushButton_Cerrar_clicked() {
    Limpiar_valores();
    this->setLog(false);
    this->hide();
}

void Login::Limpiar_valores(void) {
    ui->lineEdit_username->clear();
    ui->lineEdit_password->clear();
}
