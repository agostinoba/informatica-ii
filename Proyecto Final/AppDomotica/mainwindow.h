/*******************************************************************************
 *
 * @file                 mainwindow.h
 * @brief                Ventana principal de la aplicación de Domótica.
 * @version              1.00
 * @date                 04/07/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <config.h>
#include <historial.h>
#include <login.h>
#include <password.h>

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        MainWindow(QWidget *parent = nullptr);
        ~MainWindow();
        void CargarConfiguracion();


    public slots:
        void LeerUART();

    private slots:
        void on_pushButton_Configuracion_clicked();
        void on_pushButton_Historial_clicked();
        void on_pushButton_Conectar_clicked();
        void on_comboBox_Conexiones_currentIndexChanged(int index);

        void on_pushButton_Password_clicked();

private:
        Ui::MainWindow *ui;
        config *conf;
        Login *log;
        Password *psw;
        QString nombrePuerto;
        void TraerPuertos();
        void ProcesarTrama(QByteArray);
        void SetearPuertoSerie();
        void DesentramarUART(QString);
        QString TramaEnviar();
        QTime RTC;
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // MAINWINDOW_H
