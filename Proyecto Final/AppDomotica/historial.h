/*******************************************************************************
 *
 * @file                 historial.h
 * @brief                Módulo para ver las configuraciones seteadas.
 * @version              1.00
 * @date                 15/10/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** MODULE
 ******************************************************************************/
#ifndef HISTORIAL_H
#define HISTORIAL_H

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include <QMainWindow>

/*******************************************************************************
 *** CLASES
 ******************************************************************************/
namespace Ui {
class historial;
}

class historial : public QMainWindow
{
    Q_OBJECT

public:
    explicit historial(QWidget *parent = nullptr);
    ~historial();

private:
    Ui::historial *ui;
};

/*******************************************************************************
 *** MODULE END
 ******************************************************************************/
#endif // HISTORIAL_H
