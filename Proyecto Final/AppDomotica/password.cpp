/*******************************************************************************
 *
 * @file                 password.cpp
 * @brief                Ventana para cambiar la contraseña.
 * @version              1.00
 * @date                 15/10/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "password.h"
#include "ui_password.h"
#include "qextserialport.h"
#include <QtSerialPort/qserialportinfo.h>
#include <QTextStream>
#include <QMessageBox>
#include <QVector>
#include <QDebug>

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/
extern QextSerialPort *Uart;

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
Password::Password(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Password) {
    ui->setupUi(this);
}

Password::~Password() {
    delete ui;
}

void Password::on_pushButton_Cambiar_clicked() {
    QString username = ui->lineEdit_Usuario->text();
    QString password = ui->lineEdit_Password->text();
    QString newpassword = ui->lineEdit_NewPassword->text();

    if((username == getUsername()) && (password == getPassword())) {
        setPassword(newpassword);
        if(Uart->open(QextSerialPort::ReadWrite)) {
            Uart->write(("<P" + newpassword + ">").toUtf8().constData(), 6);
        }
        ui->lineEdit_Usuario->clear();
        ui->lineEdit_Password->clear();
        ui->lineEdit_NewPassword->clear();
        this->hide();
    }
    else {
        QMessageBox::warning(this, "Error",
         "Usuario y/o contraseña incorrectas");

    }
}

void Password::setUsername(QString usrnm) {
    this->username_seteado = usrnm;
}

void Password::setPassword(QString passwrd) {
    password_seteado = passwrd;
}

QString Password::getUsername() const {
    return this->username_seteado;
}

QString Password::getPassword() const {
    return this->password_seteado;
}
