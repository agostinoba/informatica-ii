/*******************************************************************************
 *
 * @file                 config.cpp
 * @brief                Módulo para setear una configuración.
 * @version              1.00
 * @date                 15/10/2020
 * @author               Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES GLOBALES
 ******************************************************************************/
#include "config.h"
#include "ui_config.h"
#include "uart.h"
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QVector>
#include <QDebug>

/*******************************************************************************
 *** VARIABLES GLOBALES
 ******************************************************************************/
extern QextSerialPort *Uart;

/*******************************************************************************
*** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define PATH_HISTORIAL "/home/agostino/Documents/UTN/config.txt"

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
config::config(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::config) {
    ui->setupUi(this);
    if(QFile::exists(PATH_HISTORIAL)) {
        this->CargarConfiguracion();
    }
    else {
        QFile file(PATH_HISTORIAL);
        file.open(QIODevice::ReadWrite | QIODevice::Text);
        file.close();
    }
}

config::~config() {
    delete ui;
}

void config::on_pushButton_Aceptar_clicked() {
    QString Configuracion = "";
    int checkdata = 0;

    if(0 != (ui->spinBox_tempamb_tempset->value())) {
        checkdata++;
    }

    if(ui->timeEdit_tempamb_hinit->time().hour() <
       ui->timeEdit_tempamb_hfin->time().hour()) {
        checkdata++;
    }
    else if(ui->timeEdit_tempamb_hinit->time().hour() ==
            ui->timeEdit_tempamb_hfin->time().hour()) {
        if(ui->timeEdit_tempamb_hinit->time().minute() <
           ui->timeEdit_tempamb_hfin->time().minute()) {
            checkdata++;
        }
        else {
            QMessageBox::critical(this, "Error",
             "La hora inicial de la ventilacion debe ser inferior a la final");
        }
    }
    else {
        QMessageBox::critical(this, "Error",
         "La hora inicial de la ventilacion debe ser inferior a la final");
    }

    if(ui->timeEdit_ilum_hinit->time().hour() <
       ui->timeEdit_ilum_hfin->time().hour()) {
        checkdata++;
    }
    else if(ui->timeEdit_ilum_hinit->time().hour() ==
            ui->timeEdit_ilum_hfin->time().hour()) {
        if(ui->timeEdit_ilum_hinit->time().minute() <
           ui->timeEdit_ilum_hfin->time().minute()) {
            checkdata++;
        }
        else {
            QMessageBox::critical(this, "Error",
             "La hora inicial de la iluminacion debe ser inferior a la final");
        }
    }
    else {
        QMessageBox::critical(this, "Error",
         "La hora inicial de la iluminacion debe ser inferior a la final");
    }

    if(3 == checkdata) {
        this->SetRTC(QTime::currentTime());
        this->SetTempAmb(ui->spinBox_tempamb_tempset->value());
        this->SetTempAmbHoraInic(ui->timeEdit_tempamb_hinit->time());
        this->SetTempAmbHoraFin(ui->timeEdit_tempamb_hfin->time());
        this->SetIlumHoraInic(ui->timeEdit_ilum_hinit->time());
        this->SetIlumHoraFin(ui->timeEdit_ilum_hfin->time());
        Configuracion = this->GuardarConfiguracion();
        this->EnviarConfiguracion(Configuracion);
        this->setCheck(true);
        this->hide();
    }
}

void config::CargarConfiguracion() {
    QStringList model;
    QFile file(PATH_HISTORIAL);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream textoConfig(&file);
    while(!textoConfig.atEnd()) {
        QString line = textoConfig.readLine();
        QStringList fields = line.split("|");
        model.append(fields);
    }
    file.close();
}

QString config::GuardarConfiguracion() {
    QString configuracion = "";

    configuracion += QTime::currentTime().toString("hh:mm");
    configuracion += "|";

    configuracion += QString::number(this->GetTempAmb()) + "|";
    configuracion += this->GetTempAmbHoraInic().toString("hh:mm") + "|";
    configuracion += this->GetTempAmbHoraFin().toString("hh:mm") + "|";
    configuracion += this->GetIlumHoraInic().toString("hh:mm") + "|";
    configuracion += this->GetIlumHoraFin().toString("hh:mm");

    QString configuracionGuardar = "";
    configuracionGuardar += configuracion;

    QFile file(PATH_HISTORIAL);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream textoConfig(&file);
    QString buffer = textoConfig.readAll();
    file.close();

    file.open(QIODevice::WriteOnly | QIODevice::Text);
    file.write(configuracionGuardar.toStdString().c_str());
    file.write("\n");
    file.write(buffer.toStdString().c_str());
    file.close();

    return configuracion;
}

void config::EnviarConfiguracion(QString Configuracion) {
    std::string Datos = Configuracion.toUtf8().constData();

    Uart->putChar(INICIO);
    for(int i = 0; i < Configuracion.length(); i++)
        Uart->putChar(Datos[i]);
    Uart->putChar(FIN);
}

void config::SetTempAmb(int TMax) {
    this->temperatura_ambiente = TMax;
}

void config::SetTempAmbHoraInic(QTime HInic) {
    this->temp_amb_hora_inicio = HInic;
}

void config::SetTempAmbHoraFin(QTime HFin) {
    this->temp_amb_hora_fin = HFin;
}

void config::SetIlumHoraInic(QTime HInic) {
    this->iluminacion_hora_inicio = HInic;
}

void config::SetIlumHoraFin(QTime HFin) {
    this->iluminacion_hora_fin = HFin;
}

void config::SetRTC(QTime RTC) {
    this->RTC = RTC;
}

QTime config::GetRTC() {
    return this->RTC;
}

int config::GetTempAmb() {
    return this->temperatura_ambiente;
}

QTime config::GetTempAmbHoraInic() {
    return this->temp_amb_hora_inicio;
}

QTime config::GetTempAmbHoraFin() {
    return this->temp_amb_hora_fin;
}

QTime config::GetIlumHoraInic() {
    return this->iluminacion_hora_inicio;
}

QTime config::GetIlumHoraFin() {
    return this->iluminacion_hora_fin;
}

void config::setCheck(bool check) {
    this->check_correcto = check;
}

bool config::getCheck() const {
    return this->check_correcto;
}

void config::on_pushButton_Cerrar_clicked() {
    this->setCheck(false);
    this->hide();
}
