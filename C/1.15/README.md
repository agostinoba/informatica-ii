# Ejercicio 1.15

Basándose en el ejercicio 1.14, modifique la primer aplicación para que pueda
manejar múltiples clientes, a los cuáles en lugar de enviarles un único valor
aleatorio entre 1 y 6, deberá enviarle 5 valores aleatoriamente entre 1 y 6,
esperando un segundo entre envíos. La segunda aplicación deberá ser
modificada para poder recibir los valores e ir mostrándolos en pantalla a
medida que llegan, pero sin saber cuántos valores le van a llegar, deberá
terminar cuando el servidor cierre su conexión.
 Si el servidor recibe la señal SIGUSR1 mientras tiene clientes conectados,
 el mismo deberá esperar que dichos clientes terminen de recibir los valores,
 y una vez terminados todos, recién terminar.

 Nota: No contemple el problema de que si dos clientes se conectan al mismo
 tiempo, recibirán los mismos valores aleatorios.
