/*******************************************************************************
 *
 * @file                ShmWriter13.c
 * @brief               Ejercicio 1.13 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <errno.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define SHM_SIZE (5 * sizeof(int)) // Tamaño de la memoria 5 x int
#define KEY_PATH "/tmp"
#define N_PROCESOS 1000

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int ShmConnect(int, int*);
void ShmWrite(int *, int, int);
void ShmDelete(int);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(int argc, char *argv[]) {
    if(1 == argc) { // No recibe argumentos
        int shmid, *numeros;
        int auxiliar;
        pid_t pid_child;
        key_t myKey;

        // Creo la Key
        if(-1 == (myKey = ftok(KEY_PATH, 'B'))) {
            perror("ftok");
            exit(EXIT_FAILURE);
        }

        // Se conecta o crea el segmento de memoria
        if(-1 == (shmid = ShmConnect(myKey, &auxiliar))) {
            perror("shmget");
            exit(EXIT_FAILURE);
        }

        // numeros es donde donde escribo lo que se manda
        numeros = (int *)(shmat(shmid, NULL, 0));

        if((void *)(-1) == (void *)numeros) {
            perror("shmat");
            exit(EXIT_FAILURE);
        }

        // Creo los hijos
        for(int i = 0; i < N_PROCESOS; i++) {
            pid_child = fork();
            if(auxiliar) {
                switch(pid_child) {
                    case -1: // Error
                        perror("fork");
                        break;

                    case 0: // Hijo
                        for(int j = 0; j < 5; j++) {
                            (*(numeros + j))++;
                            ShmWrite(numeros, j, *(numeros + j));
                        }

                        // Desconectarse de la memoria
                        if(-1 == shmdt((void *)numeros)) {
                            perror("shmdt");
                            exit(EXIT_FAILURE);
                        }

                        exit(0);
                        break;

                    default: // Padre
                        continue;
                        break;
                }
            }

            // El padre espera los exit de los hijos
            for(int k = 0; k < N_PROCESOS; k++)
                pid_child = wait(NULL);

            // Desconectarse de la memoria
            if(-1 == shmdt((void *)numeros)) {
                perror("shmdt");
                exit(EXIT_FAILURE);
            }
        }
    }
    else if(2 == argc) { // Envío señal SIGUSR1
        int proceso;
        sscanf(argv[1], "%d", &proceso);
        kill(proceso, SIGUSR1);
    }
    else { // Error en los parametros de main
        printf("Error al ingresar los parámetros\n");
    }
    return 0;
}

int ShmConnect(key_t key, int *f) {
    int shmid;
    *f = 0;

    if(0 <= (shmid = shmget(key, SHM_SIZE, 0644))) {
        *f = 1;
        return shmid;
    }
    return -1;
}

void ShmWrite(int *buff, int posicion, int valor) {
    *(buff + posicion) = valor;
}

void ShmDelete(int shmid) {
    if(-1 == shmctl(shmid, IPC_RMID, NULL)) {
        perror("shmctl");
        exit(EXIT_FAILURE);
    }
}
