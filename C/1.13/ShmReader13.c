/*******************************************************************************
 *
 * @file                ShmReader13.c
 * @brief               Ejercicio 1.13 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define SHM_SIZE (5 * sizeof(int)) // Tamaño de la memoria 5 x int
#define KEY_PATH "/tmp"

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
void SignalHandler(int);
int ShmConnect(int, int*);
void ShmDelete(int);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(void) {
    int shmid, *numeros, i, auxiliar;
    key_t myKey;
    pid_t myPid;

    signal(SIGUSR1, &SignalHandler);

    // Creo la Key
    if(-1 == (myKey = ftok(KEY_PATH, 'B'))) {
        perror("ftok");
        exit(EXIT_FAILURE);
    }

    // Se conecta al segmento de memoria
    if(-1 == (shmid = ShmConnect(myKey, &auxiliar))) {
        perror("shmget");
        exit(EXIT_FAILURE);
    }

    // numeros es el puntero a la shared memory
    numeros = (int *)(shmat(shmid, NULL, SHM_RDONLY));

    // En caso de que no se haya podido enganchar a la memoria
    if((void *)(-1) == (void *)numeros) {
        perror("shmat");
        exit(EXIT_FAILURE);
    }

    myPid = getpid();

    printf("  _____________________________________________________________
            \n");
    printf(" |                   Numero de proceso: %d                   |
            \n", myPid);
    printf("  _____________________________________________________________
            \n");
    printf(" | Key: %d | shmid: %d | shmpointer: %p |\n",
            myKey, shmid, numeros);
    printf(" | --------------- | ------------ | -------------------------- |
            \n");

    while(1) {
        for(i = 0; i < 5; i++)
            printf(" | Pos: %10d | Valor: %5d | Puntero: %17p |\n",
                    i, *(numeros + i), (numeros + i));
        printf(" | ----------------------------------------------------------- |
                \n");
        sleep(5);
    }
    return 0;
}

void SignalHandler(int signum) {
    if(SIGUSR1 == signum) {
        int shmid;
        key_t myKey;

        // Creo la Key
        if(-1 == (myKey = ftok(KEY_PATH, 'B'))) {
            perror("ftok");
            exit(EXIT_FAILURE);
        }

        // Elimino la Shared Memory
        if(0 <= (shmid = shmget(myKey, SHM_SIZE, 0666|IPC_CREAT)))
            ShmDelete(shmid);

        printf(" | ----------------------------------------------------------- |
                \n");
        printf(" | -------------------------Exit------------------------------ |
                \n");
        printf(" | ----------------------------------------------------------- |
                \n");
        exit(0);
    }
}

int ShmConnect(key_t key, int *f) {
    int shmid;

    *f = 0;
    // Si no existe la creo
    if(0 <= (shmid =shmget(key, SHM_SIZE, 0666|IPC_CREAT|IPC_EXCL))) {
        *f = 1; // Segmento recién creado. Hay que inicarlo
        return shmid;
    }

    // Me conecto si existe el segmento
    if(EEXIST == errno) {
        if(0 <= (shmid = shmget(key, SHM_SIZE, 0666|IPC_CREAT)))
            return shmid;
    }
    return -1;
}

void ShmDelete(int shmid) {
    if(-1 == shmctl(shmid, IPC_RMID, NULL)) {
        perror("shmctl");
        exit(EXIT_FAILURE);
    }
}
