# Ejercicio 1.13

Basándose en el ejercicio 1.11, desarrolle dos aplicaciones. La primera será
idéntica a la primera del ejercicio 1.11. La segunda deberá generar 1000
procesos hijo, y cada uno incrementará en 1 cada valor del segmento de
***memoria compartida*** creado por el primer proceso cuando es llamada sin
argumentos de main. Cuando es utilizada con un argumento de main, al igual que
en el ejercicio 1.11, deberá enviar la señal SIGUSR1 al proceso cuyo PID sea
el correspondiente al pasado por el usuario. Para hacer más notorio el efecto
buscado en éste ejercicio, ejecute la segunda aplicación de la siguiente
forma:

        for run in {1..100}; do ./nombre_del_programa; done
Esta línea ejecutará 100 veces sucesivamente el programa indicado.
¿En cuáto debería resultar la sumatoria de la memoria compartida? ¿Por qué
no sucede esto?

Implemente algún mecanismo de sincronización para evitar este efecto.
