/*******************************************************************************
 *
 * @file                ShmWriter.c
 * @brief               Ejercicio 1.10 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>
#include <time.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define SHM_SIZE 1024
#define KEY_PATH "/tmp"

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int ShmConnect(int key, int* f);
void ShmWrite(int *buff);
void ShmDelete(int shmid);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(void)
{
    int shmid, *numeros, i;
    int auxiliar;
    key_t myKey;

    // Creo la Key
    if(-1 == (myKey = ftok(KEY_PATH, 'B'))) {
        perror("ftok");
        exit(EXIT_FAILURE);
    }

    // Se conecta o crea el segmento de memoria
    if(-1 == (shmid = ShmConnect(myKey, &auxiliar))) {
        perror("shmget");
        exit(EXIT_FAILURE);
    }

    numeros = (int *)(shmat(shmid, NULL, 0));

    if((void *)(-1) == (void *)numeros) {
        ShmDelete(shmid);
        perror("shmat");
        exit(EXIT_FAILURE);
    }

    printf("  _____________________________________________________________
            \n");
    printf(" | Key: %d | shmid: %d | shmpointer: %p |\n",
            myKey, shmid, numeros);
    printf(" | --------------- | ------------ | -------------------------- |
            \n");

    if(auxiliar)
        ShmWrite(numeros);

    for(i = 0; i < 10; i++)
        printf(" | Pos: %10d | Valor: %5d | Puntero: %17p |\n",
                i, *(numeros + i), (numeros + i));
    printf("  -------------------------------------------------------------
            \n\n");

    // Desconectarse de la memoria
    if(-1 == shmdt((void *)numeros)) {
        perror("shmdt");
        exit(EXIT_FAILURE);
    }

    return 0;
}

int ShmConnect(key_t key, int *f) {
    int shmid;

    *f = 0;
    // Si no existe la creo
    if(0 <= (shmid =shmget(key, SHM_SIZE, 0666|IPC_CREAT|IPC_EXCL))) {
        *f = 1; // Segmento recién creado. Hay que inicarlo
        return shmid;
    }

    // Me conecto si existe el segmento
    if(EEXIST == errno) {
        if(0 <= (shmid = shmget(key, SHM_SIZE, 0666|IPC_CREAT)))
            return shmid;
    }

    return -1;
}

void ShmWrite(int *buff) {
    int i;
    for(i = 9; i >= 0; i--) {
        srand(i + time(0));
        *(buff + i) = (rand() % 11);
    }
}

void ShmDelete(int shmid) {
    if(-1 == shmctl(shmid, IPC_RMID, NULL)) {
        perror("shmctl");
        exit(EXIT_FAILURE);
    }
}
