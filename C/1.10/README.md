# Ejercicio 1.10

Desarrolle dos aplicaciones. La primera creará un segmento de ***memoria
compartida***, y escribirá en el mismo 10 valores enteros aleatorios entre 0
y 10. Si la ***memoria compartida*** ya estaba creada, deberá dejarla como
estaba. La segunda aplicación, leerá la ***memoria compartida*** creada por
el primer proceso, impimirá en pantalla el resultado de la sumatoria de sus
valores y eliminará el segmento de ***memoria compartida***.
