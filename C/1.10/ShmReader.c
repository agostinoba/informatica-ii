/*******************************************************************************
 *
 * @file                ShmReader.c
 * @brief               Ejercicio 1.10 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>
#include <time.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define SHM_SIZE 1024
#define KEY_PATH "/tmp"

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int ShmConnect(int key, int* f);
int ShmRead(int *buff);
void ShmDelete(int shmid);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(void) {
    int shmid, *numeros, i;
    int auxiliar, suma;
    key_t myKey;

    // Creo la Key
    if(-1 == (myKey = ftok(KEY_PATH, 'B'))) {
        perror("ftok");
        exit(EXIT_FAILURE);
    }

    // Se conecta al segmento de memoria
    if(-1 == (shmid = ShmConnect(myKey, &auxiliar))) {
        perror("shmget");
        exit(EXIT_FAILURE);
    }

    numeros = (int *)(shmat(shmid, NULL, SHM_RDONLY));

    if((void *)(-1) == (void *)numeros) {
        perror("shmat");
        exit(EXIT_FAILURE);
    }

    printf("  _____________________________________________________________
            \n");
    printf(" | Key: %d | shmid: %d | shmpointer: %p |\n",
            myKey, shmid, numeros);
    printf(" | --------------- | ------------ | -------------------------- |
            \n");

    for(i = 0; i < 10; i++)
        printf(" | Pos: %10d | Valor: %5d | Puntero: %17p |\n",
                i, *(numeros + i), (numeros + i));
    printf(" | ----------------------------------------------------------- |
            \n");

    if(auxiliar)
        suma = ShmRead(numeros);

    printf(" | ");
    for(i = 0; i < 9; i++)
        printf("%2d + ", *(numeros + i));
    printf("%2d    =%6d  |\n", *(numeros + 9), suma);
    printf("  -------------------------------------------------------------
            \n\n");

    // Desconectarse de la memoria
    if(-1 == shmdt((void *)numeros)) {
        perror("shmdt");
        exit(EXIT_FAILURE);
    }
    ShmDelete(shmid);
    return 0;
}

int ShmConnect(key_t key, int *f) {
    int shmid;

    *f = 0;

    if(0 <= (shmid = shmget(key, SHM_SIZE, 0644))) {
        *f = 1;
        return shmid;
    }
    return -1;
}

int ShmRead(int *buff) {
    int i, suma = 0;
    for(i = 9; i >= 0; i--)
        suma += *(buff + i);
    return suma;
}

void ShmDelete(int shmid) {
    if(-1 == shmctl(shmid, IPC_RMID, NULL)) {
        perror("shmctl");
        exit(EXIT_FAILURE);
    }
}
