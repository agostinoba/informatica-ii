/*******************************************************************************
 *
 * @file                SemWaiter12.c
 * @brief               Ejercicio 1.12 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <errno.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define KEY_PATH "/tmp"

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
void main(void) {
    int semid; // Identificador del semaforo
    int valor;
    key_t myKey;
    struct sembuf dsem; // Operaciones el semaforo

    if(-1 == (myKey = ftok(KEY_PATH, 'B'))) {
        perror("ftok");
        exit(EXIT_FAILURE);
    }

    // Creo un grupo de semaforos de tamaño 1
    if(-1 == (semid = semget(myKey, 1, 0666))) {
        perror("semid");
        exit(EXIT_FAILURE);
    }

    while(EIDRM != errno) {
        // Checkeo si el semáforo está en 0
        if(0 == (valor = semctl(semid, 0, GETVAL, 0))) {
            printf("Tengo acceso!\n");
            sleep(1);
        }
        // Checkeo que el semáforo se haya eliminado
        if(-1 == (valor = semctl(semid, 0, GETVAL, 0))) {
            perror("semctl");
            exit(EXIT_FAILURE);
        }
    }
}
