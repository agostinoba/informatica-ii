/*******************************************************************************
 *
 * @file                SemCreator12.c
 * @brief               Ejercicio 1.12 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define KEY_PATH "/tmp"

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
void AdquirirSemaforo(int semid, int sem);
void LiberarSemaforo(int semid, int sem);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
void main(void) {
    int semid; // Identificador del semaforo
    int valor;
    struct sembuf buff;
    key_t myKey;

    if(-1 == (myKey = ftok(KEY_PATH, 'B'))) {
        perror("ftok");
        exit(EXIT_FAILURE);
    }

    // Creo un grupo de semaforos de tamaño 1
    if(-1 == (semid = semget(myKey, 1, 0666 | IPC_CREAT))) {
        perror("semid");
        exit(EXIT_FAILURE);
    }

    // Inicializo el semaforo en 0
    if(-1 == (semctl(semid, 0, SETVAL, 1))) {
        perror("semctl");
        exit(EXIT_FAILURE);
    }

    for(int i = 0; i < 3; i++) {
        printf("Presione ENTER para tomar el semaforo.\n");
        getchar();
        AdquirirSemaforo(semid, 0);
        printf("Presione ENTER para liberar el semaforo.\n");
        getchar();
        LiberarSemaforo(semid, 0);
    }

    // Para eliminar el semaforo
    if(-1 == (semctl(semid, 0, IPC_RMID))) {
        perror("semctl");
        exit(EXIT_FAILURE);
    }
}

void AdquirirSemaforo(int semid, int sem) {
    struct sembuf buff;
    buff.sem_num = sem;
    buff.sem_op = -1;
    buff.sem_flg = IPC_NOWAIT;
    if(-1 == (semop(semid, &buff, 1))) {
        perror("semop");
        exit(EXIT_FAILURE);
    }
}

void LiberarSemaforo(int semid, int sem) {
    struct sembuf buff;
    buff.sem_num = sem;
    buff.sem_op = 1;
    buff.sem_flg = IPC_NOWAIT;
    if(-1 == (semop(semid, &buff, 1))) {
        perror("semop");
        exit(EXIT_FAILURE);
    }
}
