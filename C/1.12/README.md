# Ejercicio 1.12

Desarrolle dos aplicaciones. La primera creará un ***semáforo***, lo
inicializará en val = 0, y estará a la espera de la tecla ENTER, indicando el
mensaje *"Presione ENTER para tomar el semáforo."*. Cuando el usuario
presione la tecla ENTER, deberá tomar el ***semáforo***, incrementando su
valor en uno, y nuevamente esperar la tecla ENTER, indicando el mensaje
*"Presione ENTER para liberar el semáforo."*. Cuando el usuario presione la
tecla ENTER, deberá liberar el ***semáforo***, decrementando su valor en uno,
indicar el mensaje *"Vuelta N/3"* siendo N el número de vuelta actual, y
volver a empezar. Este comportamiento se realizará 3 veces y luego la
aplicación eliminará el ***semáforo*** y terminará. La segunda aplicación
esperará a que el ***semáforo*** creado por la primer aplicación esté en
cero, momento en el cuál imprimirá en pantalla *"Tengo acceso!"* y esperará
un segundo, para repetir este proceso de forma indefinida. La segunda
aplicación terminará cuando el ***semáforo*** creado por la primer
aplicación sea eliminado.
