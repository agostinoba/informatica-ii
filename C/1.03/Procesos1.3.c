/*******************************************************************************
 *
 * @file                Procesos1.3.c
 * @brief               Ejercicio 1.3 de Informática II
 * @date                10/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
#define N_PROCESOS      5

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
void Signal_Handler(int sig) {
    // Que iba acá?
}

int f_rand(int min_range, int max_range) {
    srand(getpid());
    return (min_range + (rand() % (max_range - 1)));
}

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
int main(void) {
    pid_t pid_Child;
    int i, tiempo;

    // Cuando capte la señal SIGCHLD llama a Signal_Handler y le manda como
    // parámetro el número correspodniente a la señal
    signal(SIGCHLD, &Signal_Handler);

    // Muestro en pantalla el Pid del padre
    printf("Padre: PID %d, procesos a crear: %d\n", getpid(), N_PROCESOS);

    for(i = N_PROCESOS; i > 0; i--) {
        pid_Child = fork(); // Creo N procesos hijo

        if(pid_Child > 0) { // Padre
            printf("Hijo creado: %d\n", pid_Child);
            continue;
        }
        else if(0 == pid_Child) { // Hijo
            tiempo  = f_rand(2, 5);
            printf("Hijo %d: Estoy vivo... Tiempo: %d\n", getpid(), tiempo);
            sleep(tiempo);
            printf("Hijo %d terminando actividad\n", getpid());
            exit(0); // Devuelve "0" al wait() del padre
        }
        else { // Error pid_Child < 0
            printf("Error al crear un proceso hijo\n");
        }
    }

    for(i = N_PROCESOS; i > 0; i--) {
        // wait() blockea hasta que un hijo use exit(), devuelve pid del hijo
        pid_Child = wait(NULL);
        printf("Padre: Hijo %d termino su actividad\n", pid_Child);
    }

    return 0;
}
