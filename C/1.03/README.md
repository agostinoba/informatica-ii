# Ejercicio 1.3

Repita el ejercicio 1.2 pero eliminando procesos en estado *Zombie* de forma
asincrónica (mediante el uso de la señal SIGCHLD), informando si el mismo
terminó en forma normal o no. En caso de que el proceso haya finalizado en
forma normal, se deberá informar *"Padre: Hijo NNNN terminó normalmente."*.
En caso de que el proceso haya terminado de forma anormal, redisparar un nuevo
proceso, informando el suceso de la forma *"Padre: Hijo NNNN terminó
inesperadamente. Redisparando un proceso nuevo."* asimismo, el nuevo proceso,
deberá informar en pantalla cada 1 segundo que sigue vivo en la misma forma que
los demás.
