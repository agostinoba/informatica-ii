/*******************************************************************************
 *
 * @file                FifoReader.c
 * @brief               Ejercicio 1.6 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define KEY_PATH "/tmp"
#define MSG_SIZE 100

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/
struct msgbuff {
    long mtype;
    char mtext[MSG_SIZE];
};

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
key_t   MsqKey(void);
int     MsqConnect(key_t);
void    MsqControl(int, int);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(void) {
    int i, msqid;
    key_t myKey;
    struct msgbuff buff;

    myKey = MsqKey();

    msqid = MsqConnect(myKey);

    for(i = 0; 5 > i; i++) {
        // Recibe mensajes de tipo 1. Flag = 0
        if(-1 == msgrcv(msqid, &buff, sizeof(buff.mtext), 1, 0)) {
            perror("msgrcv");
            exit(EXIT_FAILURE);
        }
        printf("Mensaje recibido %d: %s\n", i, buff.mtext);
    }

    MsqControl(msqid, IPC_RMID);

    return 0;
}

key_t MsqKey(void) {
    key_t key;
    if(-1 == (key = ftok(KEY_PATH, 'B'))) {
        perror("ftok");
        exit(EXIT_FAILURE);
    }
    return key;
}

int MsqConnect(key_t key) {
    int msqid;

    if(-1 == (msqid = msgget(key, 0644 | IPC_CREAT))) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }

    return msqid;
}

void MsqControl(int msqid, int msqflg) {
    if(-1 == msgctl(msqid, msqflg, NULL)) {
        perror("msgctl");
        exit(EXIT_FAILURE);
    }
}
