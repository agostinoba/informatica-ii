# Ejercicio 1.8

Implemente el ejercicio 1.7 pero con ***colas de mensajes***. Descartar el
nombre, ya que las colas de mensajes no tienen nombre, e implementar que la
aplicación que lee los mensajes de la cola, luego de leer 5 mensajes, se
cierre, eliminando previamente la ***cola de mensajes***. La aplicación que
escriba mensajes, deberá escribir mensajes con tipo 1.
