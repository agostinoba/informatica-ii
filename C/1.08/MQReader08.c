/*******************************************************************************
 *
 * @file                MQReader08.c
 * @brief               Ejercicio 1.8 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define KEY_PATH "/tmp"
#define MSG_SIZE 100

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/
struct msgbuff {
    long mtype;
    char mtext[MSG_SIZE];
};

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
key_t   MsqKey(void);
int     MsqConnect(key_t key);
void    MsqControl(int msqid, int msqflg);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(int argc, char *argv[]) {

    if(6 != argc) {
        printf("Debes ingresar 5 argumentos\n");
        return 0;
    }

    int i;
    for(i = 1; i < 6; i++) {
        if(100 < strlen(argv[i])) {
            printf("El argumento %d es muy largo\n", i);
            return 0;
        }
    }

    int msqid;
    key_t myKey;
    struct msgbuff buff;

    myKey = MsqKey();

    msqid = MsqConnect(myKey);

    buff.mtype = 1; // Envia mensajes de tipo 1
    for(i = 1; i < 6; i++) {
        strcpy(buff.mtext, argv[i]);
        printf("Mensaje: %s | Buff: %s\n", argv[i], buff.mtext);

        // +1 por '\0'. Flag = 0
        if(-1 == (msgsnd(msqid, &buff, 1 + strlen(buff.mtext), 0))) {
            perror("msgsnd");
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}

key_t MsqKey(void) {
    key_t key;
    if(-1 == (key = ftok(KEY_PATH, 'B'))) {
        perror("ftok");
        exit(EXIT_FAILURE);
    }
    return key;
}

int MsqConnect(key_t key) {
    int msqid;

    if(-1 == (msqid = msgget(key, 0644 | IPC_CREAT))) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }

    return msqid;
}

void MsqControl(int msqid, int msqflg) {
    if(-1 == msgctl(msqid, msqflg, NULL)) {
        perror("msgctl");
        exit(EXIT_FAILURE);
    }
}
