/*******************************************************************************
 *
 * @file                PrPadre.c
 * @brief               Ejercicio 1.4 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
#define TIEMPO_DE_VIDA 5

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int f_rand(int);

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
int main(void) {
    pid_t pid_Child;
    int i, j, value, buff, bytes_leidos, fd[2];

    // Pipe tiene el puntero de lectura y escritura. El que no uso tengo que
    // cerrarlo
    if(0 > pipe(fd))
        perror("pipe\n");

    printf("Pid Padre:   %d\n", getpid()); // Muestro el Pid del padre

    pid_Child = fork(); // Creo un proceso hijo

    switch(pid_Child) {
        case -1: // Error
            perror("fork\n");
            break;

        case 0: // Hijo
            close(fd[0]); // Cierro el pipe de lectura. Solo escribo
            for(i = TIEMPO_DE_VIDA; i > 0; i--) {
                value = f_rand(i);
                write(fd[1], &value, sizeof(value));
                sleep(1);
            }
            close(fd[1]);
            exit(0);
            break;

        default: // Padre
            j = TIEMPO_DE_VIDA;
            printf("Pid Hijo:    %d\n", pid_Child);
            close(fd[1]); // Cierro el pipe de escritura. Solo leo
            printf("  ___________________________________________________\n");
            printf(" |   Pid del hijo | Valor aleatorio | Tiempo de vida |\n");
            printf(" |----------------|-----------------|----------------|\n");
            // read = 0 cuando se cierra el pipe
            while(0 < (bytes_leidos = read(fd[0], &buff, sizeof(buff)))) {
                printf(" |      %-9d |    %-12d |       %-5d    |\n", \
                    pid_Child, buff, j);
                j--;
            }
            printf("  ---------------------------------------------------\n\n");
            close(fd[0]);
            break;
    }

    // wait() blockea hasta que un hijo use exit(), devuelve pid del hijo
    pid_Child = wait(NULL);

    return 0;
}

int f_rand(int i) {
    srand(getpid() + i);
    return (rand());
}
