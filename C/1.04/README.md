# Ejercicio 1.4

Desarrolle un programa que genere un sub-proceso, el cuál tendrá un tiempo de
vida definido por una macro TIEMPO_DE_VIDA, y por cada segundo de vida
transmita, mediante un ***pipe***, un valor generado aleatoriamente a su
proceso creador (Padre) el cuál informará en pantalla el mismo en la forma
*"Mensaje recibido: NUMERO".*
