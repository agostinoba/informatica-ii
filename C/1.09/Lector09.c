/*******************************************************************************
 *
 * @file                Lector09.c
 * @brief               Ejercicio 1.9 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define KEY_PATH "/tmp"
#define MSG_SIZE 100

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/
struct msgbuff {
    long mtype;
    char mtext[MSG_SIZE];
};

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
key_t   MsqKey(void);
int     MsqConnect(key_t key);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(void) {
    int i, msqid;
    key_t myKey;
    struct msgbuff buff;

    myKey = MsqKey();

    msqid = MsqConnect(myKey);

    while(1) {
       for(i = 1; i < 6; i++) {
           // Recibe mensajes de tipo 1. Flag = 0
           if(-1 != msgrcv(msqid, &buff, sizeof(buff.mtext), i, IPC_NOWAIT)) {
               printf("Prioridad: %d | Mensaje: %s | ", i, buff.mtext);
               // Recibe mensajes de tipo 1. Flag = 0
               if(-1 != msgrcv(msqid, &buff, sizeof(buff.mtext), i, 0))
                   printf("Hora: %s\n", buff.mtext);
           }
       }
       if(errno != ENOMSG) { // IPC_NOWAIT devuelve este error si no recibe nada
           if(errno == EIDRM) {
               perror("EIDRM");
               exit(EXIT_FAILURE);
           }
           if(errno == EINVAL) {
               perror("EINVAL");
               exit(EXIT_FAILURE);
           }
           perror("msgrcv");
           exit(EXIT_FAILURE);
       }
    }
    return 0;
}

key_t MsqKey(void) {
    key_t key;
    if(-1 == (key = ftok(KEY_PATH, 'B'))) {
        perror("ftok");
        exit(EXIT_FAILURE);
    }
    return key;
}

int MsqConnect(key_t key) {
    int msqid;
    if(-1 == (msqid = msgget(key, 0644 | IPC_CREAT))) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }
    return msqid;
}
