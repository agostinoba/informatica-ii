# Ejercicio 1.9

Desarrolle tres aplicaciones. La primera creará una ***cola de mensajes*** y
estará a la espera de mensajes **SOLO** de tipo **TERMINAR** en la misma. En
el momento en que la aplicación reciba un mensaje de tipo **TERMINAR
(Tipo = 100)**, eliminará la ***cola de mensajes*** y terminará la
aplicación. La segunda aplicación, se vinculará a la misma ***cola de
mensajes***, y mostrará los mensajes de tipo **IMPORTANCIA_N5** o menores,
indicando la hora que llegó en el mensaje, el contenido del mismo y la
importancia del mismo. Asimismo, deberá informar cuando el primer proceso
cerró la ***cola de mensajes***. La tercer aplicación, irá pidiéndole al
usuario primero el nivel de importancia del mensaje, y luego el mensaje a
encolar, que tendrá como máximo 100 caracteres y también incluirá la hora
(el mensaje y la hora __**DEBEN**__ ir en campos separados del mensaje).
El nivel de importancia tendrá que ser mayor a cero, y menor a 100.

No contemplar el caso donde el nivel de importancia que ingresa el usuario es
menor a uno, o no es numérico.
