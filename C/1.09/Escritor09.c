/*******************************************************************************
 *
 * @file                Escritor09.c
 * @brief               Ejercicio 1.9 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define KEY_PATH "/tmp"
#define MSG_SIZE 100
#define TERMINAR 100

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/
struct msgbuff {
    long mtype;
    char mtext[MSG_SIZE];
};

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
key_t   MsqKey(void);
int     MsqConnect(key_t key);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(void) {
    int msqid;
    long Tipo = 0;
    key_t myKey;
    time_t tiempo;
    struct msgbuff buff;

    myKey = MsqKey();

    msqid = MsqConnect(myKey);

    while(TERMINAR != Tipo) {
        printf("Ingrese nivel de importancia del mensaje\n");
        scanf("%ld", &(buff.mtype));

        if((long int)100 != buff.mtype) {
            printf("Ingrese el mensaje\n");
            scanf("%s", buff.mtext);

            // +1 por '\0'. Flag = 0
            if(-1 == (msgsnd(msqid, &buff, 1 + strlen(buff.mtext), 0))) {
                perror("msgsnd");
                exit(EXIT_FAILURE);
            }

            time(&tiempo);
            strcpy(buff.mtext, ctime(&tiempo));

            // +1 por '\0'. Flag = 0
            if(-1 == (msgsnd(msqid, &buff, 1 + strlen(buff.mtext), 0))) {
                perror("msgsnd");
                exit(EXIT_FAILURE);
            }
        }
        else {
            time(&tiempo);
            strcpy(buff.mtext, ctime(&tiempo));
            printf("Prioridad: %ld | Mensaje: %s\n", buff.mtype, buff.mtext);

            // +1 por '\0'. Flag = 0
            if(-1 == (msgsnd(msqid, &buff, 1 + strlen(buff.mtext), 0))) {
                perror("msgsnd");
                exit(EXIT_FAILURE);
            }
        }
        Tipo = buff.mtype;
    }

    return 0;
}


key_t MsqKey(void) {
    key_t key;
    if(-1 == (key = ftok(KEY_PATH, 'B'))) {
        perror("ftok");
        exit(EXIT_FAILURE);
    }
    return key;
}

int MsqConnect(key_t key) {
    int msqid;
    if(-1 == (msqid = msgget(key, 0644 | IPC_CREAT))) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }
    return msqid;
}
