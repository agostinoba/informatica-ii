/*******************************************************************************
 *
 * @file                Client14.c
 * @brief               Ejercicio 1.14 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <errno.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define MYPORT          8000
#define N_CONEXIONES    1

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
void FuncError(const char *);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(int argc, char *argv[]) {
    if(2 == argc) {
        int sockfd, number;
        struct in_addr addr;
        struct sockaddr_in servaddr;

        if(0 == inet_aton(argv[1], &addr))
            FuncError("IP invalida");

        // Creo el socket
        if(-1 == (sockfd = socket(AF_INET, SOCK_STREAM, 0)))
            FuncError("socket");

        // Armo la estructura sockaddr_in
        servaddr.sin_family         = AF_INET;
        servaddr.sin_addr           = addr;
        servaddr.sin_port           = htons(MYPORT);
        // Seteo en cero el resto de la estructura
        memset((void *) &(servaddr.sin_zero), '\0', 8);

        if(-1 == connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)))
            FuncError("connect");

        if(-1 == recv(sockfd, &number, sizeof(number), 0)) // flag = 0
            FuncError("recv");

        printf("Número aleatorio recibido: %d\n", number);

        close(sockfd);
    }
    else
        printf("Debe ingresar la IP del servidor\n");

    return 0;
}

void FuncError(const char *msgerror) {
    perror(msgerror);
    exit(EXIT_FAILURE);
}
