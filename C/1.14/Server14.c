/*******************************************************************************
 *
 * @file                Server14.c
 * @brief               Ejercicio 1.14 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <errno.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define MYPORT          8000
#define N_CONEXIONES    1

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
int g_salida;

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
void ProtegerCierrePuertos(int);
void sigchld_handler(int);
void EliminarProcesosMuertos(void);
void EnviarMensaje(int);
int FuncRand(int, int);
void SignalHandler(int);
void FuncError(const char*);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(int argc, char *argv[]) {
    if(2 == argc) {
        int sockfd, connfd;
        socklen_t clilen;
        struct sockaddr_in servaddr, client;
        g_salida = 1;

        signal(SIGUSR1, &SignalHandler);

        // Creo el socket
        if(-1 == (sockfd = socket(AF_INET, SOCK_STREAM, 0)))
            FuncError("socket");

        ProtegerCierrePuertos(sockfd);

        // Armo la estructura sockaddr_in
        servaddr.sin_family         = AF_INET;
        servaddr.sin_addr.s_addr    = inet_addr(argv[1]);
        servaddr.sin_port           = htons(MYPORT);
        // Seteo en cero el resto de la estructura
        memset((void *) &(servaddr.sin_zero), '\0', 8);

        // Uno el socket con el puerto
        if(-1 == (bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr))))
            FuncError("bind");

        if(-1 == listen(sockfd, N_CONEXIONES))
            FuncError("listen");

        clilen = sizeof(client);

        EliminarProcesosMuertos();

        while(g_salida) {
            printf("Esperando conexion\n");
            if(0 > (connfd = accept
                        (sockfd, (struct sockaddr *)&client, &clilen))) {
                if(EWOULDBLOCK != errno)
                    FuncError("accept");
            }
            EnviarMensaje(connfd);
            close(connfd);
        }
    }
    else
        printf("Debe ingresar la IP\n");

    return 0;
}

void ProtegerCierrePuertos(int sockfd) {
    // Puede pasar que algún socket quede colgado en el puerto
    // Libero el puerto de los socket que puedan estar bloqueandolo
    int yes = 1;
    if(-1 == (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes))))
        FuncError("setsockopt");
}

void sigchld_handler(int s) {
    while(wait(NULL) > 0);
}

void EliminarProcesosMuertos(void) { // Elimina procesos muertos
    struct sigaction sa;
    sa.sa_handler = sigchld_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if(-1 == sigaction(SIGCHLD, &sa, NULL))
        FuncError("sigaction");
}

void EnviarMensaje(int connfd) {
    int numero;
    numero = FuncRand(1, 6);
    if(-1 == (write(connfd, &numero, sizeof(numero))))
        FuncError("write");
}

int FuncRand(int min_range, int max_range) {
    srand(getpid() + time(0));
    return (min_range + (rand() % max_range));
}

void SignalHandler(int signum) {
    if(SIGUSR1 == signum)
        g_salida = 0;
}

void FuncError(const char *msgerror) {
    perror(msgerror);
    exit(EXIT_FAILURE);
}
