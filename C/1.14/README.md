# Ejercicio 1.14

Desarrolle dos aplicaciones. La primera creará un servidor, escuchando
conexiones en el puerto 8000, al momento de tener un cliente, le enviará un
número entero generado aleatoriamente entre 1 y 6, y luego, cerrará la
conexión para pasar a la espera de un nuevo cliente. A la aplicación se le
pasará por argumento de main la IP a la cuál vincular sus servicios. La
aplicación terminará cuando se le envíe la señal SIGUSR1 por línea de
comando. La segunda aplicación será el cliente que se conecte al servidor
creado en la primer aplicación , e imprimirá en pantalla el valor aleatorio
recibido. Recibirá por argumento de main, la IP del servidor.

Nota: Para obtener la IP de su PC utilice el comando "ifconfig" el cuál
arrojará una lista de todas las interfaces que tiene su PC. Utilice la de tipo
"inet" en el tipo de conexión que use (ethernet, wifi, etc.).
