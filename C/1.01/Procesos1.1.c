/*******************************************************************************
 *
 * @file                Procesos1.1.c
 * @brief               Ejercicio 1.1 de Informática II
 * @date                07/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

/*******************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 ******************************************************************************/
int main(void) {
        pid_t pid_Child;
        int i;

        printf("Padre: PID %d\n", getpid()); // Muestro el Pid del padre

        if(pid_Child = fork() > 0) { // Padre
                wait(NULL); // wait() blockea hasta que un hijo ejecute exit()
                printf("Padre: Hijo %d termino su actividad\n", pid_Child);
        }
        else if(pid_Child == 0){ // Hijo
                for(i = 0; i < 5; i++) {
                        printf("Hijo %d: Estoy vivo...\n", getpid());
                        sleep(1);
                }
                printf("Hijo %d terminando actividad\n", getpid());
                exit(0); // Devuelve "0" al wait() del padre
        }
        else { // Error pidC < 0
                printf("Error al crear un proceso hijo\n");
        }

        return 0;
}
