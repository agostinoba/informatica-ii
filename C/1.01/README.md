# Ejercicio 1.1

Desarrolle un programa que al ejecutarse genere un nuevo proceso, indicando en
pantalla previo a su generación, el ***Process ID*** del proceso principal en
la forma *"Padre: PID PPPP."* siendo PPPP el ***Process ID*** del proceso
principal. Una vez creado el nuevo proceso, deberá escribir cada 1 segundo en
pantalla *"Hijo NNNN: Estoy vivo..."*, siendo NNNN el ***Process ID*** del
mismo y pasados 5 segundos terminar, escribiendo *"Hijo NNNN: terminando
actividad."*. Cuando el proceso termine su tiempo de vida, el proceso padre
deberá escribir en pantalla *"Padre: Hijo NNNN terminó su actividad."* siendo
NNNN el ***Process ID*** del hijo que terminó su actividad.

No contemple el uso de la señal SIGCHLD.
