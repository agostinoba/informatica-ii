# Ejercicio 1.6

Desarrolle dos aplicaciones. La primera de ellas, creará una ***FIFO*** con el
nombre "Prueba_FIFO" y escribirá en la misma 10 valores aleatoriamente
generados, imprimiendo en pantalla los mismos. La segunda aplicación leerá la
***FIFO*** creada por la aplicación anterior, e imprimirá en pantalla los
valores leídos.

Nota: El programa que escribe, bloqueará hasta que el segundo programa lea la
***FIFO***.
