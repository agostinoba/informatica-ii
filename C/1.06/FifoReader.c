/*******************************************************************************
 *
 * @file                FifoReader.c
 * @brief               Ejercicio 1.6 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include  <sys/stat.h>
#include  <unistd.h>
#include  <fcntl.h>
#include  <stdio.h>

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(void) {
    int i, fd, n, buff[10];

    fd = open("/home/agostino/Documents/UTN/Info II/Practica/Guia Practica/
            1.06/prueba_FIFO", O_RDONLY);

    // El proceso queda bloqueado en el read hasta que tenga algo para leer
    n = read(fd, buff, sizeof(buff));

    for(i = 10; i > 0; i--) {
        printf("RX mensaje: %d \n", buff[i - 1]);
    }

    close(fd);

    return 0;
}
