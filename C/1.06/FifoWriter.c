/*******************************************************************************
 *
 * @file                FifoWriter.c
 * @brief               Ejercicio 1.6 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include  <sys/stat.h>
#include  <unistd.h>
#include  <fcntl.h>
#include  <stdio.h>
#include  <stdlib.h>

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int f_rand(int i);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(void) {
    int i, fd, buff[10];
    char filename[] = "prueba_FIFO";

    mkfifo("/home/agostino/Documents/UTN/Info II/Practica/Guia Practica/
            1.06/prueba_FIFO", 0666);

    // Este proceso queda bloqueado hasta que otro proceso abra el pipe
    // Un vez que otro proceso lo abrió, escribe en el pipe
    fd = open("/home/agostino/Documents/UTN/Info II/Practica/Guia Practica/
            1.06/prueba_FIFO", O_WRONLY);

    for(i = 10; i > 0; i--) {
        buff[i - 1] = f_rand(i);
        printf("Numero: %d\n", buff[i - 1]);
    }

    write(fd, buff, sizeof(buff));
    close(fd);

    i = remove(filename);

    return 0;
}

int f_rand(int i) {
    srand(getpid() + i);
    return (rand());
}
