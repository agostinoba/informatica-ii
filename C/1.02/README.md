# Ejercicio 1.2

Basándose en el ejercicio 1.1 desarrolle un programa que genere N_PROCESOS
procesos nuevos, siendo N_PROCESOS una macro, indicando en pantalla previo a
su generación, el ***Process ID*** del proceso principal en la forma *"Padre:
PID PPPP, procesos a crear: N_PROCESOS."* siendo PPPP el ***Process ID*** del
proceso principal. Cada proceso creado deberá tener un tiempo de vida
aleatorio, entre 2 y 5 segundos, durante el cuál deberá escribir en pantalla
*"Hijo NNNN: Estoy vivo..."* siendo NNNN el ***Process ID*** del proceso que lo
escriba. A medida que los procesos vayan terminando, deberán escribir en
pantalla *"Hijo NNNN: terminando actividad."*, y a medida que lo vayan haciendo,
el proceso principal deberá indicar por cada sub-proceso terminado el mensaje
*"Padre: Hijo NNNN termino su actividad."* siendo NNNN el ***Process ID*** del
hijo que terminó su actividad.

No contemple el uso de la señal SIGCHLD.

