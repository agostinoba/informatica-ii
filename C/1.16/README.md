# Ejercicio 1.16

Desarrolle una aplicación que implemente tres operaciones a lo largo de un
vector, ejecutando cada operación en un ***hilo*** distinto. Las operaciones
a realizar serán sumatoria, productoria, y conteo de números impares en un
vector inicializado aleatoriamente con valores entre 1 y 5, que tendrá
LARGO_BUFFER posiciones, siendo LARGO_BUFFER una macro de valor 10. La
aplicación deberá imprimir en pantalla los resultados que vaya obteniendo de
los distintos ***hilos***, **no imprimir los resultados desde las tareas de los
*hilos***. Implemente cada operación como una tarea de ***hilo*** distinta.
