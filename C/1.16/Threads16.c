/*******************************************************************************
 *
 * @file                Threads16.c
 * @brief               Ejercicio 1.16 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define NUM_THREADS     3
#define LARGO_BUFFER    10

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
int vecfunc[3] = {0, 0, 0};

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int FuncRand(int, int, int);
void *FuncSumatoria(void *);
void *FuncProducto(void *);
void *FuncImpares(void *);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(void) {
    pthread_t threads[NUM_THREADS]; // identificador de mi hilo
    void *(*Funciones[])(void*) = {&FuncSumatoria, &FuncProducto, &FuncImpares};
    int randvec[LARGO_BUFFER];

    for(int i = 0; i < LARGO_BUFFER; i++)
        randvec[i] = FuncRand(1, 5, i);

    for(int i = 0; i < NUM_THREADS; i++) {
       if(0 != (pthread_create
                   (&threads[i], NULL, Funciones[i], (void *)randvec))) {
            // thread id,
            // atributos del hilo,
            // funcion que se va a ejecutar,
            // argumentos que recibe la funcion
            perror("pthread_create");
            exit(EXIT_FAILURE);
        }
   }

    if(0 != (pthread_join(threads[0], NULL))) {
        perror("thread suma");
        exit(EXIT_FAILURE);
    }
    if(0 != (pthread_join(threads[1], NULL))) {
        perror("thread producto");
        exit(EXIT_FAILURE);
    }
    if(0 != (pthread_join(threads[2], NULL))) {
        perror("thread impares");
        exit(EXIT_FAILURE);
    }

    printf("Valores en el vector: ");
    for(int i = 0; i < LARGO_BUFFER - 1; i++)
        printf("%d, ",randvec[i]);
    printf("%d\n",randvec[LARGO_BUFFER - 1]);

    printf("La suma es: %d\nEl producto es: %d\nLa cantidad de impares es: %d\n"
            , vecfunc[0], vecfunc[1], vecfunc[2]);

    return 0;
}

int FuncRand(int min_range, int max_range, int indice) {
    srand(getpid() + time(0) + indice);
    return (min_range + (rand() % max_range));
}

void *FuncSumatoria(void *vec) {
    int *randvec = vec;

    for(int i = 0; i < LARGO_BUFFER; i++)
        vecfunc[0] += randvec[i];

    pthread_exit(NULL);
}

void *FuncProducto(void *vec) {
    int *randvec = vec;
    vecfunc[1] = randvec[0];

    for(int i = 1; i < LARGO_BUFFER; i++)
        vecfunc[1]  *= randvec[i];

    pthread_exit(NULL);
}

void *FuncImpares(void *vec) {
    int *randvec = vec;

    for(int i = 0; i < LARGO_BUFFER; i++) {
        if(randvec[i] % 2)
            vecfunc[2]++;
    }
    pthread_exit(NULL);
}
