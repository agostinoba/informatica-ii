/*******************************************************************************
 *
 * @file                ShmWriter11.c
 * @brief               Ejercicio 1.11 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define SHM_SIZE (5 * sizeof(int)) // Tamaño de la memoria 5 x int
#define KEY_PATH "/tmp"

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int ShmConnect(int, int*);
void ShmWrite(int *, int, int);
void ShmDelete(int);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(int argc, char *argv[]) {
    if(3 == argc) {
        int shmid, *numeros, i;
        int posicion, valor, auxiliar;
        key_t myKey;

        sscanf(argv[1], "%d", &posicion);
        sscanf(argv[2], "%d", &valor);

        if(1 > posicion)
            posicion = 1;
        if(5 < posicion)
            posicion = 5;

        // Creo la Key
        if(-1 == (myKey = ftok(KEY_PATH, 'B'))) {
            perror("ftok");
            exit(EXIT_FAILURE);
        }

        // Se conecta o crea el segmento de memoria
        if(-1 == (shmid = ShmConnect(myKey, &auxiliar))) {
            perror("shmget");
            exit(EXIT_FAILURE);
        }

        // numeros es donde donde escribo lo que se manda
        numeros = (int *)(shmat(shmid, NULL, 0));

        if((void *)(-1) == (void *)numeros) {
            perror("shmat");
            exit(EXIT_FAILURE);
        }

        printf("Posicion: %d | Valor: %d\n", posicion, valor);

        if(auxiliar)
            ShmWrite(numeros, posicion, valor);

        // Desconectarse de la memoria
        if(-1 == shmdt((void *)numeros)) {
            perror("shmdt");
            exit(EXIT_FAILURE);
        }

    }
    else if(2 == argc) { // Envío señal SIGUSR1
        int proceso;
        sscanf(argv[1], "%d", &proceso);
        kill(proceso, SIGUSR1);
    }
    else { // Error en los parametros de main
        printf("Error al ingresar los parámetros\n");
    }
    return 0;
}

int ShmConnect(key_t key, int *f) {
    int shmid;

    *f = 0;

    if(0 <= (shmid = shmget(key, SHM_SIZE, 0644))) {
        *f = 1;
        return shmid;
    }

    return -1;
}

void ShmWrite(int *buff, int posicion, int valor) {
    *(buff + posicion - 1) = valor;
}

void ShmDelete(int shmid) {
    if(-1 == shmctl(shmid, IPC_RMID, NULL)) {
        perror("shmctl");
        exit(EXIT_FAILURE);
    }
}
