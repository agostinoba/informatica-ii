# Ejercicio 1.11

Desarrolle dos aplicaciones. La primera creará un segmento de ***memoria
compartida*** que constará de 5 valores enteros, e imprimirá en pantalla cada
5 segundos dichos valores junto con el PID del proceso. La aplicación también
deberá atender la señal SIGUSR1 y al recibirla, eliminar el segmento de
memoria compartida y terminar. La segunda aplicación recibirá por argumentos
de main dos valores numéricos, uno correspondiente a la posición en el
segmento de ***memoria compartida***, y el segundo corresponderá al valor que
se quiera colocar en dicha posición. En caso de usar la aplicación con un
solo argumento, la aplicación deberá enviar la señal SIGUSR1 al proceso
cuyo PID sea el correspondiente al pasado por el usuario. En caso de intentar
escribir en una posición menor a uno, se truncará la posición en uno, y en
caso de ser mayor a cinco, se truncará en cinco.
