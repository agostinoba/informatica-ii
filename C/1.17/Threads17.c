/*******************************************************************************
 *
 * @file                Threads17.c
 * @brief               Ejercicio 1.17 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>

/*******************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 ******************************************************************************/
#define NUM_THREADS     4
#define LARGO_BUFFER    10

/*******************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 ******************************************************************************/
int randvec[LARGO_BUFFER];

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int FuncRand(int, int, int);
void *FuncSumatoria(void *);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(void) {
    pthread_t threads[NUM_THREADS]; // identificador de mi hilo

    for(int i = 0; i < LARGO_BUFFER; i++)
        randvec[i] = FuncRand(1, 5, i);

    printf("Valores en el vector: ");
    for(int i = 0; i < LARGO_BUFFER - 1; i++)
        printf("%d, ",randvec[i]);
    printf("%d\n",randvec[LARGO_BUFFER - 1]);

    for(int i = 0; i < NUM_THREADS; i++) {
        // necesito crear un espacio en memoria con el valor que le mando al
        // thread porque le mando una referencia que va cambiando despues de
        // crear el thread
        int *value = (int*) malloc(sizeof(int));
        *value = i;
       if(0 != (pthread_create
                   (&threads[i], NULL, FuncSumatoria, (void *)value))) {
            // thread id,
            // atributos del hilo,
            // funcion que se va a ejecutar,
            // argumentos que recibe la funcion
            perror("pthread_create");
            exit(EXIT_FAILURE);
        }
   }

    for(int i = 0; i < NUM_THREADS; i++) {
        if(0 != (pthread_join(threads[i], NULL))) {
            perror("thread suma");
            exit(EXIT_FAILURE);
        }

    }

    printf("Valores en el vector sumando: ");
    for(int j = 0; j < LARGO_BUFFER - 1; j++)
        printf("%d, ",randvec[j]);
    printf("%d\n",randvec[LARGO_BUFFER - 1]);

    return 0;
}

int FuncRand(int min_range, int max_range, int indice) {
    srand(getpid() + time(0) + indice);
    return (min_range + (rand() % max_range));
}

void *FuncSumatoria(void *val) {
    int *sumador = (int*) val;

    for(int i = 0; i < LARGO_BUFFER; i++)
        randvec[i] += *sumador + 1;

    free(sumador);
    pthread_exit(NULL);
}
