# Ejercicio 1.17

Desarrolle una aplicación que implemente una única operación a lo largo de
un vector, seccionando dicha operación en 4 partes, cada una ejecutada en un
***hilo*** distinto. La operación será incrementar cada valor del vector en
cierto valor, el cuál será pasado a cada ***hilo*** como argumento. El valor
sobre el cuál se va a operar, será un vector inicializado aleatoriamente con
valores entre 1 y 5, que tendrá LARGO_BUFFER posiciones, siendo LARGO_BUFFER
una macro de valor 10. La aplicación deberá imprimir el vector inicial, y el
vector luego de que todos los hilos hayan terminado de operar sobre el mismo.
El incremento de cada hilo será respectivamente 1, 2, 3 y 4.
