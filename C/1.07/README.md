# Ejercicio 1.7

Desarrolle dos aplicaciones. La primera de ellas, creará una ***FIFO*** con el
nombre "avisos_FIFO" y leerá los datos escritos en ella, imprimiendo en
pantalla los mismos en formato de string. Una vez leído el mensaje, la
aplicación terminará, cerrando la ***FIFO*** creada. La segunda aplicación
recibirá un argumento por línea de comando, que será el mensaje a guardar en
la ***FIFO*** creada por la primer aplicación, el cuá deberá ser menor a 100
caracteres y no deberá contener espacios.

Nota: El programa que lee, bloqueará hasta que el segundo programa escriba la
***FIFO***.
