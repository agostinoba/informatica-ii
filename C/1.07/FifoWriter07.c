/*******************************************************************************
 *
 * @file                FifoWriter07.c
 * @brief               Ejercicio 1.7 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include  <sys/stat.h>
#include  <unistd.h>
#include  <fcntl.h>
#include  <stdio.h>
#include  <stdlib.h>

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(void) {
    int i, fd, n;
    char buff[100], filename[] = "avisos_FIFO";

    mkfifo("/home/agostino/Documents/UTN/Info II/Practica/Guia Practica/
            1.07/avisos_FIFO", 0666);

    fd = open("/home/agostino/Documents/UTN/Info II/Practica/Guia Practica/
            1.07/avisos_FIFO", O_RDONLY);

    if(0 > fd)
        printf("Error al abrir el archivo\n");

    // El proceso queda bloqueado en el read hasta que tenga algo para leer
    n = read(fd, buff, sizeof(buff));

    if(0 > n)
        printf("Error al leer el archivo\n");

    printf("Aviso escrito: %s\n", buff);

    close(fd);

    i = remove(filename);

    return 0;
}
