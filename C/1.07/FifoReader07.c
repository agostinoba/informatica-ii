/*******************************************************************************
 *
 * @file                FifoReader07.c
 * @brief               Ejercicio 1.7 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include  <sys/stat.h>
#include  <unistd.h>
#include  <fcntl.h>
#include  <stdio.h>
#include  <string.h>

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(int argc, char *argv[]) {

    if((0 != argv[1]) && (100 > strlen(argv[1])) && (3 > argc)) {
        int i, fd;

        fd = open("/home/agostino/Documents/UTN/Info II/Practica/Guia Practica/
                1.07/avisos_FIFO", O_WRONLY);

        printf("El mensaje ingresado es: %s | tamaño: %ld\n",
                argv[1], strlen(argv[1]));

        write(fd, argv[1], sizeof(argv[1]));
        close(fd);
    }
    else {
        if((long unsigned int)100 < strlen(argv[1]))
            printf("El mensaje que ingreso es muy largo\n");
        else if(2 < argc)
            printf("Debe ingresar un mensaje sin espacios\n");
        else
            printf("Debe ingresar un mensaje como argumento\n");
    }

    return 0;
}
