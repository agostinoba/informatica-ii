/*******************************************************************************
 *
 * @file                PrPadre.c
 * @brief               Ejercicio 1.5 de Informática II
 * @date                21/04/2020
 * @author              Agostino Barbetti
 *
 ******************************************************************************/

/*******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

/*******************************************************************************
 *** MACROS PRIVADAS AL MODULO
 ******************************************************************************/
#define TIEMPO_DE_VIDA 5

/*******************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 ******************************************************************************/
struct Datos {
    int rand_value;
    pid_t pChild;
    char cTime[30];
    char message[50];
};

/*******************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int f_rand(int);

/*******************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 ******************************************************************************/
int main(void) {
    pid_t pid_Child;
    int i, value, bytes_leidos, len, fd[2];
    struct Datos Sdatos, buff;
    time_t vtime;

    // Pipe tiene el puntero de lectura y escritura. El que no uso tengo que
    // cerrarlo
    if(0 > pipe(fd))
        perror("pipe\n");

    printf("Pid Padre:   %d\n", getpid()); // Muestro el Pid del padre

    pid_Child = fork(); // Creo un proceso hijo

    switch(pid_Child) {
        case -1: // Error
            perror("fork\n");
            break;

        case 0: // Hijo
            close(fd[0]); // Cierro el pipe de lectura. Solo escribo
            for(i = TIEMPO_DE_VIDA; i > 0; i--) {
                Sdatos.rand_value = f_rand(i); // Numero aleatorio
                Sdatos.pChild = getpid(); // Pid del hijo
                time(&vtime); // Tiempo
                strcpy(Sdatos.cTime, ctime(&vtime));
                len = strlen(Sdatos.cTime);
                if(len > 0 && Sdatos.cTime[len-1] == '\n')
                    Sdatos.cTime[len-1] = '\0';
                // Mensaje con el numero de mensaje
                sprintf(Sdatos.message, "Mensaje %d", i);
                // Escribo estructura en el Pipe
                write(fd[1], &Sdatos, sizeof(Sdatos));
                sleep(1);
            }
            close(fd[1]);
            exit(0);
            break;

        default: // Padre
            printf("Pid Hijo:    %d\n", pid_Child);
            close(fd[1]); // Cierro el pipe de escritura. Solo leo
            printf("  __________________________________________________________
                    ___________________________ \n");
            printf(" |   Valor Aleatorio   | Pid del hijo | Hora al generar el
                    mensaje | Numero de mensaje | \n");
            printf(" |----------------------------------------------------------
                    ---------------------------| \n");
            // read = 0 cuando se cierra el pipe
            while(0 < (bytes_leidos = read(fd[0], &buff, sizeof(buff)))) {
                printf(" |     %-15d |     %-8d |  %-25s |     %-13s |\n",
                    buff.rand_value, buff.pChild, buff.cTime, buff.message);
            }
            printf("  ----------------------------------------------------------
                    --------------------------- \n\n");
            close(fd[0]);
            break;
    }

    // wait() blockea hasta que un hijo use exit(), devuelve pid del hijo
    pid_Child = wait(NULL);

    return 0;
}

int f_rand(int i) {
    srand(getpid() + i);
    return (rand());
}
