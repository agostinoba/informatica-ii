# Ejercicio 1.5

Basándose en el ejercicio 1.4, desarrolle un programa que en lugar de enviar
un valor generado aleatoriamente a su proceso creador, envíe un conjunto de
datos comprendido por:

- Valor generado aleatoriamente (entero)
- PID del proceso hijo
- Hora al generar el mensaje
- String con el texto "Mensaje A" donde A es el número de mensaje enviado

El proceso padre cada vez que reciba un nuevo mensaje deberá presentarlos en
pantalla, mostrando los cuatro campos.
